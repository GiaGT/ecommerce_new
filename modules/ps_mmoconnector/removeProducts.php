<?php

require_once __DIR__.'/ps_mmoconnector.php';
require_once __DIR__.'/vendor/autoload.php';

use MIP\PrestaShop\MMOLogger;

$productIds = [];

foreach ($productIds as $productId) {
    /** @var Product $object */
    $product = loadProduct($productId);

    if (!$product) {
        continue;
    }

    deleteProduct($product);
}

echo date("Y-m-d H:i:s") . " - OK";

/**
 * @param int $id
 * @return bool|\Product
 */
function loadProduct($id)
{
    if (!$id || !\Validate::isUnsignedId($id)) {
        MMOLogger::getInstance()->info('PRODUCT_DELETER.The object cannot be loaded (the identifier is missing or invalid)');
        return false;
    }

    $product = new \Product($id);

    if (!\Validate::isLoadedObject($product)) {
        MMOLogger::getInstance()->info('PRODUCT_DELETER.The object cannot be loaded (or found)');

        return false;
    }

    if (\Shop::getContext() == \Shop::CONTEXT_SHOP && \Shop::isFeatureActive() && !$product->isAssociatedToShop()) {
        $default_product = new \Product((int)$product->id, false, null, (int)$product->id_shop_default);
        $def = \ObjectModel::getDefinition($product);

        foreach ($def['fields'] as $field_name => $row) {
            if (is_array($default_product->{$field_name})) {
                foreach ($default_product->{$field_name} as $key => $value) {
                    $product->{$field_name}[$key] = $value;
                }
            } else {
                $product->{$field_name} = $default_product->{$field_name};
            }
        }
    }

    $product->loadStockData();

    return $product;
}

/**
 * @param \Product $product
 */
function deleteProduct($product)
{
    if (!\Validate::isLoadedObject($product)) {
        MMOLogger::getInstance()->info('PRODUCT_DELETER.An error occurred while deleting the object.(cannot load object)');
        return;
    }

    if ($product->advanced_stock_management && \Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
        $stock_manager = \StockManagerFactory::getManager();
        $physical_quantity = $stock_manager->getProductPhysicalQuantities($product->id, 0);
        $real_quantity = $stock_manager->getProductRealQuantities($product->id, 0);

        if ($physical_quantity > 0 || $real_quantity > $physical_quantity) {
            MMOLogger::getInstance()->info('You cannot delete this product because there is physical stock left. Product ID: '.$product->id);
            return;
        }
    }

    if ($product->delete()) {
        MMOLogger::getInstance()->info('PRODUCT_DELETER.SUCCESS Product ID: '.$product->id);
        return;
    }

    MMOLogger::getInstance()->info('PRODUCT_DELETER.An error occurred during deletion. Product ID: '.$product->id);
}