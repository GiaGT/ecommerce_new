<?php

if (!in_array(@$_SERVER['REMOTE_ADDR'],['90.161.45.249','localhost','127.0.0.1','::1'])) {
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");

    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    header("Location: ../");

    exit;
}

$parameterName = 'filename';
$filename = isset($_POST[$parameterName]) ? $_POST[$parameterName] : '';

if (empty($filename)) {
    $filename = isset($_GET[$parameterName]) ? $_GET[$parameterName] : null;
}

if (!empty($filename) && is_string($filename)) {
    $filename = stripslashes(urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($filename))));
}
$filePath = __DIR__."/$filename";

if (file_exists($filePath)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filePath));
    readfile($filePath);

    exit;
}