<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use MIP\PrestaShop\MMOLogger;
use MIP\PrestaShop\MMOFile;
use MIP\PrestaShop\MMOImport;

/**
 * @param \Module $module
 */
function upgrade_module_4_2_5($module)
{
    $success = true;

    require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
    $sql = require __DIR__.'/sql/sql-upgrade-4-2-5.php';

    foreach ($sql as $query) {
        try {
            if (!\Db::getInstance()->execute($query)) {
                $success = false;
                MMOLogger::getInstance()->critical(__METHOD__.' database update FAILED. Query: '.$query);
            }
        } catch (\Exception $e) {
            $success = false;
            MMOLogger::getInstance()->critical(__METHOD__.' database update FAILED. Query: '.$query.' Error: '.$e->getMessage());
        }
    }

    if (!$success) {
        MMOLogger::getInstance()->info(__METHOD__.' FAILED');
    }

    return true;
}
