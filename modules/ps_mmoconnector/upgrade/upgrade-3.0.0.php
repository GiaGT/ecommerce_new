<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use MIP\PrestaShop\MMOLogger;

/**
 * @param \Module $module
 */
function upgrade_module_3_0_0($module)
{
    require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
    $sql = require __DIR__.'/sql/sql-upgrade-3-0-0.php';

    if (empty($sql) || !is_array($sql)) {
        return true;
    }

    $success = true;

    foreach ($sql as $query) {
        try {
            if (!\Db::getInstance()->execute($query)) {
                $success = false;
				MMOLogger::getInstance()->critical('upgrade_module_3_0_0 database update FAILED. Query: '.$query);
            }
        } catch (\Exception $e) {
            $success = false;
            MMOLogger::getInstance()->critical('upgrade_module_3_0_0 database update FAILED. Query: '.$query.' Error: '.$e->getMessage());
        }
    }

    if ($success) {
        MMOLogger::getInstance()->info('upgrade_module_3_0_1 SUCCESS');
    }

    return true;
}
