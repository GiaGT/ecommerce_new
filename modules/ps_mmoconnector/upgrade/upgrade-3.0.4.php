<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use MIP\PrestaShop\MMOLogger;
use MIP\PrestaShop\MMOFile;
use MIP\PrestaShop\MMOImport;

/**
 * @param \Module $module
 */
function upgrade_module_3_0_4($module)
{
    $success = true;

    require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
    $sql = require __DIR__.'/sql/sql-upgrade-3-0-4.php';

    foreach ($sql as $query) {
        try {
            if (!\Db::getInstance()->execute($query)) {
                $success = false;
                MMOLogger::getInstance()->critical('upgrade_module_3_0_0 database update FAILED. Query: '.$query);
            }
        } catch (\Exception $e) {
            $success = false;
            MMOLogger::getInstance()->critical('upgrade_module_3_0_1 database update FAILED. Query: '.$query.' Error: '.$e->getMessage());
        }
    }

    if (!$success) {
        MMOLogger::getInstance()->info('upgrade_module_3_0_3 FAILED');
    }

    $success = true;
    $filesWithoutProcess = [];

    try {
        $filesWithoutProcess = MMOFile::getAllFilesNotProcessed();
    } catch (\Exception $e) {
        $success = false;
    }

    foreach ($filesWithoutProcess as $file) {
        try {
            MMOImport::preProcessInformation($file['name']);
        } catch (\Exception $e) {
            $success = false;
        }
    }

    if (!$success) {
        MMOLogger::getInstance()->info('upgrade_module_3_0_4 FAILED');
    }

    return true;
}
