<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use MIP\PrestaShop\MMOLogger;
use MIP\PrestaShop\MMOFile;
use MIP\PrestaShop\MMOImport;

/**
 * @param \Module $module
 */
function upgrade_module_4_1_7($module)
{
    $success = true;

    require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
    $sql = require __DIR__.'/sql/sql-upgrade-4-1-7.php';

    foreach ($sql as $query) {
        try {
            if (!\Db::getInstance()->execute($query)) {
                $success = false;
                MMOLogger::getInstance()->critical('upgrade_module_4_1_7 database update FAILED. Query: '.$query);
            }
        } catch (\Exception $e) {
            $success = false;
            MMOLogger::getInstance()->critical('upgrade_module_4_1_7 database update FAILED. Query: '.$query.' Error: '.$e->getMessage());
        }
    }

    if (!$success) {
        MMOLogger::getInstance()->info('upgrade_module_4_1_7 FAILED');
    }

    $success = true;
    $filesWithoutProcess = [];

    try {
        $filesWithoutProcess = MMOFile::getAllFilesNotProcessed();
    } catch (\Exception $e) {
        $success = false;
    }

    foreach ($filesWithoutProcess as $file) {
        try {
            MMOImport::preProcessInformation($file['name']);
        } catch (\Exception $e) {
            $success = false;
        }
    }

    if (!$success) {
        MMOLogger::getInstance()->info('upgrade_module_4_1_7 FAILED');
    }

    return true;
}
