<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use MIP\PrestaShop\MMOLogger;

/**
 * @param \Module $module
 */
function upgrade_module_2_3_3($module)
{
    require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

    try {
        unlink(_PS_MODULE_DIR_.\Ps_Mmoconnector::MODULE_NAME.'/images.php');
    } catch (\Exception $e) {
        MMOLogger::getInstance()->critical('upgrade_module_2_3_3 unlink images.php failed. '.$e->getMessage());
    }

    MMOLogger::getInstance()->info('upgrade_module_2_3_3 SUCCESS');

    return true;
}
