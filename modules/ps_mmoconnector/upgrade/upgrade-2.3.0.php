<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use MIP\PrestaShop\MMOLogger;
use MIP\PrestaShop\MMOSetup;

/**
 * @param \Module $module
 */
function upgrade_module_2_3_0($module)
{
    require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

    try {
        MMOSetup::removeDirectory(_PS_MODULE_DIR_.\Ps_Mmoconnector::MODULE_NAME.'/vendor/guzzlehttp');
    } catch (\Exception $e) {
        MMOLogger::getInstance()->critical('upgrade_module_2_3_0 removeDirectory guzzlehttp failed. '.$e->getMessage());
    }

    try {
        MMOSetup::removeDirectory(_PS_MODULE_DIR_.\Ps_Mmoconnector::MODULE_NAME.'/vendor/psr/http-message');
    } catch (\Exception $e) {
        MMOLogger::getInstance()->critical('upgrade_module_2_3_0 removeDirectory http-message failed. '.$e->getMessage());
    }

    MMOLogger::getInstance()->info('upgrade_module_2_3_0 SUCCESS');

    return true;
}
