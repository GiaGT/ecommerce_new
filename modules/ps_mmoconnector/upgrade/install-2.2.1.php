<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use MIP\PrestaShop\MMOSetup;
use MIP\PrestaShop\MMOLogger;

/**
 * @param \Module $module
 */
function upgrade_module_2_2_1($module)
{
    require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

    MMOSetup::fixPermissions(_PS_MODULE_DIR_.'ps_mmoconnector');

    MMOLogger::getInstance()->info('upgrade_module_2_2_1 SUCCESS');

    return true;
}
