<?php

$sql[] = "ALTER TABLE `"._DB_PREFIX_."mmo_connector_product_map` ADD `message_version` DATETIME DEFAULT NULL;";
$sql[] = "UPDATE ps_mmo_connector_product_map SET message_version = NULL;";

return $sql;