<?php

$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_product_image_url` 
            (
                `id_product` INT(11) UNSIGNED NOT NULL,
                `id_combination` INT(11) UNSIGNED NOT NULL,
                `id_image` INT(11) UNSIGNED NULL DEFAULT NULL,
                INDEX `id_product` (`id_product`),
                INDEX `id_combination` (`id_combination`),
                INDEX `id_image` (`id_image`)
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci';";

return $sql;