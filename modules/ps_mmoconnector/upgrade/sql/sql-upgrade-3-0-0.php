<?php

$sql[] = "ALTER TABLE `"._DB_PREFIX_."mmo_connector_product_url` ALTER `id_lang` DROP DEFAULT;";
$sql[] = "ALTER TABLE `"._DB_PREFIX_."mmo_connector_product_url` DROP PRIMARY KEY, ADD PRIMARY KEY (`id_product`, `id_lang`);";
$sql[] = "ALTER TABLE `"._DB_PREFIX_."mmo_connector_product_map` ADD `message_version` DATETIME DEFAULT NULL;";

return $sql;