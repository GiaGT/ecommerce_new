<?php

include_once __DIR__.'/../../config/config.inc.php';

class BestSellers
{

	public function __construct()
	{

	}

	public function processBestSellers(){
		$bestSellers = $this->getBestSellers();

		if(empty($bestSellers)){
			echo "No se ha obtenido ningún valor";
			die();
		}

		$this->vaciarOldBestSellers();
		$formatBestSellers = $this->generateFormatSql($bestSellers);

		$this->insertBestSellers($formatBestSellers);
	}

	private function vaciarOldBestSellers(){
		$sql = 'TRUNCATE '._DB_PREFIX_.'product_sale;';
		$result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);
	}

	private function getBestSellers(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,'https://www.bigbuy.eu/service/product/returnTopVentas/1000');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$response = curl_exec($ch);
		$topVentas = json_decode($response);
		curl_close($ch);

		return $topVentas;
	}

	private function generateFormatSql($topVentas){
		$topSeller = "";
		$i = 0;
		foreach ($topVentas as $top) {
			if ($i == 0) {
				$topSeller = "'" . $top->reference . "'";
			} else {
				$topSeller = $topSeller . ", '" . $top->reference . "'";
			}
			$i++;
		}

		return $topSeller;
	}

	private function insertBestSellers($topSeller){
		$sql = 'SELECT `id_product`, `quantity`'
		       . 'FROM '._DB_PREFIX_.'product pro '
		       . 'WHERE pro.reference IN ('.$topSeller.') LIMIT 100;';
		$result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

		foreach ($result as $row) {
			$id_product = (int)$row['id_product'];
			$quantity = (int)$row['quantity'];

			\Db::getInstance()->insert('product_sale', array(
				'id_product' => (int) $id_product,
				'quantity' => (int) $quantity,
				'sale_nbr' => 3,
			));
		}
	}
}

class Featured
{

	public function __construct()
	{

	}

	public function processFeatured(){
		$featured = $this->getFeatured();

		if(empty($featured)){
			echo "No se ha obtenido ningún valor";
			die();
		}

		$formatFeatured = $this->generateFormatSql($featured);
		$this->insertFeatured($formatFeatured);
	}

	private function getFeatured(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,'https://www.bigbuy.eu/service/product/returnProductsRecommened/500');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$response = curl_exec($ch);
		$topVentas = json_decode($response);
		curl_close($ch);

		return $topVentas;
	}

	private function generateFormatSql($featured){
		$formatFeatured = "";
		$i = 0;
		foreach ($featured as $fea) {
			if ($i == 0) {
				$formatFeatured = "'" . $fea->reference . "'";
			} else {
				$formatFeatured = $formatFeatured . ", '" . $fea->reference . "'";
			}
			$i++;
		}

		return $formatFeatured;
	}

	private function insertFeatured($featured){
		$sql = 'SELECT `id_product`, `quantity`'
		       . 'FROM '._DB_PREFIX_.'product pro '
		       . 'WHERE pro.reference IN ('.$featured.') LIMIT 50;';
		$result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

		foreach ($result as $row) {
			$id_product = (int)$row['id_product'];

			\Db::getInstance()->insert('category_product', array(
				'id_category' => (int) 2,
				'id_product' => (int) $id_product,
				'position' => 0,
			));
		}
	}
}

$bestSellers = new BestSellers();
$bestSellers->processBestSellers();

$featured = new Featured();
$featured->processFeatured();