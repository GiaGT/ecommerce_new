<?php

require_once dirname(dirname(__DIR__)).'/vendor/autoload.php';
require_once dirname(dirname(__DIR__)).'/ps_mmoconnector.php';

use MIP\PrestaShop\MMOLogger;
use MIP\PrestaShop\MMOFile;
use MIP\PrestaShop\MMOImport;
use MIP\PrestaShop\MMOImportProcess;
use MIP\PrestaShop\MMOLang;
use MIP\PrestaShop\MMOTaxesRules;
use MIP\PrestaShop\MMOProduct;
use MIP\PrestaShop\MMOOrder;
use MIP\PrestaShop\MMOControl;
use MIP\PrestaShop\MMOCategory;
use MIP\PrestaShop\MMOCarrier;
use MIP\PrestaShop\Model\MMOStatusReport;

$controller = new MMOConnector();
$controller->processAction();

/**
 * FrontController ps_mmoconnectorModuleFrontController
 *
 * Url format:
 *
 * https://myshop.com/modules/ps_mmoconnector/controllers/front/MMOConnector.php&messageType=PRODUCT&operationType=LIST
 */
class MMOConnector
{
    const SIGN_ALGORITHM_SHA256 = 'sha256';

    protected $messageType;
    protected $operationType;
    protected $accessToken;
    protected $sku;
    protected $message;
    protected $signature;
    protected $version;
    protected $headers;

    private $idFile;
    private $fileDir;
    private $fileName;
    private $state = true;
    private $typeState = '';

    /**
     * Processes external requests
     */
    public function processAction()
    {
        try {
            if (!defined('MMOCONECTOR_WEBSERVICE_KEY_ID')) {
                $psmmo = new \ps_mmoconnector();
                $autKey = $psmmo->getKeyWebservice();
                define('MMOCONECTOR_WEBSERVICE_KEY_ID', $autKey);
            }

            /**
             * Comprobamos que el el metodo de la petición sea valido. Si no es validas cortamos la ejecución devolviendo un error 400
             */
            $this->checkMethod();
            $this->headers = getallheaders();

            /**
             * Comprobamos que las cabeceras de la peticion sean valiidas. Si no son validas cortamos la ejecución devolviendo un error 400
             */
            $this->sanitizeHeaders();
            $this->checkHeaders();

            $this->messageType = \Tools::getValue('messageType');
            $this->operationType = \Tools::getValue('operationType');
            $this->accessToken = $this->headers['Access-Token'];
            $this->signature = $this->headers['X-Body-Signature'];
            $this->version = $this->headers['X-Version'];
            $this->sku = \Tools::getValue('sku');
            $this->message = @file_get_contents('php://input');

            MMOLogger::getInstance()->debug("Petición recibida {$this->messageType} {$this->operationType}");

            /**
             * Comprobamos el token de seguridad. Si no es valido cortamos la ejecución devolviendo un error 400
             */
            $this->checkSecurityToken();

            /**
             * Comprobamos que concida la firma del mensaje con lo recibido. Si no coincide cortamos la ejecución devolviendo un error 403
             */
            $this->checkSignature();

            /**
             * Ejecutamos la acción si es valida, de lo contrario cortamos la ejecución devolviendo un error 400
             */
            $response = $this->executeAction();

            $this->JsonResponse(200, 'Ok', $response);
        }
        catch (\Exception $e){
            MMOLogger::getInstance()->critical('Error de sistema. '.$e->getMessage(), $e->getTrace());
            $this->JsonResponse(500, 'System error');
        }
    }

    /**
     * Comprueba que la petición sea de tipo POST
     *
     * Si no es validas cortamos la ejecución devolviendo un error 400
     */
    private function checkMethod()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            $message = "El método de la petición ({$_SERVER["REQUEST_METHOD"]}) no es valido.";
            $code = 400;

            $this->JsonResponse($code, $message);
        }
    }

    /**
     * Comprueba que la petición contenga las cabeceras.
     *
     * La petición deberá contener siempre las siguientes cabeceras:
     *      - Access-Token
     *      - X-Body-Signature
     *      - X-Version
     *
     * Si no contiene las cabeceras necesarias cortamos la ejecución devolviendo un error 400
     */
    private function checkHeaders()
    {
        if (empty($this->headers['Access-Token'])) {
            $message = 'El parámetro de la cabecera Access-Token no esta definido.';
            $code = 400;

            $this->JsonResponse($code, $message);
        }

        if (empty($this->headers['X-Body-Signature'])) {
            $message = 'No se encontró la firma del mensaje.';
            $code = 400;

            $this->JsonResponse($code, $message);
        }

        if (empty($this->headers['X-Version'])) {
            $message = 'No se encontró la version del mensaje.';
            $code = 400;

            $this->JsonResponse($code, $message);
        }
    }

    /**
     * Función para comprobar el token de seguridad
     *
     * Si no es valido cortamos la ejecución devolviendo un error 400
     */
    private function checkSecurityToken()
    {
        $accessToken = \Configuration::get('MMO_CONNECTOR_ACCESS_TOKEN');

        if ($this->accessToken !== $accessToken) {
            $message = "Access token incorrecto '{$this->accessToken}'.";
            $code = 400;

            MMOLogger::getInstance()->error(
                "$code - $message",
                ['Access-Token' => $accessToken, 'post' => $_POST, 'get' => $_GET, 'server' => $_SERVER]
            );

            $this->JsonResponse($code, $message);
        }
    }

    /**
     * Comprobamos que concida la firma del mensaje con lo recibido.
     *
     * Si no coincide cortamos la ejecución devolviendo un error 403
     */
    private function checkSignature()
    {
        $secretKey = \Configuration::get('MMO_CONNECTOR_SECRET_KEY');
        $signature = $this->sign($this->message, $secretKey);

        if ($this->signature !== $signature) {
            $message = "Firma del mensaje incorrecta {$this->signature}.";// correcta {$signature}.";

            $code = 403;

            MMOLogger::getInstance()->error(
                "$code - $message",
                ['X-Body-Signature' => $signature, 'post' => $_POST, 'get' => $_GET, 'server' => $_SERVER]
            );

            $this->JsonResponse($code, $message);
        }
    }

    /**
     * Función para comprobar que operación hemos de ejecutar dependiendo de la petición
     *
     * Si la operacion no es valida cortamos la ejecución devolviendo un error 400
     */
    private function executeAction()
    {
        switch ($this->messageType) {
            case 'PRODUCT':
                return $this->executeProductAction();
            case 'ORDER':
                return $this->executeOrderAction();
            case 'SUBMISSION':
                return $this->executeSubmissionAction();
            case 'SYSTEM':
                return $this->executeSystemAction();
            case 'CATEGORY':
                return $this->executeCategoryAction();
            default:
                $this->JsonResponse(400, 'La operación solicitada no es valida.');
        }
    }


    /**
     * Función para comprobar que accíon hemos de ejecutar dependiendo de la petición
     *
     * Si la accíon no es valida cortamos la ejecución devolviendo un error 400
     */
    private function executeProductAction()
    {
        switch ($this->operationType) {
            case 'LIST':
            case 'UPDATE':
                return $this->listProduct();
            case 'GET':
                return $this->getProducts();
            case 'DELETE':
                return $this->listProduct();
            default :
                $this->JsonResponse(400, 'La acción de producto no es valida.');
        }
    }

    /**
     * Función para comprobar que accíon hemos de ejecutar dependiendo de la petición
     *
     * Si la accíon no es valida cortamos la ejecución devolviendo un error 400
     */
    private function executeOrderAction()
    {
        switch ($this->operationType) {
            case 'GET':
                return $this->listOrders();
            case 'UPDATE' :
                return $this->modifyOrders();
            case 'DELETE' :
            default:
                $this->JsonResponse(400, 'La acción de pedido no es valida.');
        }
    }

    /**
     * Función para comprobar que accíon hemos de ejecutar dependiendo de la petición
     *
     * Si la accíon no es valida cortamos la ejecución devolviendo un error 400
     */
    private function executeSubmissionAction()
    {
        switch ($this->operationType) {
            case 'LIST':
                return $this->listSubmissions();
            default :
                $this->JsonResponse(400, 'La acción del estado del envío no es valida.');
        }
    }

    /**
     * Función para comprobar que accíon hemos de ejecutar dependiendo de la petición
     *
     * Si la accíon no es valida cortamos la ejecución devolviendo un error 400
     */
    private function executeSystemAction()
    {
        switch ($this->operationType) {
            case 'TAXES':
                return $this->getListTaxes();
            case 'LANGUAGES':
                return $this->getListLanguages();
            case 'UPGRADE':
                return [\Ps_Mmoconnector::upgrade()];
            case 'ACCOUNTINFO':
                $mmoManager = new MMOControl();
                return $mmoManager->getAccountInformation();
            case 'STATUSREPORT':
                return $this->getStatusReport();
            case 'PERMISSION':
                return $this->setPermissionType();
            default :
                $this->JsonResponse(400, 'La acción del sistema no es valida.');
        }
    }

    private function setPermissionType()
    {
        $request = json_decode($this->message, true);

        if (empty($request['ActionType'])) {
            $this->JsonResponse(400, 'El campo ActionType no esta definido.');
        }

        switch ($request['ActionType']) {
            case 'ALLOW':
            case 'DENY':
                return $this->setPermission();
            default :
                $this->JsonResponse(400, 'La acción del sistema no es valida.');
        }
    }

    private function setPermission()
    {
        $request = json_decode($this->message, true);

        if (empty($request['Permission'])) {
            $this->JsonResponse(400, 'El campo Permission no esta definido.');
        }

        switch ($request['Permission']) {
            case 'INSTALLMODULES':
                $mmoManager = new MMOControl();
                return $mmoManager->setInstallModulePermissions($request['ActionType']);
            default :
                $this->JsonResponse(400, 'La acción del sistema no es valida.');
        }
    }

    private function getStatusReport()
    {
        $response = new MMOStatusReport();
        $timezone = new \DateTimeZone('UTC');
        $now = new \DateTime('now', $timezone);
        $response->RequestDate = $now->format(DATE_W3C);

        $taxes = MMOTaxesRules::process();
        if (is_string($taxes)) {$taxes = ['Taxes' => []];}
        $response->Taxes = $taxes['Taxes'];
        $languages = MMOLang::process();
        if (is_string($languages)) {$languages = ['Languages' => []];}
        $response->Languages = $languages['Languages'];

        $mmoManager = new MMOControl();
        $response->AccountInfo = $mmoManager->getAccountInfo();
        $mmoProduct = new MMOProduct();
        $response->ProductsReport = $mmoProduct->getProductReport();
        $response->ProductVariationsReport = $mmoProduct->getProductVariationReport();
        $mmoCategory = new MMOCategory();
        $response->CategoriesReport = $mmoCategory->getCategoryReport();
        $mmoFile = new MMOFile();
        $response->FilesReport = $mmoFile->getFileReport();

        return json_decode(json_encode($response), true);
    }

    /**
     * Función para comprobar que accíon hemos de ejecutar dependiendo de la petición
     *
     * Si la accíon no es valida cortamos la ejecución devolviendo un error 400
     */
    private function executeCategoryAction()
    {
        switch ($this->operationType) {
            case 'LIST':
            case 'UPDATE':
                return $this->listCategory();
            case 'GET':
                /**
                 * @TODO - Añadir la implementación
                 */
            default :
                $this->JsonResponse(400, 'La acción del sistema no es valida.');
        }
    }

    /**
     * Función para obtener le estado de la ejecución de un fichero pasado como parámetro
     */
    private function listSubmissions()
    {
        $response = ['SubmissionProducts' => []];
        $request = json_decode($this->message, true);

        if (empty($request['Submission']) || empty($request['Submission']['SubmissionID'])) {
            $this->JsonResponse(400, 'El mensaje no es valido.', $response);
        }

        $submissionId = $request['Submission']['SubmissionID'];
        $infoStateFile = MMOFile::getStateFileImport($submissionId);
        $response = MMOImportProcess::infoProcessMMOImportProcess($submissionId);

        MMOLogger::getInstance()->debug("Requested SubmissionID $submissionId", [$infoStateFile, $response]);

        $this->JsonResponse($infoStateFile->state, $infoStateFile->message, $response);
    }

    /**
     * @return array|string
     */
    private function getListLanguages()
    {
        $response = ['Languages' => []];
        $languages = MMOLang::process();

        if (is_string($languages)) {
            $this->JsonResponse(400, $languages, $response);
        }

        if (empty($languages)) {
            $this->JsonResponse(404, 'La tienda no ha ningún devuelvo lenguaje', $response);
        }

        $response = $languages;

        return $response;
    }

    /**
     * @return array|string
     */
    private function getListTaxes()
    {
        $response = ['Taxes' => []];
        $taxes = MMOTaxesRules::process();

        if (is_string($taxes)) {
            $this->JsonResponse(400, $taxes, $response);
        }

        if (empty($taxes)) {
            $this->JsonResponse(404, 'La tienda no ha devuelto ninguna tasa.', $response);
        }

        $response = $taxes;

        return $response;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function listProduct()
    {
        $this->idFile = uniqid();
        $this->fileDir = _PS_MODULE_DIR_.'ps_mmoconnector/files';
        $this->fileName = $this->fileDir.'/'.$this->idFile.'.json';

        try {
            $oldPermissions = umask(0000);

            if (!@mkdir($this->fileDir) && !is_dir($this->fileDir)) {
                $this->JsonResponse(500, "No se pudo crear el directorio de ficheros en {$this->fileDir}");
            }

            @chmod($this->fileDir, 0775);
            umask($oldPermissions);

            file_put_contents($this->fileName, $this->message);
        } catch (\Exception $e) {
            $this->state = false;
            $this->typeState = $e->getMessage();
            MMOLogger::getInstance()->critical("List product file creation failed. {$e->getMessage()}", [$e->getTrace()]);
        }

        $this->state = MMOFile::insertMMOConectorFileLog($this->idFile, $this->version);
        $mmoFile = MMOFile::getFile($this->idFile);

        if (null === $mmoFile->name) {
            $this->JsonResponse(500, "File entry $this->idFile was not found in DB.");
        }

        MMOLogger::getInstance()->info("Add file to process list.");
        MMOImport::preProcessInformation($mmoFile->name);
        MMOLogger::getInstance()->info("Finish add file to process list.");

        $result = ['Submission' => ['SubmissionID' => $this->idFile]];

        return $result;
    }

    /**
     * Función para almacenar la información en el fichero json generado.
     */
    private function listCategory()
    {
        $request = json_decode($this->message, true);

        if (is_string($request)) {
            $this->JsonResponse(400, 'There is an error in the categories', array(false));
        }

        try {
            MMOImport::getCategories($request);
        }catch(\Exception $e){
            $this->JsonResponse(400, 'Error processing Categories', array(false));
        }

        return array(true);
    }

    /**
     * Función para obtener las URLs de un producto y sus imagenes.
     */
    private function getProducts()
    {
        $request = json_decode($this->message, true);
        $date = $request['CreateTimeFrom'];
        $languages = \Language::getLanguages(true);
        $languageIsoCodesById = array_column($languages, 'iso_code', 'id_lang');

        $productUrls = MMOProduct::getUrlsProducts($date);
        $productImageUrls = MMOProduct::getUrlsImages($date);
        $products = array();

        foreach ($productUrls as $productUrl) {
            $products[$productUrl['id_product']]['ProductID'] = $productUrl['id_product'];
            $productUrlData = array(
                'IsoCode' => $languageIsoCodesById[$productUrl['id_lang']],
                'Link' => $productUrl['url'],
                'Version' => 1,
                'AttributeID' => $productUrl['id_combination'],
            );
            $products[$productUrl['id_product']]['ProductUrls'][] = $productUrlData;
        }

        foreach ($productImageUrls as $prodcutImageUrl) {
            $products[$prodcutImageUrl['id_product']]['ProductID'] = $prodcutImageUrl['id_product'];
            $productImageUrlData = array(
                'ImageURL' => $prodcutImageUrl['url'],
                'Order' => 1,
                'Cover' => $prodcutImageUrl['cover'],
            );
            $products[$prodcutImageUrl['id_product']]['Images'][] = $productImageUrlData;
        }

        return array('Products' => array_values($products));
    }

    /**
     * Función para obtener la lista de todos los pedidos de la tienda.
     */
    private function listOrders()
    {
        $request = json_decode($this->message, true);
        $orders = array('Orders' => []);

        if (empty($request['CreateTimeFrom'])) {
            $this->JsonResponse(400, 'El campo CreateTimeFrom no esta definido.', $orders);
        }

        $date = $request['CreateTimeFrom'];
        $ordersWithoutShipping = MMOOrder::getOrdersWithoutShipping($date);

        if (empty($ordersWithoutShipping)) {
            $this->JsonResponse(404, 'La tienda no ha devuelto ningún pedido.', $orders);
        }

        foreach ($ordersWithoutShipping as $key => $value) {
            $orderId = (int)$value['id_order'];
            $infoMMOOrder = MMOOrder::process($orderId);
            $orders['Orders'][] = $infoMMOOrder;
            MMOOrder::checkOrderAsProcess($orderId);
        }

        return $orders;
    }

    /**
     * Función para modificar la información de un pedido
     */
    private function modifyOrders()
    {
        $request = json_decode($this->message, true);
        $responseModifyOrders = ['Orders' => []];

        MMOLogger::getInstance()->info(
            'Modify Order - ' .date('Y-m-d H:i:s') ,
            ['Request Orders' => $request['Orders']]
        );

        foreach ($request["Orders"] as $order) {
            $orderStateIdNew = $this->stateOrderMap($order["State"]);

            if(!$orderStateIdNew){
                MMOLogger::getInstance()->error("Bad Order Status - " .date('Y-m-d H:i:s') , $order);
                $this->JsonResponse(400, "Bad Order Status", $order["State"]);
            }

            $orderCarrier = $this->modifyCarrierOrder($order);

            if(!$orderCarrier){
                MMOLogger::getInstance()->info("Order Carrier not modify - " .date('Y-m-d H:i:s') , $orderCarrier);
            }

            $result = MMOOrder::changeStateOrder($order, $orderStateIdNew, $order['State']);
            $responseModifyOrders["Orders"][] = $result;
        }

        MMOLogger::getInstance()->info(
            'Modify Order Response- ' .date('Y-m-d H:i:s') ,
            ['Response Modify Orders' => $responseModifyOrders]
        );

        return $responseModifyOrders;
    }

    private function modifyCarrierOrder($order)
    {
        $idLang = $this->context->language->id;
        $carrierDataResponse = MMOCarrier::getCarrierDataRequest();
        if ($carrierDataResponse['status_code'] != 200) {
            return false;
        }

        $orderData = MMOOrder::getOrder($order["OrderID"]);

        if(!MMOCarrier::isNativeCarrier($idLang, $orderData)){
            return false;
        }

        $carriersApiMip = json_decode($carrierDataResponse['response'], true);
        $idCarrier = MMOCarrier::getCarrier($carriersApiMip, $order["ShippingService"]);

        if(!$idCarrier ){
            return false;
        }

        MMOCarrier::updateOrderCarrier($orderData, $idCarrier);
        MMOOrder::updateCarrierOrder($orderData, $idCarrier);

        return true;
    }

    /**
     * Devuelve la firma del los datos en BASE64 calculando el sha256
     *
     * @param string $data
     * @param string $secretKey
     * @return string los datos firmadon en BASE64
     * @throws Exception
     */
    private function sign($data, $secretKey)
    {
        return base64_encode(
            hash_hmac(self::SIGN_ALGORITHM_SHA256, $data, $secretKey, true)
        );
    }

    /**
     * Devuelve una respuesta en formato JSON y corta la ejecución.
     *
     * @param integer $code
     * @param string $message
     * @param array $data
     */
    private function JsonResponse($code, $message = '', array $data = [])
    {
        MMOLogger::getInstance()->debug(
            "$code - $message",
            ['Data' => $data, 'Post' => $_POST, 'Get' => $_GET, 'Server' => $_SERVER]
        );

        $result['Code'] = $code;
        $result['Message'] = $message;
        $result['Data'] = $data;

        header('Content-Type: application/json');

        $response = json_encode($result, JSON_UNESCAPED_UNICODE);

        if ($code > 399) {
            MMOLogger::getInstance()->alert("$code - $message RESPONSE: $response");
        }
        else {
            MMOLogger::getInstance()->debug("$code - $message RESPONSE: $response");
        }

        die($response);
    }

    /**
     * @param $stateOrder
     * @return bool|int
     */
    private function stateOrderMap($stateOrder)
    {
        switch ($stateOrder) {
            case MMOOrder::ORDER_STATUS_CANCELLED:
                return (int)\Configuration::get('PS_OS_CANCELED');
            case MMOOrder::ORDER_STATUS_DELIVERED:
                return (int)\Configuration::get('PS_OS_DELIVERED');
            case MMOOrder::ORDER_STATUS_IN_PROCESS:
                $resultGetValidateOrderStatus = MMOOrder::getValidateOrderStatus();
                $selectionStatus = MMOOrder::formatValidateOrderStatus($resultGetValidateOrderStatus);

                return (int)$selectionStatus->in_process;
            case MMOOrder::ORDER_STATUS_SENT:
                $resultGetValidateOrderStatus = MMOOrder::getValidateOrderStatus();
                $selectionStatus = MMOOrder::formatValidateOrderStatus($resultGetValidateOrderStatus);

                return (int)$selectionStatus->sent;
            case MMOOrder::ORDER_STATUS_PENDING_REVIEW:
                return (int)\Configuration::get('PS_OS_PREPARATION');
            default:
                return false;
        }
    }

    private function sanitizeHeaders()
    {
        foreach ($this->headers as $header => $value) {
            $sanitizeHeader = str_replace(' ', '-', ucwords(strtolower(str_replace('-', ' ', $header))));
            $this->headers[$sanitizeHeader] = $value;
        }
    }
}