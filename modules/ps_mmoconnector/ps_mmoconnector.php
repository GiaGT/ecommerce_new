<?php

include_once __DIR__.'/../../config/config.inc.php';

use MIP\PrestaShop\MMOConnector;
use MIP\PrestaShop\MMOOrder;
use MIP\PrestaShop\MMOProduct;
use MIP\PrestaShop\MMOCombination;
use MIP\PrestaShop\MMOLogger;
use MIP\PrestaShop\MMOImport;
use MIP\PrestaShop\MMOCarrier;
use MIP\PrestaShop\MMOSetup;

class Ps_Mmoconnector extends \Module
{
    const MODULE_NAME = 'ps_mmoconnector';
    const MODULE_DIR = __DIR__;
    const MODULE_VERSION = '4.5.2';

    const API_BASE_URI = 'http://platform.bigbuy.eu';
    const API_CONNECTOR_TOKEN = 'MmQzMTk3YTg3NDRjZTNiNWY4YTMwZTMwNThiN2UyYTZiMzgyZDcwMTFjNTY3ZjkwNjg5OWQ3NzdiYWVlNzNjYQ';
    const MAX_FILE_PROCESS_BATCH = 10;

    private $templateFile;
    public static $supplier = 'BB';

    public function __construct()
    {
        $this->name = self::MODULE_NAME;
        $this->version = self::MODULE_VERSION;
        $this->author = 'BigBuy';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('ps_mmoconnector', array(), 'Modules.ps_mmoconnector');
        $this->description = $this->trans('ps_mmoconnector.', array(), 'Modules.ps_mmoconnector');
        $this->templateFile = 'module:ps_mmoconnector/ps_mmoconnector.tpl';
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        if (!parent::install()) {
            return false;
        }

        $this->installFixtures();
        $this->getWebserviceKey();
        $mmoManager = new MMOConnector();

        if (!$mmoManager->getSupplierId()) {
            $mmoManager->createSupplier();
        }

        $this->registerHook('actionOrderStatusPostUpdate');
        $this->registerHook('actionOrderStatusUpdate');
        $this->registerHook('actionObjectDeleteAfter');
        $this->registerHook('actionObjectAddAfter');
        $this->registerHook('actionObjectUpdateAfter');

        MMOSetup::install();

        return true;
    }

    public function uninstall()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        if (!parent::uninstall()) {
            return false;
        }

        $this->deleteWebserviceKey();
        MMOConnector::uninstall();
        $this->unregisterHook('actionOrderStatusPostUpdate');
        $this->unregisterHook('actionOrderStatusUpdate');
        $this->unregisterHook('actionObjectDeleteAfter');
        $this->unregisterHook('actionObjectAddAfter');
        $this->unregisterHook('actionObjectUpdateAfter');

        MMOSetup::uninstall();

        return true;
    }

    private function installFixtures()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        MMOConnector::install();
    }

    /**
     * @return \WebserviceKey
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function createWebserviceKey()
    {
        $webservice_key = new \WebserviceKey();
        $webservice_key->description = 'MMOCONECTOR';
        $webservice_key->key = $this->getRandomKey();
        $webservice_key->active = true;
        $webservice_key->add();
        $this->setPermissions($webservice_key);
        \Configuration::updateValue('MMOCONECTOR_WEBSERVICE_KEY_ID', $webservice_key->id);

        return $webservice_key;
    }

    /**
     *
     */
    private function deleteWebserviceKey()
    {
        $key_id = (int)\Configuration::updateValue('MMOCONECTOR_WEBSERVICE_KEY_ID', 0);

        if (!$key_id) {
            return;
        }

        $webservice_key = new \WebserviceKey($key_id);

        if (\Validate::isLoadedObject($webservice_key)) {
            $webservice_key = new \WebserviceKey();
            $webservice_key->delete();
        }

        \Configuration::updateValue('MMOCONECTOR_WEBSERVICE_KEY_ID', 0);
    }

    /**
     *
     */
    private function updateWebserviceKey()
    {
        $key_id = (int)\Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');

        if (!$key_id) {
            return;
        }

        $webservice_key = new \WebserviceKey($key_id);

        if (!\Validate::isLoadedObject($webservice_key)) {
            $this->createWebserviceKey();
            return;
        }

        $webservice_key->key = $this->getRandomKey();
        $webservice_key->save();
    }

    /**
     * @return bool|string
     */
    public function resetWebserviceKey()
    {
        try {
            $this->updateWebserviceKey();

            return true;
        } catch (\Exception $e) {
            MMOLogger::getInstance()->critical('No se pudo resetear el acceso al WebService. '.$e->getMessage());

            return false;
        }
    }

    private function getRandomKey($length = 32)
    {
        $random = '';
        $chars = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
        $cantidadCaracteres = strlen($chars);
        $cantidadCaracteres--;

        for ($x = 1; $x <= $length; $x++) {
            $randomCharacterIndex = mt_rand(0, $cantidadCaracteres);
            $random .= substr($chars, $randomCharacterIndex, 1);
        }

        return $random;
    }

    public function renderForm()
    {
        $accessToken = \Configuration::get('MMO_CONNECTOR_ACCESS_TOKEN');
        $secretKey = \Configuration::get('MMO_CONNECTOR_SECRET_KEY');

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Settings', array(), 'Admin.Global'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('ACCESS TOKEN'),
                        'name' => 'mmo_connector_access_token',
                        'class' => 'col-sm-2',
                        'required' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('SECRET KEY'),
                        'name' => 'mmo_connector_secret_key',
                        'class' => 'col-sm-2',
                        'required' => true,
                    ),
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions'),
                ),
            ),
        );

        $helper = new \HelperForm();
        $helper->module = $this;
        $helper->identifier = 'mmoconnector';
        $helper->token = \Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = \AdminController::$currentIndex.'&configure='.$this->name;
        $helper->submit_action = 'submitMMOConnectorAccess';
        $helper->show_toolbar = true;
        $helper->fields_value = array(
            'mmo_connector_access_token' => $accessToken,
            'mmo_connector_secret_key' => $secretKey,
        );

        return $helper->generateForm(array($fields_form));
    }

    public function sendEmail()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
        global $smarty;

        $smarty->assign(
            array(
                'sendEmail' =>(int)\Configuration::get('MMO_CONNECTOR_SEND_EMAIL'),
            )
        );
        return $smarty->fetch(_PS_MODULE_DIR_.'ps_mmoconnector/views/templates/admin/sendEmailConfiguration.tpl');
    }

    public function getContent()
    {
        $content = $this->postProcess();
        $content .= $this->loadViewCronAdvert();
        $content .= $this->renderForm();
        $content .= $this->loadViewConfigureStateOrders();
        $content .= $this->loadViewConfigurationCashOndelivery();
        $content .= $this->loadViewConfigureCarriers();
        $content .= $this->sendEmail();

        return $content;
    }

    public function loadViewConfigureCarriers()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
        global $smarty;
        $carriersMip = array();

        $idLang = $this->context->language->id;
        $carriers = \Carrier::getCarriers($idLang);

        $carrierDataResponse = MMOCarrier::getCarrierDataRequest();

        if ($carrierDataResponse['status_code'] === 200) {
            $carriersMip = json_decode($carrierDataResponse['response'], true);
        }

        $carriersAssociation = json_decode(\Configuration::get('MMO_CONNECTOR_ASSOCIATION_CARRIERS'), true);

        $smarty->assign(
            array(
                'carriers' => $carriers,
                'carriersMip' => $carriersMip,
                'carriersAssociation' => $carriersAssociation,
            )
        );
        return $smarty->fetch(_PS_MODULE_DIR_.'ps_mmoconnector/views/templates/admin/configureCarriers.tpl');
    }

    public function loadViewConfigureStateOrders()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
        global $smarty;

        $idLang = $this->context->language->id;

        $orderStates = \OrderState::getOrderStates($idLang);

        $resultGetValidateOrderStatus = MMOOrder::getValidateOrderStatus();
        $selectionStatus = MMOOrder::formatValidateOrderStatus($resultGetValidateOrderStatus);

        $smarty->assign(
            array(
                'orderStates' => $orderStates,
                'form_action' => \AdminController::$currentIndex.'&configure='.$this->name.'&token='.\Tools::getAdminTokenLite(
                        'AdminModules'
                    ),
                'selectionStatus' => $selectionStatus,
            )
        );

        return $smarty->fetch(_PS_MODULE_DIR_.'ps_mmoconnector/views/templates/admin/configureStateOrders.tpl');
    }

    public function loadViewCronAdvert()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';
        global $smarty;

        $link = new \Link();
        $domainShop = rtrim($link->getBaseLink(), '/');
        $moduleDir = rtrim(__DIR__, '/');

        $smarty->assign(
            array(
                'phpCompatible' => version_compare(phpversion(),'5.6') < 0?false:true,
                'hasMogrify' => MMOConnector::hasMogrify(),
                'domainShop' => $domainShop,
                'moduleDir' => $moduleDir,
            )
        );

        return $smarty->fetch(_PS_MODULE_DIR_.'ps_mmoconnector/views/templates/admin/cronAdvert.tpl');
    }

    public function loadViewConfigurationCashOndelivery()
    {
        global $smarty;

        $modulesSelected = \Configuration::get('MMO_CONNECTOR_MODULES_COD');
        $paymentModules = \Module::getPaymentModules();
        $modulesCOD = [];

        foreach ($paymentModules as $key => $value) {
            $nameModule = \Module::getModuleName($value['name']);
            $modulesCOD[$value['name']][]= $nameModule;
        }

        $smarty->assign(
            array(
                'ModulesCOD' => $modulesCOD,
                'form_action' => \AdminController::$currentIndex.'&configure='.$this->name.'&token='.\Tools::getAdminTokenLite(
                        'AdminModules'
                    ),
                'selectionModules' => json_decode($modulesSelected, true),
            )
        );

        return $smarty->fetch(_PS_MODULE_DIR_.'ps_mmoconnector/views/templates/admin/configureCashOnDeliveryModule.tpl');
    }

    public function postProcess()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        if (\Tools::isSubmit('checkAndUpdate')) {
            self::upgrade();

            return $this->displayConfirmation(
                $this->trans('Module was updated successfully.', array(), 'Admin.Notifications.Success')
            );
        }

        if (\Tools::isSubmit('submitMMOConnectorAccess')) {
            $accessToken = \Tools::getValue('mmo_connector_access_token');
            $secretKey = \Tools::getValue('mmo_connector_secret_key');
            MMOConnector::updateAccessToken($accessToken);
            MMOConnector::updateSecretKey($secretKey);

            return $this->displayConfirmation(
                $this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success')
            );
        }

        if (\Tools::isSubmit('submitConfigurationStates')) {
            $initialStateOrderBigBuy = \Tools::getValue('initialStateOrderBigBuy');
            $inProcessStateOrder = \Tools::getValue('inProcessStateOrder');
            $sentStateOrder = \Tools::getValue('sentStateOrder');
            MMOOrder::updateValidateOrderStatus($initialStateOrderBigBuy, $inProcessStateOrder, $sentStateOrder);

            return $this->displayConfirmation(
                $this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success')
            );
        }

        if (\Tools::isSubmit('submitModulesCOD')) {
            $modules = \Tools::getValue('modulesCOD', []);
            MMOOrder::updateModulesCOD($modules);

            return $this->displayConfirmation(
                $this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success')
            );
        }

        if (\Tools::isSubmit('submitCarriers')) {
            $carriersNameIndexedById = \Tools::getValue('carriers');

            if(!$this->checkDuplicateShopCarrierAssociation($carriersNameIndexedById)){
                return $this->displayError('Los transportistas sólo pueden ser asociados una sola vez', array(), 'Admin.Notifications.Error');
            }

            $carriersFormat = json_encode($carriersNameIndexedById);
            MMOConnector::updateAssociationCarriers($carriersFormat);

            return $this->displayConfirmation(
                $this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success')
            );
        }

        if (\Tools::isSubmit('submitSendEmailConfiguration')) {
            $sendEmailConfiguration = \Tools::getValue('send_email_sandbox');
            \Configuration::updateValue('MMO_CONNECTOR_SEND_EMAIL', $sendEmailConfiguration);
        }

        return '';
    }

    public function checkDuplicateShopCarrierAssociation(array $carriersNameIndexedById)
    {
        $originalCarrierNameIndexedById = $carriersNameIndexedById;

        foreach ($carriersNameIndexedById as $carrierId => $carrierName) {
            unset($originalCarrierNameIndexedById[$carrierId]);

            if (empty($carrierName)) {
                $originalCarrierNameIndexedById = $carriersNameIndexedById;
                continue;
            }

            if (in_array($carrierName, $originalCarrierNameIndexedById)) {
                return false;
            }
        }

        return true;
    }

    public function getKeyWebservice()
    {
        $keyWebService = $this->getSignupLink();

        return $keyWebService;
    }

    private function getSignupLink()
    {
        $webservice_key = $this->getWebserviceKeyCode();

        if ($webservice_key == '') {
            return '';
        }

        return $webservice_key;
    }

    private function getWebserviceKeyCode()
    {
        $webservice_key = $this->getWebserviceKey();

        return (null === $webservice_key ? '' : $webservice_key->key);
    }

    private function getWebserviceKey()
    {
        $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID', null);

        if ($key_id == '') {
            $webservice_key = $this->createWebserviceKey();
        } else {
            $webservice_key = new \WebserviceKey($key_id);
            if ($webservice_key->key == '') {
                return $this->createWebserviceKey();
            }
        }

        // Set appropriate permissions
        $this->setPermissions($webservice_key);
        // Make sure htaccess is properly setup.
        \Tools::generateHtaccess();
        // Enable Webservice
        \Configuration::updateValue('PS_WEBSERVICE', '1');

        return $webservice_key;
    }

    private function setPermissions($webserviceKey)
    {
        $sql = '';
        $permissionsToSet = array();
        $resources = \WebserviceRequest::getResources();

        foreach ($resources as $resourceName => $resource) {
            if ($resourceName === 'products' || $resourceName === 'categories'
                || $resourceName === 'tags' || $resourceName === 'manufacturers'
                || $resourceName === 'images' || $resourceName === 'image_types'
                || $resourceName === 'stocks' || $resourceName === 'stock_availables'
                || $resourceName === 'combinations' || $resourceName === 'product_options'
                || $resourceName === 'product_option_values' || $resourceName === 'languages'
                || $resourceName === 'tax_rule_groups' || $resourceName === 'tax_rules'
                || $resourceName === 'taxes' || $resourceName === 'addresses'
                || $resourceName === 'orders' || $resourceName === 'currencies' || $resourceName === 'carriers'
                || $resourceName === 'customers'
            ) {
                $permissionsToSet[] = array('method' => 'GET', 'resource' => $resourceName);
                $permissionsToSet[] = array('method' => 'PUT', 'resource' => $resourceName);
                $permissionsToSet[] = array('method' => 'POST', 'resource' => $resourceName);
                $permissionsToSet[] = array('method' => 'DELETE', 'resource' => $resourceName);
                $permissionsToSet[] = array('method' => 'HEAD', 'resource' => $resourceName);
            }
        }

        foreach ($permissionsToSet as $permission) {
            if ($this->permissionExists($webserviceKey->id, $permission)) {
                continue;
            }

            if (empty($sql)) {
                $sql = 'INSERT INTO `'._DB_PREFIX_.'webservice_permission` (`id_webservice_permission` ,`resource` ,`method` ,`id_webservice_account`) VALUES ';
            }

            $sql .= '(NULL , \''.pSQL($permission['resource']).'\', \''.pSQL($permission['method']).'\', '.(int)$webserviceKey->id.'), ';
        }

        $sql = rtrim($sql, ', ');

        if (empty($sql)) {
            MMOLogger::getInstance()->info("Permissions were OK for {$webserviceKey->id}");

            return;
        }

        if(!\Db::getInstance()->execute($sql)) {
            MMOLogger::getInstance()->error("Could not set permissions for account {$webserviceKey->id}", $permissionsToSet);
        }
    }

    private function permissionExists($webserviceKeyId, $permission)
    {
        $sql = 'SELECT id_webservice_permission, id_webservice_account, resource, method  
                  FROM `'._DB_PREFIX_."webservice_permission` 
                  WHERE resource = '{$permission['resource']}' AND method = '{$permission['method']}' AND id_webservice_account = '{$webserviceKeyId}'";
        $result = \Db::getInstance()->getRow($sql, false);

        if (empty($result)) {
            return false;
        }

        return true;
    }

    //Hook para registrar el estado del pedido una vez se ha creado.
    public function hookActionObjectOrderStatusPostUpdate($params)
    {
        $this->checkStateOrder($params);
        return true;
    }

    //Hook para registrar el estado del pedido una vez se ha actualizado su estado
    public function hookActionOrderStatusUpdate($params)
    {
        $this->checkStateOrder($params);
        return true;
    }

    public function hookActionObjectProductAddAfter ($params)
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        $productURL = new MMOProduct();
        $productURL->getProductUrl($params['object']);
        $productURL->getImagesUrl($params['object']);

        return true;
    }

    public function hookActionObjectProductUpdateAfter ($params)
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        $productURL = new MMOProduct();
        $productURL->getProductUrl($params['object']);
        $productURL->getImagesUrl($params['object']);

        return true;
    }

    public function hookActionObjectDeleteAfter($params)
    {
    }

    protected function checkStateOrder($params)
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        if (isset($params['newOrderStatus'])) {
            $lastIdOrderState = $params['newOrderStatus']->id;
        } else {
            $lastIdOrderState = MMOOrder::getLastStateOrder($params['id_order']);
        }

        $order = new \stdClass;
        $order->id_order = $params['id_order'];
        $order->id_state_order = $lastIdOrderState;

        $resultGetValidateOrderStatus = MMOOrder::getValidateOrderStatus();
        $selectionStatus = MMOOrder::formatValidateOrderStatus($resultGetValidateOrderStatus);
        $checkSelectionStatus = MMOOrder::checkOrderWithSelectionOrderStatus($order, $selectionStatus);

        if ($checkSelectionStatus) {
            MMOOrder::addMmoConnectorOrderLog($order);
        }

        return true;
    }

    public function process()
    {
        require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

        $this->initializeWebservice();

        for ($processedFiles = 0; $processedFiles < self::MAX_FILE_PROCESS_BATCH; $processedFiles++) {
            MMOImport::readFile();
        }
    }

    public function updateProductImageUrl()
    {
        $this->initializeWebservice();
        $maxProducts = 1000;

        $productURL = new MMOProduct();
        $productURL->cleanProductUrls();

        $languageId = \Context::getContext()->language->id;
        $sql = 'SELECT COUNT(p.`id_product`) AS nb
                FROM `'._DB_PREFIX_.'product` p
                '.\Shop::addSqlAssociation('product', 'p').'
                INNER JOIN '._DB_PREFIX_.'mmo_connector_product_map pm ON p.id_product = pm.id_product_shop
                WHERE product_shop.`active` = 1';

        $productsCount = (int)\Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql, false);
        MMOLogger::getInstance()->info("updateProductImageUrl for PRODUCT COUNT: $productsCount and LANG ID: $languageId");

        for($offset = 0; $offset < $productsCount; $offset += $maxProducts) {
            $sql = "SELECT p.`id_product` AS id_product
                FROM `"._DB_PREFIX_."product` p
                ".\Shop::addSqlAssociation('product', 'p')."
                INNER JOIN "._DB_PREFIX_."mmo_connector_product_map pm ON p.id_product = pm.id_product_shop
                WHERE product_shop.`active` = 1
                ORDER BY p.`id_product` ASC
                LIMIT $offset, $maxProducts";
            $productIds = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

            foreach ($productIds as $productId) {
                $product = new \Product($productId['id_product'], false, $languageId);
                $productURL->getProductUrl($product);
                $productURL->getImagesUrl($product);
            }
        }
    }

    public static function upgrade()
    {
        try {
            require_once _PS_MODULE_DIR_.'ps_mmoconnector/vendor/autoload.php';

            $nameZipTmp = 'tmpUpdateModule.zip';
            $dir = _PS_MODULE_DIR_;

            $handler = curl_init(self::API_BASE_URI.'/rest/selling_channel_connector/1.json');
            curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handler, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($handler, CURLOPT_HEADER, false);
            curl_setopt($handler, CURLOPT_CONNECTTIMEOUT ,30);
            curl_setopt($handler, CURLOPT_TIMEOUT, 60);

            curl_setopt(
                $handler,
                CURLOPT_HTTPHEADER,
                array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'Authorization: Bearer '.self::API_CONNECTOR_TOKEN,
                )
            );

            $response = curl_exec($handler);
            $responseErrorNumber = curl_errno($handler);

            if ($responseErrorNumber) {
                MMOLogger::getInstance()->critical("Update server respended with error $responseErrorNumber");

                return false;
            }

            $statusCode = @curl_getinfo($handler, CURLINFO_HTTP_CODE);

            if ($statusCode !== 200) {
                MMOLogger::getInstance()->critical("Update server respended with status code $statusCode");

                return false;
            }

            $connectorInfo = json_decode($response);
            $connectorUrl = $connectorInfo->url;
            $connectorVersion = $connectorInfo->version;
            $moduleVersion = \Ps_Mmoconnector::MODULE_VERSION;

            if (version_compare($connectorVersion, $moduleVersion) <= 0) {
                MMOLogger::getInstance()->critical("Update server version $connectorVersion lower than installed module version $moduleVersion.");

                return false;
            }

            MMOLogger::getInstance()->info(
                'Found new version',
                ['Current version' => \Ps_Mmoconnector::MODULE_VERSION, 'New Version' => $connectorVersion]
            );
            $updateContent = file_get_contents($connectorUrl);

            if (!$updateContent) {
                MMOLogger::getInstance()->warning('Update Module: Error copying files to folder '.$dir.$nameZipTmp);

                return false;
            }

            file_put_contents($dir.self::MODULE_NAME.'/'.$nameZipTmp, $updateContent);
            $zip = new \ZipArchive();
            $result = $zip->open($dir.self::MODULE_NAME.'/'.$nameZipTmp);

            if (!$result) {
                MMOLogger::getInstance()->warning('Update Module: Error copying files to folder '.$dir.$nameZipTmp);

                return false;
            }

            $zip->extractTo($dir);
            $zip->close();
            unlink($dir.self::MODULE_NAME.'/'.$nameZipTmp);
            MMOSetup::update($connectorVersion);

            MMOLogger::getInstance()->info('Success updating module.');

            return true;
        }
        catch (\Exception $e) {
            MMOLogger::getInstance()->critical('MODULE UPDATE FAILED. '.$e->getMessage(), $e->getTrace());

            return false;
        }
    }

    public function updateCatalogStock()
    {
        $lastCatalogStockUpdate = \Configuration::get('MMO_CONNECTOR_LAST_CATALOG_STOCK_UPDATE');
        mt_srand(strtotime($lastCatalogStockUpdate));
        $hoursRandom = mt_rand(1,3);
        $minutesRandom = mt_rand(0,59);
        $nextCatalogStockUpdate = strtotime(' +'.$hoursRandom.' hours +'. $minutesRandom . ' minutes', strtotime($lastCatalogStockUpdate));

        $currentTime = date('Y-m-d H:i:s');

        if ($lastCatalogStockUpdate && $nextCatalogStockUpdate > strtotime($currentTime)) {
            return false;
        }

        \Configuration::updateValue('MMO_CONNECTOR_LAST_CATALOG_STOCK_UPDATE', $currentTime);

        $productDataResponse = $this->getProductDataRequest();
        if ($productDataResponse['status_code'] !== 200) {
            return false;
        }

        $productData = json_decode($productDataResponse['response'], true);

        foreach ($productData as $product) {
            if (!array_key_exists('productActive', $product)
                || !$product['productActive']
                || !array_key_exists('productId', $product)
                || !$product['productId']
            ) {
                continue;
            }

            $mmoProduct = new MMOProduct();
            $productShopId = (int)$mmoProduct->getIdProductShop($product['productId']);
            if (!$productShopId) {
                continue;
            }

            $productAttributeShopId = 0;
            if (!empty($product['productVariationId'])) {
                $mmoCombination = new MMOCombination();
                $mmoCombination->objectExternalId = $product['productVariationId'];
                $mmoCombination->exists();
                $productAttributeShopId = (int)$mmoCombination->objectId;
            }
            $this->updateProductStock($productShopId, $productAttributeShopId, (int)$product['stockQuantity']);
        }

    }

    private function initializeWebservice()
    {
        if (!defined('MMOCONECTOR_WEBSERVICE_KEY_ID')) {
            $autKey = $this->getKeyWebservice();
            define('MMOCONECTOR_WEBSERVICE_KEY_ID', $autKey);
        }

        if (!defined('URLWEBSERVICE')) {
            $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
            $urlWebService = \Context::getContext()->shop->getBaseURL($forceSsl, true);
            $urlWebService = rtrim($urlWebService, '/');
            define('URLWEBSERVICE', $urlWebService);
        }
    }

    private function getProductDataRequest()
    {
        $url = 'http://platform.bigbuy.eu/rest/customer/stocks';

        $productData = null;
        $mipAPIToken = \Configuration::get('MMO_CONNECTOR_ACCESS_TOKEN');

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $mipAPIToken
        );

        $curlOptions = array(
            CURLOPT_HEADER => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLINFO_HEADER_OUT => TRUE,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_TIMEOUT => 60,
        );

        $session = curl_init($url);
        curl_setopt_array($session, $curlOptions);
        $response = curl_exec($session);
        $index = strpos($response, "\r\n\r\n");

        $header = substr($response, 0, $index);
        $body = substr($response, $index + 4);
        $headerArrayTmp = explode("\n", $header);
        $headerArray = array();

        foreach ($headerArrayTmp as $headerItem) {
            $tmp = explode(':', $headerItem);
            $tmp = array_map('trim', $tmp);

            if (count($tmp) === 2) {
                $headerArray[$tmp[0]] = $tmp[1];
            }
        }

        $status_code = curl_getinfo($session, CURLINFO_HTTP_CODE);

        curl_close($session);

        return array('status_code' => (int)$status_code, 'response' => $body, 'header' => $header);
    }


    /**
     * @param int $productId
     * @param int $productAttributeId
     * @param int $quantity
     */
    private function updateProductStock($productId, $productAttributeId, $quantity)
    {
        $product = new \Product((int) $productId);

        $stock_available = \StockAvailable::getQuantityAvailableByProduct((int) $product->id, $productAttributeId, (int) $this->context->shop->id);

        if ($quantity > 10) {
            $quantity = 10;
        }

        if ($stock_available !== $quantity && \Validate::isLoadedObject($product)) {
            $product->quantity = $quantity;

            if (\Shop::isFeatureActive()) {
                $shops = array();
                if (empty($shops)) {
                    $shops = \Shop::getContextListShopID();
                }
                foreach ($shops as $shop) {
                    \StockAvailable::setQuantity((int) $product->id, $productAttributeId, (int) $product->quantity, (int) $shop);
                }
            } else {
                \StockAvailable::setQuantity((int) $product->id, $productAttributeId, (int) $product->quantity, (int) $this->context->shop->id);
            }
        }
    }
}