<?php

namespace MIP\PrestaShop;

class MMOConnector
{
    const SUPPLIER_NAME = 'BB';

    public static function install()
    {
        $order_status = array(
            'order_status' => array(
                'initial' => '',
                'in_process' => '',
                'sent' => '',
            ),
        );

        $sql = "
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_attribute_group_map` 
            (
                `id_attribute_group` int(11) NOT NULL DEFAULT '0', 
                `id_attribute_group_shop` int(11) DEFAULT NULL, 
                `version` VARCHAR(16000) DEFAULT '{}', 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_attribute_group`), 
                INDEX `id_attribute_group_shop` (`id_attribute_group_shop`) 
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci'; 

            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_attribute_map` 
            (
                `id_attribute` int(11) NOT NULL DEFAULT '0', 
                `id_attribute_shop` int(11) DEFAULT NULL, 
                `version` VARCHAR(16000) DEFAULT '{}', 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_attribute`), 
                INDEX `id_attribute_shop` (`id_attribute_shop`) 
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci';

            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_product_attribute_map` 
            (
                `id_product_attribute` int(11) NOT NULL DEFAULT '0', 
                `id_product_attribute_shop` int(11) DEFAULT NULL, 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_product_attribute`), 
                INDEX `id_product_attribute_shop` (`id_product_attribute_shop`) 
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci';

            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_category_map` 
            (
                `id_category` int(11) NOT NULL DEFAULT '0', 
                `id_category_shop` int(11) DEFAULT NULL, 
                `version` VARCHAR(16000) DEFAULT '{}', 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_category`), 
	            INDEX `id_category_shop` (`id_category_shop`)
            ) 
            ENGINE=InnoDB COLLATE='utf8_general_ci';
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_manufacturer_map` 
            (
                `id_manufacturer` int(11) NOT NULL DEFAULT '0', 
                `id_manufacturer_shop` int(11) DEFAULT NULL, 
                `version` VARCHAR(4096) DEFAULT '0', 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_manufacturer`), 
	            INDEX `id_manufacturer_shop` (`id_manufacturer_shop`)
            ) 
            ENGINE=InnoDB COLLATE='utf8_general_ci';
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_product_map` 
            (
                `id_product` int(11) NOT NULL DEFAULT '0', 
                `id_product_shop` int(11) DEFAULT NULL, 
                `version` VARCHAR(16000) DEFAULT '{}', 
                `version_img` INT(4) DEFAULT NULL, 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                `message_version` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_product`), 
	            INDEX `id_product_shop` (`id_product_shop`)
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci'; 
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_order_log` 
            (
                `id_order` int(11) NOT NULL DEFAULT '0', 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                `date_process` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_order`), 
                INDEX `date_process` (`date_process`),
                INDEX `date_update` (`date_update`)
            ) 
            ENGINE=InnoDB COLLATE='utf8_general_ci';
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_tag_map`  
            (
                `id_tag` INT(11) NOT NULL DEFAULT '0', 
                `id_tag_shop` INT(11) NULL DEFAULT NULL, 
                `version` VARCHAR(16000) DEFAULT NULL, 
                `date_add` DATETIME NULL DEFAULT NULL, 
                `date_update` DATETIME NULL DEFAULT NULL, 
                `id_lang` INT(11) NOT NULL DEFAULT '0', 
                PRIMARY KEY (`id_tag`, `id_lang`), 
                INDEX `id_tag_shop` (`id_tag_shop`), 
                INDEX `id_lang` (`id_lang`), 
                INDEX `id_tag` (`id_tag`), 
                INDEX `id_tag_id_lang` (`id_tag`, `id_lang`)
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci'; 
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_import_process` 
            (
                `id` INT(11) NOT NULL AUTO_INCREMENT, 
                `id_product` INT(11) NOT NULL DEFAULT '0',  
                `sku` VARCHAR(32) NULL DEFAULT NULL,  
                `id_file` INT(11) NOT NULL, 
                `response_web_service` INT(2) NULL DEFAULT NULL, 
                `message_response_web_service` VARCHAR(2048) NULL DEFAULT NULL, 
                `date_add` DATETIME DEFAULT NULL,
                `date_update` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id`), 
                INDEX `id_product` (`id_product`), 
                INDEX `sku` (`sku`), 
                INDEX `id_file` (`id_file`) 
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci'; 
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_file_log` 
            (
                `id_file` int(11) NOT NULL AUTO_INCREMENT, 
                `name` VARCHAR(255) DEFAULT NULL, 
                `version` VARCHAR(12) DEFAULT NULL, 
                `date_add` DATETIME DEFAULT NULL, 
                `date_process` DATETIME DEFAULT NULL, 
                `date_processing_start` DATETIME DEFAULT NULL, 
                `in_process` TINYINT(1) NULL DEFAULT '0', 
                PRIMARY KEY (`id_file`), 
                INDEX `name` (`name`), 
                INDEX `date_process` (`date_process`), 
                INDEX `in_process` (`in_process`), 
                INDEX `date_add` (`date_add`) 
            ) 
            ENGINE=InnoDB COLLATE='utf8_general_ci'; 
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_image_url` 
            (
                `id_image` INT(11) UNSIGNED NOT NULL, 
                `cover` INT(11) UNSIGNED DEFAULT NULL, 
                `url` VARCHAR(2048) DEFAULT NULL, 
                `date_add` DATETIME DEFAULT NULL, 
                `date_update` DATETIME DEFAULT NULL, 
                PRIMARY KEY (`id_image`), 
	            INDEX `date_update` (`date_update`)
            ) 
            ENGINE=InnoDB COLLATE='utf8_general_ci'; 
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_product_url` 
            (
                `id_product` INT(11) UNSIGNED NOT NULL,
                `id_combination` INT(11) UNSIGNED NULL DEFAULT NULL,
                `id_lang` INT(4) UNSIGNED NOT NULL,
                `url` VARCHAR(2048) NULL DEFAULT NULL,
                `date_add` DATETIME NULL DEFAULT NULL,
                `date_update` DATETIME NULL DEFAULT NULL,
                PRIMARY KEY (`id_product`, `id_lang`),
                INDEX `id_combination` (`id_combination`), 
                INDEX `date_update` (`date_update`)
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci';
            
            CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."mmo_connector_product_image_url` 
            (
                `id_product` INT(11) UNSIGNED NOT NULL,
                `id_combination` INT(11) UNSIGNED NOT NULL,
                `id_image` INT(11) UNSIGNED NULL DEFAULT NULL,
                INDEX `id_product` (`id_product`),
                INDEX `id_combination` (`id_combination`),
                INDEX `id_image` (`id_image`)
            )
            ENGINE=InnoDB COLLATE='utf8_general_ci';
        ";
                
        \Configuration::updateValue('MMO_CONNECTOR_ACCESS_TOKEN', null);
        \Configuration::updateValue('MMO_CONNECTOR_SECRET_KEY', null);
        \Configuration::updateValue('MMO_CONNECTOR_SEND_EMAIL', 0);
        \Configuration::updateValue('MMO_CONNECTOR_ORDERSTATUS_VALID', json_encode($order_status));

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

        return $result;
    }

    public static function uninstall()
    {
        /*$sql = "DROP TABLE IF EXISTS
              `" . _DB_PREFIX_ . "mmo_connector_attribute_group_map`,
              `" . _DB_PREFIX_ . "mmo_connector_attribute_map`,
              `" . _DB_PREFIX_ . "mmo_connector_product_attribute_map`,
              `" . _DB_PREFIX_ . "mmo_connector_category_map`,
              `" . _DB_PREFIX_ . "mmo_connector_manufacturer_map`,
              `" . _DB_PREFIX_ . "mmo_connector_product_map`,
              `" . _DB_PREFIX_ . "mmo_connector_order_log`,
              `" . _DB_PREFIX_ . "mmo_connector_tag_map`,
              `" . _DB_PREFIX_ . "mmo_connector_file_log`;";

      $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

      Configuration::deleteByName('MMO_CONNECTOR_ACCESS_TOKEN');
      Configuration::deleteByName('MMO_CONNECTOR_ORDERSTATUS_VALID');
      Configuration::deleteByName('MMOCONECTOR_WEBSERVICE_KEY_ID');
      Configuration::deleteByName('MMO_CONNECTOR_SUPPLIER');

      return $result;*/

        return true;
    }

    /**
     * @param string $requestAccessToken
     * @return bool
     */
    public static function checkAccessToken($requestAccessToken)
    {
        $accessToken = \Configuration::get('MMO_CONNECTOR_ACCESS_TOKEN');

        return $accessToken === $requestAccessToken;
    }

    /**
     * @param string $accessToken
     */
    public static function updateAccessToken($accessToken)
    {
        \Configuration::updateValue('MMO_CONNECTOR_ACCESS_TOKEN', $accessToken);
    }

    /**
     * @param string $secretKey
     */
    public static function updateSecretKey($secretKey)
    {
        \Configuration::updateValue('MMO_CONNECTOR_SECRET_KEY', $secretKey);
    }

    /**
     * @param string $carriers
     */
    public static function updateAssociationCarriers($carriers)
    {
        \Configuration::updateValue('MMO_CONNECTOR_ASSOCIATION_CARRIERS', $carriers);
    }

    /**
     * @return bool
     */
    public static function hasMogrify()
    {
        $cacheId = 'MMOConnector-hasMogrify';

        if(MMOCache::isStored($cacheId)) {
            return MMOCache::get($cacheId);
        }

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            MMOCache::store($cacheId, false);
            MMOLogger::getInstance()->info('La optimización está desactivada en WindowsOS.');

            return false;
        }

        if(!function_exists('exec')) {
            MMOCache::store($cacheId, false);
            MMOLogger::getInstance()->info('El comando EXEC esta desactivado.');

            return false;
        }

        $mogrifyOutput = [];
        exec('which mogrify', $mogrifyOutput);

        if (!empty($mogrifyOutput)) {
            MMOCache::store($cacheId, true);
            return true;
        }

        MMOCache::store($cacheId, false);
        MMOLogger::getInstance()->info('El comando MOGRIFY NO está disponible.');

        return false;
    }

    public function getSupplierId()
    {
        return \Supplier::getIdByName(self::SUPPLIER_NAME);
    }

    public function createSupplier()
    {
        $supplier_object = new \Supplier();
        $supplier_object->active = true;
        $supplier_object->name = self::SUPPLIER_NAME;
        $supplier_object->save();

        $id_supplier = $supplier_object->id;

        $supplier_address = new \Address();
        $supplier_address->id_supplier = $id_supplier;
        $supplier_address->address1 = self::SUPPLIER_NAME;
        $supplier_address->alias = self::SUPPLIER_NAME;
        $supplier_address->lastname = self::SUPPLIER_NAME;
        $supplier_address->firstname = self::SUPPLIER_NAME;
        $supplier_address->city = self::SUPPLIER_NAME;
        $supplier_address->id_country = \Country::getByIso('ES');
        $supplier_address->save();

        \Configuration::updateValue('MMO_CONNECTOR_SUPPLIER', $id_supplier);
    }
}
