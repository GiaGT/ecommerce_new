<?php

/**
 * Created by PhpStorm.
 * User: a4b-4
 * Date: 12/06/2017
 * Time: 8:05
 */
namespace MIP\PrestaShop;

class MMOLang
{
    public static $objectType = 'languages';
    public static $objectId;
    public static $webService;
    public static $xmlObject;
    public static $defaultLanguageId;
    public static $shopLanguages = array();

    public static function process()
    {
        self::createDefaultConfiguration();
        self::getXMLObjectFromWebService();

        if (is_string(self::$xmlObject)) {
            return self::$xmlObject;
        }

        if (!self::$defaultLanguageId) {
            self::initDefaultLanguageId();
        }

        if (empty(self::$shopLanguages)) {
            self::initLanguages();
        }

        return ['Languages' => self::$shopLanguages];
    }

    public static function initDefaultLanguageId()
    {
        if (!self::$defaultLanguageId) {
            self::$defaultLanguageId = \Configuration::get('PS_LANG_DEFAULT');
        }

        return self::$defaultLanguageId;
    }

    private static function getXMLObjectFromWebService()
    {
        if (self::$objectId) {
            self::$xmlObject = self::$webService->getXMLId(self::$objectType, self::$objectId);
        } else {
            self::$xmlObject = self::$webService->getXMLList(['resource' => self::$objectType, ]);
        }
    }

    private static function initLanguages()
    {
        foreach (self::$xmlObject->languages->language as $key => $value) {
            self::$objectId = $value['id'];
            self::$objectType = 'languages';
            self::getXMLObjectFromWebService();

            $id = (string)self::$xmlObject->language->id;
            $name = (string)self::$xmlObject->language->name;
            $isoCode = (string)self::$xmlObject->language->iso_code;
            $active = (int)self::$xmlObject->language->active;
            $default = (int)($id == self::$defaultLanguageId);

            self::$shopLanguages[] = self::getLanguage($id, $name, $isoCode, $active, $default);
        }
    }

    private static function getLanguage($id, $name, $isoCode, $active, $default)
    {
        $language = array();

        $language['LanguageID'] = $id;
        $language['Name'] = $name;
        $language['IsoCode'] = $isoCode;
        $language['Active'] = $active;
        $language['Default'] = $default;

        return $language;
    }

    private static function createDefaultConfiguration()
    {
        $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
        $webservice_key = new \WebserviceKey($key_id);

        //self::givePermisionsWebservice($key_id);
        $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $urlWebService = \Context::getContext()->shop->getBaseURL($forceSsl, true);
        $urlWebService = rtrim($urlWebService, '/');

        if (!defined('MMOCONECTOR_WEBSERVICE_KEY_ID')){
            define('MMOCONECTOR_WEBSERVICE_KEY_ID', $webservice_key->key);
        }

        if (!defined('URLWEBSERVICE')) {
            define('URLWEBSERVICE', $urlWebService);
        }

        self::$webService = MMOWebservice::getInstance();
    }

    public static function getActiveLanguageIds()
    {
        $cacheId = 'MMOObject-getActiveLanguagesShop';

        if (MMOCache::isValid($cacheId)) {
            return MMOCache::get($cacheId);
        }

        $languageIds = \Language::getLanguages(true, false, true);

        MMOCache::store($cacheId, $languageIds);

        return $languageIds;
    }
}