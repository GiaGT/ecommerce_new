<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of MMOProductVariationReport
 */
class MMOProductVariationReport
{
    /** @var integer */
    public $TotalProductVariations;

    /** @var integer */
    public $ActiveProductVariations;

    /** @var array */
    public $ProductVariations;

}
