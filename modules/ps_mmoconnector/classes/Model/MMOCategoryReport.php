<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of MMOCategoryReport
 */
class MMOCategoryReport
{
    /** @var integer */
    public $TotalCategories;

    /** @var integer */
    public $ActiveCategories;

    /** @var array */
    public $Categories;

}
