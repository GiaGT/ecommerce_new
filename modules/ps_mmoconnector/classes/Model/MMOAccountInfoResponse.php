<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of AccountInfoResponse
 */
class MMOAccountInfoResponse extends MMOResponse
{
    /**
     * @var MMOAccountInfo
     */
    public $Data;
}