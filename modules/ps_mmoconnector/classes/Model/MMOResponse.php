<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of MMOResponse
 */
abstract class MMOResponse
{
    /**
     * @var integer
     */
    public $Code;

    /**
     * @var string
     */
    public $Message;

    /**
     * @var array
     */
    public $Data;
}
