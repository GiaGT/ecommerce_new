<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of MMOStatusReport
 */
class MMOStatusReport
{
    /**
     * @var \DateTime
     *
     * The value represents the time in the UTC timezone and W3C datetime format
     */
    public $RequestDate;

    /** @var MMOAccountInfo */
    public $AccountInfo;

    /** @var MMOProductReport */
    public $ProductsReport;

    /** @var MMOProductVariationReport */
    public $ProductVariationsReport;

    /** @var MMOCategoryReport */
    public $CategoriesReport;

    /** @var MMOFileReport */
    public $FilesReport;

    /** @var array */
    public $Taxes;

    /** @var array */
    public $Languages;

}
