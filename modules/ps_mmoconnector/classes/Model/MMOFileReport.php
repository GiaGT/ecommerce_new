<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of MMOFileReport
 */
class MMOFileReport
{
    /** @var integer */
    public $TotalFiles;

    /** @var integer */
    public $PendingFiles;

    /** @var boolean */
    public $ProcessingFiles;

    /** @var \DateTime */
    public $LastStartTime;

    /** @var \DateTime */
    public $LastAddTime;

}
