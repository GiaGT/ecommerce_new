<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of MMOProductReport
 */
class MMOProductReport
{
    /** @var integer */
    public $TotalProducts;

    /** @var integer */
    public $ActiveProducts;

    /** @var array */
    public $Products;

}
