<?php

namespace MIP\PrestaShop\Model;

/**
 * Description of MMOAccountInfo
 */
class MMOAccountInfo
{
    /**
     * @var string
     */
    public $Version;

    /**
     * @var boolean
     */
    public $Installed;

    /**
     * @var \DateTime
     *
     * The value represents the time in the UTC timezone and W3C datetime format
     */
    public  $LastCronExecutionDate;

    /**
     * @var boolean
     */
    public $ModuleInstallationPermissions;

    /**
     * @var boolean
     */
    public $FileFolderPermissions;

}
