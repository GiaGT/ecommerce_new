<?php

namespace MIP\PrestaShop;

class MMOWebserviceAdapter
{
    const FOLDER_IMAGES = '/tmp';
    const URL_IMAGES = 'http://www.dropshippers.com.es/imgs/';

    public $shop_path;
    private static $instance;

    private function __construct()
    {
        $_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.1';
        $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $this->shop_path = \Context::getContext()->shop->getBaseURL($forceSsl, true);
        $this->shop_path = rtrim($this->shop_path, '/');
        \WebserviceRequest::$ws_current_classname = \WebserviceKey::getClassFromKey(MMOCONECTOR_WEBSERVICE_KEY_ID);
    }

    /**
     * @return MMOWebserviceAdapter
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return \WebserviceRequest
     */
    public function getWebservice()
    {
        return new \WebserviceRequest::$ws_current_classname();
    }

    /**
     * @param string $type
     * @return \SimpleXMLElement|string
     * @throws MMOPrestaShopWebserviceException
     */
    public function getXMLModel($type)
    {
        MMOLogger::getInstance()->debug("MMOWSA->getXMLModel $type");
        $url = addslashes($type);
        $cacheId = "MMOWSA-$url";

        if (MMOCache::isValid($cacheId)) {
            MMOLogger::getInstance()->info("CACHE HIT getXMLModel $url");
            return MMOCache::get($cacheId);
        }

        $parameters = ['schema' => 'synopsis'];

        try {
            $response = $this->getWebservice()->fetch(MMOCONECTOR_WEBSERVICE_KEY_ID, 'GET', $url, $parameters, false);
            $statusCode = $this->getStatusCode($response['headers']);
            $this->checkStatusCode($statusCode);
            $xml = $this->parseXML($response['content']);
            MMOCache::store($cacheId, $xml);
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

            if ($ex->getCode() === 404 || $ex->getCode() === 500) {
                $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
                $webservice_key = new \WebserviceKey($key_id);

                MMOLogger::getInstance()->notice('getXMLModel: '.$ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type]);
                return $ex->getCode().'|'.$ex->getMessage();
            }

            MMOLogger::getInstance()->critical(__FUNCTION__.' '.$ex->getCode().' '.$ex->getMessage(), [$this->shop_path, $type]);
            throw $ex;
        }

        MMOCache::store($cacheId, $xml);

        return $xml;
    }

    /**
     * @param string $type
     * @param int $id
     * @return \SimpleXMLElement|string
     * @throws MMOPrestaShopWebserviceException
     */
    public function getXMLId($type, $id)
    {
        MMOLogger::getInstance()->debug("MMOWSA->getXMLId $type $id");
        $url = addslashes("$type/$id");

        try {
            $response = $this->getWebservice()->fetch(MMOCONECTOR_WEBSERVICE_KEY_ID, 'GET', $url, [], false);
            $statusCode = $this->getStatusCode($response['headers']);
            $this->checkStatusCode($statusCode);
            $xml = $this->parseXML($response['content']);

            return $xml;
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

         if ($ex->getCode() === 404) {
                $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
                $webservice_key = new \WebserviceKey($key_id);

                MMOLogger::getInstance()->critical('getXMLId: '.$ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type, $id]);
                return $ex->getCode().'|'.$ex->getMessage();
            }

            if ($ex->getCode() === 500 && $type === 'images/products') {
                $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
                $webservice_key = new \WebserviceKey($key_id);

                MMOLogger::getInstance()->critical('getXMLId: '.$ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type, $id]);
                return $ex->getCode().'|'.$ex->getMessage();
            }

            MMOLogger::getInstance()->critical(__FUNCTION__.' '.$ex->getCode().' '.$ex->getMessage(), [$this->shop_path, $type, $id]);
            throw $ex;
        }
    }

    /**
     * @param string $type
     * @param integer $id
     * @param \SimpleXMLElement $xmlObject
     * @return \SimpleXMLElement|string
     * @throws MMOPrestaShopWebserviceException
     */
    public function setXMLId($type, $id, $xmlObject)
    {
        MMOLogger::getInstance()->debug("MMOWSA->setXMLId $type $id");
        $url = addslashes("$type/$id");
        $response = [];

        try {
            $response = $this->getWebservice()->fetch(MMOCONECTOR_WEBSERVICE_KEY_ID, 'PUT', $url, [], false, $xmlObject->asXML());
            $statusCode = $this->getStatusCode($response['headers']);
            $this->checkStatusCode($statusCode);
            $xml = $this->parseXML($response['content']);

            return $xml;
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

            $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
            $webservice_key = new \WebserviceKey($key_id);

            MMOLogger::getInstance()->critical($ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type, $id]);
            MMOLogger::getInstance()->debug($ex->getMessage().' | W'.$webservice_key->key.'=', [$response]);

            return $ex->getCode().'|'.$ex->getMessage();
        }
    }

    /**
     * @param string $type
     * @param \SimpleXMLElement $xmlObject
     * @return \SimpleXMLElement|string
     * @throws MMOPrestaShopWebserviceException
     */
    public function addXMLId($type, $xmlObject)
    {
        MMOLogger::getInstance()->debug("MMOWSA->addXMLId $type");
        $url = addslashes("$type");
        $response = [];

        try {
            $response = $this->getWebservice()->fetch(MMOCONECTOR_WEBSERVICE_KEY_ID, 'POST', $url, [], false, $xmlObject->asXML());
            $statusCode = $this->getStatusCode($response['headers']);
            $this->checkStatusCode($statusCode);
            $xml = $this->parseXML($response['content']);

            return $xml;
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

            $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
            $webservice_key = new \WebserviceKey($key_id);

            MMOLogger::getInstance()->critical($ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type]);
            MMOLogger::getInstance()->debug($ex->getMessage().' | W'.$webservice_key->key.'=', [$response]);

            return $ex->getCode().'|'.$ex->getMessage();
        }
    }

    /**
     * @see http://doc.prestashop.com/display/PS16/Chapter+8+-+Advanced+use
     *
     * $options = array(
     *      'resource'   => 'customers',
     *      'display'    => '[firstname,lastname]',
     *      'filter[firstname]' => '[John]',
     *      'filter[lastname]'  => '[DOE]',
     *      'sort'     => '[lastname_ASC]',
     *      'limit'    => '5',
     * );
     *
     * URL: (Store URL)/api/customers/?display=[lastname]&filter[id]=[1,10]
     *
     * @param array $options
     * @return \SimpleXMLElement|null
     * @throws \Exception
     */
    public function getXMLList(array $options)
    {
        MMOLogger::getInstance()->debug('MMOWSA->getXMLList', $options);
        $url = addslashes($options['resource']);
        parse_str(http_build_query($options), $parameters);
        unset($parameters['resource']);
        $response = [];

        try {
            $response = $this->getWebservice()->fetch(MMOCONECTOR_WEBSERVICE_KEY_ID, 'GET', $url, $parameters, false);
            $statusCode = $this->getStatusCode($response['headers']);
            $this->checkStatusCode($statusCode);
            $xml = $this->parseXML($response['content']);

            return $xml;
        } catch (\Exception $e) {
            if ($e->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $e;
            }

            MMOLogger::getInstance()->critical('getXMLList: ' . $e->getMessage(), [$this->shop_path, $options]);
            MMOLogger::getInstance()->debug('getXMLList: ' . $e->getMessage(), $response);
        }

        return null;
    }

    /**
     * @see http://doc.prestashop.com/display/PS16/Chapter+8+-+Advanced+use
     *
     * $options = array(
     *      'id'         => '10',
     *      'resource'   => 'customers',
     *      'display'    => '[firstname,lastname]',
     *      'filter[firstname]' => '[John]',
     *      'filter[lastname]'  => '[DOE]',
     *      'sort'     => '[lastname_ASC]',
     *      'limit'    => '5',
     * );
     *
     * URL: (Store URL)/api/products/?filter[id]=[1,10]
     *
     * @param array $options
     * @throws \Exception
     */
    public function delete(array $options)
    {
        MMOLogger::getInstance()->debug('MMOWSA->delete', $options);
        $url = addslashes($options['resource'].'/'.$options['id']);
        parse_str(http_build_query($options), $parameters);
        unset($parameters['resource']);
        unset($parameters['id']);
        $response = [];

        try {
            $response = $this->getWebservice()->fetch(MMOCONECTOR_WEBSERVICE_KEY_ID, 'DELETE', $url, [], false);
            $statusCode = $this->getStatusCode($response['headers']);
            $this->checkStatusCode($statusCode);
        } catch (\Exception $e) {
            if ($e->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $e;
            }

            MMOLogger::getInstance()->critical('delete: ' . $e->getMessage(), [$this->shop_path, $options]);
            MMOLogger::getInstance()->debug('delete: ' . $e->getMessage(), $response);
        }
    }

    /**
     * Load XML from string. Can throw exception
     * @param string $responseContent String from a CURL response
     * @return \SimpleXMLElement status_code, response
     * @throws MMOPrestaShopWebserviceException
     */
    public function parseXML($responseContent)
    {
        if (empty($responseContent)) {
            throw new MMOPrestaShopWebserviceException('HTTP response is empty');
        }

        libxml_clear_errors();
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($responseContent,'SimpleXMLElement', LIBXML_NOCDATA);

        if (libxml_get_errors()) {
            $msg = var_export(libxml_get_errors(), true);
            libxml_clear_errors();
            throw new MMOPrestaShopWebserviceException('HTTP XML response is not parsable: '.$msg);
        }

        return $xml;
    }

    /**
     * @param array $headers
     * @return int
     */
    protected function getStatusCode(array $headers)
    {
        $statusCode = 0;

        foreach ($headers as $header) {
            if (stripos($header, 'HTTP/1') === 0) {
                $responseStatusData = explode(' ', $header, 3);
                $statusCode = (int)$responseStatusData[1];

                break;
            }
        }

        return $statusCode;
    }


    /**
     * Take the status code and throw an exception if the server didn't return 200 or 201 code
     * @param int $statusCode Status code of an HTTP return
     * @throws MMOPrestaShopWebserviceException
     */
    protected function checkStatusCode($statusCode)
    {
        $errorMessage = 'This call to PrestaShop Web Services failed and returned an HTTP status of %d. That means: %s.';

        switch($statusCode) {
            case 200:
            case 201:
                break;
            case 204: throw new MMOPrestaShopWebserviceException(sprintf($errorMessage, $statusCode, 'No content'), $statusCode);break;
            case 400: throw new MMOPrestaShopWebserviceException(sprintf($errorMessage, $statusCode, 'Bad Request'), $statusCode);break;
            case 401: throw new MMOPrestaShopWebserviceException(sprintf($errorMessage, $statusCode, 'Unauthorized'), $statusCode);break;
            case 404: throw new MMOPrestaShopWebserviceException(sprintf($errorMessage, $statusCode, 'Not Found'), $statusCode);break;
            case 405: throw new MMOPrestaShopWebserviceException(sprintf($errorMessage, $statusCode, 'Method Not Allowed'), $statusCode);break;
            case 500: throw new MMOPrestaShopWebserviceException(sprintf($errorMessage, $statusCode, 'Internal Server Error'), $statusCode);break;
            default: throw new MMOPrestaShopWebserviceException('This call to PrestaShop Web Services returned an unexpected HTTP status of:' . $statusCode, $statusCode);
        }
    }
}