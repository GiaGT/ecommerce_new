<?php

namespace MIP\PrestaShop;

use PrestaShop\PrestaShop\Adapter\Entity\Order;

class MMOOrder
{
    const ORDER_STATUS_IN_PROCESS = 'ORDER_IN_PROCESS';
    const ORDER_STATUS_SENT = 'ORDER_SENT';
    const ORDER_STATUS_PENDING_REVIEW = 'ORDER_PENDING_REVIEW';
    const ORDER_STATUS_DELIVERED = 'ORDER_DELIVERED';
    const ORDER_STATUS_CANCELLED = 'ORDER_CANCELLED';

    public static $objectType = 'orders';
    public static $objectId;
    /** @var MMOWebservice */
    public static $webService;
    public static $xmlObject;
    public static $objectOrder = array();

    public static function process($orderId)
    {
        if (empty($orderId)) {
            return self::$objectOrder;
        }

        self::$objectId = $orderId;
        self::createDefaultConfiguration();
        self::getInfoOrderByWebService($orderId);

        return self::formatReponseOrder();
    }

    private static function getInfoOrderByWebService($orderId)
    {
        self::getXMLObjectFromWebService($orderId);

        if (is_string(self::$xmlObject)) {
            self::$objectOrder = '';

            return;
        }
    }

    private  static function getInfoAdress($id_address_delivery)
    {
        $xmlInfroAdress = self::$webService->getXMLId('addresses', $id_address_delivery);

        return $xmlInfroAdress;
    }

    private static function getInfoCurrency($idCurrency)
    {
        $xmlInfoCurrency = self::$webService->getXMLId('currencies', $idCurrency);

        return $xmlInfoCurrency;
    }

    private static function getInfoCarrier($idCarrier)
    {
        $xmlInfoCarrier = self::$webService->getXMLId('carriers', $idCarrier);

        return $xmlInfoCarrier;
    }

    private static function getEmailCustomer($idCustomer)
    {
        $xmlInfoCustomer = self::$webService->getXMLId('customers', $idCustomer);

        return $xmlInfoCustomer;
    }

    private static function formatReponseOrder()
    {
        $orderManager = new \stdClass();
        $orderManager->OrderID = (string)self::$xmlObject->order->reference;
        $orderManager->OrderTotal = new \stdClass();
        $orderManager->OrderTotal->Amount = (string)self::$xmlObject->order->total_paid;
        $orderManager->OrderTotal->ConversionRate = (string)self::$xmlObject->order->conversion_rate;

        $xmlInfoCurrency = self::getInfoCurrency(self::$xmlObject->order->id_currency);
        $orderManager->OrderTotal->CurrencyCode = (string)$xmlInfoCurrency->currency->iso_code;
        $xmlInfoAdress = self::getInfoAdress(self::$xmlObject->order->id_address_delivery);
        $orderManager->PaymentMethod = (string)self::$xmlObject->order->payment;
        $modulesCOD = json_decode(\Configuration::get('MMO_CONNECTOR_MODULES_COD'));

        if (is_array($modulesCOD)) {
            $orderManager->CashOnDelivery = in_array((string)self::$xmlObject->order->module, $modulesCOD);
        }

        $orderManager->DateCreated = (string)self::$xmlObject->order->date_add;
		//$dateAdd = date('Y-m-d\TH:i:sO');
        $xmlInfoCarrier = self::getInfoCarrier(self::$xmlObject->order->id_carrier);
        $orderManager->ShippingService = (string)$xmlInfoCarrier->carrier->name;
        $xmlInfoCustomer = self::getEmailCustomer(self::$xmlObject->order->id_customer);

        $orderManager->ShippingAddress = new \stdClass();
        $orderManager->ShippingAddress->Name = (string)$xmlInfoAdress->address->firstname;
        $orderManager->ShippingAddress->Surname = (string)$xmlInfoAdress->address->lastname;
        $orderManager->ShippingAddress->Email = (string)$xmlInfoCustomer->customer->email;
        $orderManager->ShippingAddress->AddressLine1 = (string)$xmlInfoAdress->address->address1;
        $orderManager->ShippingAddress->AddressLine2 = (string)$xmlInfoAdress->address->address2;
        $orderManager->ShippingAddress->AddressPostalCode = (string)$xmlInfoAdress->address->postcode;
        $orderManager->ShippingAddress->AddressCountry = (string)\Country::getIsoById(
            (int)$xmlInfoAdress->address->id_country
        );
        $orderManager->ShippingAddress->AddressCity = (string)$xmlInfoAdress->address->city;
        $orderManager->ShippingAddress->Phone = (string)$xmlInfoAdress->address->phone_mobile;
        $orderManager->ShippingAddress->Comments = (string)$xmlInfoAdress->address->other;

        if (!$orderManager->ShippingAddress->Phone) {
            $orderManager->ShippingAddress->Phone = (string)$xmlInfoAdress->address->phone;
        }

        if($xmlInfoAdress->address->company){
            $orderManager->ShippingAddress->Company = (string)$xmlInfoAdress->address->company;
        }

        $orderManager->ShippingCost = new \stdClass();
        $orderManager->ShippingCost->Amount = (string)self::$xmlObject->order->total_shipping;
        $orderManager->ShippingCost->CurrencyCode = (string)$xmlInfoCurrency->currency->iso_code;

        $orderLines = self::$xmlObject->order->associations->order_rows->order_row;
        $productsOrder = array();

        foreach ($orderLines as $product) {
            $productManager = new \stdClass;
            $productManager->ASIN = (string)$product->product_reference;

            $productManager->ItemPrice = new \stdClass;
            $productManager->ItemPrice->Amount = (float)$product->product_price;
            $productManager->ItemPrice->CurrencyCode = (string)$xmlInfoCurrency->currency->iso_code;

            $productManager->ItemTax = new \stdClass;
            $productManager->ItemTax->Amount = (float)$product->product_quantity * ((float)$product->unit_price_tax_incl - (float)$product->unit_price_tax_excl);
            $productManager->ItemTax->CurrencyCode = (string)$xmlInfoCurrency->currency->iso_code;

            $productManager->OrderItemId = (string)$product->id;
            $productManager->QuantityOrdered = (string)$product->product_quantity;
            $productManager->SellerSKU = (string)$product->product_reference;

            $productManager->ShippingPrice = new \stdClass;
            $productManager->ShippingPrice->Amount = 0.0;
            $productManager->ShippingPrice->CurrencyCode = (string)$xmlInfoCurrency->currency->iso_code;

            $productManager->ShippingTax = new \stdClass;
            $productManager->ShippingTax->Amount = 0.0;
            $productManager->ShippingTax->CurrencyCode = (string)$xmlInfoCurrency->currency->iso_code;

            $productManager->ItemTitle = (string)$product->product_name;

            $productsOrder[] = $productManager;
            unset($productManager);
        }

        $orderManager->OrderLines = $productsOrder;

        $orderManager->OrderLines->ASIN = '-';

        $orderManager->OrderLines->ItemPrice = new \stdClass;
        $orderManager->OrderLines->ItemPrice->Amount = '-';
        $orderManager->OrderLines->ItemPrice->CurrencyCode = '-';

        $orderManager->OrderLines->ItemTax = new \stdClass;
        $orderManager->OrderLines->ItemTax->Amount = '-';
        $orderManager->OrderLines->ItemTax->CurrencyCode = '-';

        $orderManager->OrderLines->OrderItemId = '-';
        $orderManager->OrderLines->QuantityOrdered = '-';
        $orderManager->OrderLines->SellerSKU = '-';
        $orderManager->OrderLines->OrderItemId = '-';

        $orderManager->OrderLines->ShippingPrice = new \stdClass;
        $orderManager->OrderLines->ShippingPrice->Amount = '-';
        $orderManager->OrderLines->ShippingPrice->CurrencyCode = '-';

        $orderManager->OrderLines->ShippingTax = new \stdClass;
        $orderManager->OrderLines->ShippingTax->CurrencyCode = '-';
        $orderManager->OrderLines->ShippingTax->Amount = '-';
        $orderManager->OrderLines->ItemTitle = '-';

        return $orderManager;
    }

    /**
     * @param $reference
     * @return \SimpleXMLElement|null
     * @throws \Exception
     */
    public static function getXMLObjectFromWebServiceByReference($reference)
    {
        if (!$reference) {
            return null;
        }

        $options = [
            'resource'   => self::$objectType,
            'filter[reference]' => "[$reference]",
            'display'    => 'full',
            'limit'    => '1',
        ];

        $ordersListResponse = self::$webService->getXMLList($options);

        if (!($ordersListResponse instanceof \SimpleXMLElement)) {
            return null;
        }

        return $ordersListResponse->orders;
    }

    /**
     * @param $orderId
     * @throws MMOPrestaShopWebserviceException
     */
    private static function getXMLObjectFromWebService($orderId)
    {
        if (!$orderId) {
            self::$xmlObject = "No order ID.";
            return;
        }

        self::$xmlObject = self::$webService->getXMLId(self::$objectType, $orderId);
    }

    private static function createDefaultConfiguration()
    {
        $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
        $webservice_key = new \WebserviceKey($key_id);

        $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $urlWebService = \Context::getContext()->shop->getBaseURL($forceSsl, true);
        $urlWebService = rtrim($urlWebService, '/');

        if (!defined('MMOCONECTOR_WEBSERVICE_KEY_ID')) {
            define('MMOCONECTOR_WEBSERVICE_KEY_ID', $webservice_key->key);
        }

        if (!defined('URLWEBSERVICE')) {
            define('URLWEBSERVICE', $urlWebService);
        }

        self::$webService = MMOWebservice::getInstance();
    }

    //Función para actualizar los diferentes estados válidos para los pedidos
    public static function updateValidateOrderStatus($initialStateOrderBigBuy, $inProcessStateOrder, $sentStateOrder)
    {
        $order_status = array(
            'order_status' => array(
                'initial' => $initialStateOrderBigBuy,
                'in_process' => $inProcessStateOrder,
                'sent' => $sentStateOrder,
            ),
        );

        $order_status = json_encode($order_status);

        $sql = 'UPDATE `'._DB_PREFIX_."configuration` SET `value` = '".$order_status."' WHERE `name` = 'MMO_CONNECTOR_ORDERSTATUS_VALID'";
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    public static function updateModulesCOD ($modules)
    {
        $modules_COD = json_encode($modules);
        $result = \Configuration::updateValue('MMO_CONNECTOR_MODULES_COD', $modules_COD);

        return $result;
    }

    //Función para obtener los diferentes estados válidos para los pedidos
    public static function getValidateOrderStatus()
    {
        return \Configuration::get('MMO_CONNECTOR_ORDERSTATUS_VALID');
    }

    //Función para obtener los pedidos que aun no han sido capturados por la API.
    public static function getOrdersWithoutShipping($date = null)
    {
        $sql = 'SELECT * FROM `'._DB_PREFIX_."mmo_connector_order_log` WHERE `date_process` IS NULL";

        if (!empty($date)) {
            $sql .= " or `date_add` > '".$date."'";
        }

        return \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);
    }

    //Función para formatear la estados válidos para los pedidos
    public static function formatValidateOrderStatus($orderValidateStatus)
    {
        $statesValidate = new \stdClass;
        $states = json_decode($orderValidateStatus);

        $statesValidate->initial = $states->order_status->initial;
        $statesValidate->in_process = $states->order_status->in_process;
        $statesValidate->sent = $states->order_status->sent;

        return $statesValidate;
    }

    //Función para chequear si el estado del pedido coincide con el seleccionado por el usuario.
    public static function checkOrderWithSelectionOrderStatus($order, $selectionStatus)
    {
        if (!$selectionStatus->initial || !is_array($selectionStatus->initial)) {
            return false;
        }

        if (in_array($order->id_state_order, $selectionStatus->initial)) {
            return true;
        }

        return false;
    }

    /**
     * @param $orderData
     * @param $newStateId
     * @param $externalStateId
     * @return array
     * @throws \Exception
     * @throws \PrestaShopException
     */
    public static function changeStateOrder($orderData, $newStateId, $externalStateId)
    {
        self::createDefaultConfiguration();
        self::$xmlObject = self::getXMLObjectFromWebServiceByReference($orderData['OrderID']);
        $orderId = (int)self::$xmlObject->order->id;
        $orderTrackingNumberCurrent = (string)self::$xmlObject->order->shipping_number;

        $currentStateId = (int)self::$xmlObject->order->current_state;
        $response = [
            'OrderID' => $orderData['OrderID'],
            'TrackingNumber' => $orderData['TrackingNumber'],
            'State' => false,
        ];

        if (!empty($orderData['TrackingNumber']) && $orderTrackingNumberCurrent !== $orderData['TrackingNumber']) {
            self::updateTrackingOrder($orderId, $orderData['TrackingNumber']);
        }

        if ($externalStateId === self::ORDER_STATUS_SENT && empty($orderData['TrackingNumber'])) {
            MMOLogger::getInstance()->alert('Skipping order status update: STATUS SENT BUT NO TRACKING FOUND.');
            return $response;
        }

        if (!$newStateId) {
            MMOLogger::getInstance()->alert('Skipping order status update: NEW STATUS IS IS NOT VALID.');
            return $response;
        }

        if ($currentStateId === $newStateId) {
            MMOLogger::getInstance()->alert('Skipping order status update: STATUS HAS NOT CHANGED.');
            return $response;
        }

        $response['State'] = true;
        $sendEmailConfiguration = (int)\Configuration::get('MMO_CONNECTOR_SEND_EMAIL');

        $orderHistory = new \OrderHistory();
        $orderHistory->id_order = $orderId;
        $orderHistory->id_order_state = $newStateId;

        if($sendEmailConfiguration){
            $orderHistory->addWithemail();
        }else{
            $orderHistory->changeIdOrderState((int)$newStateId, $orderId);
            $orderHistory->save();
        }

        MMOLogger::getInstance()->info(
            'New history order - ' .date('d-m-Y H:i:s') ,
            ['New Order' => $orderHistory]
        );

        return $response;
    }

    //Función para añadir una nueva fila a la tabla "ps_mmo_connector_order_log"
    public static function addMmoConnectorOrderLog($order)
    {
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'mmo_connector_order_log` WHERE `id_order` = '.$order->id_order;
        $exists = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        if (!$exists) {
            $sql = 'INSERT INTO `'._DB_PREFIX_.'mmo_connector_order_log` (`id_order`, `date_add`)'
                ."VALUES ('".$order->id_order."',NOW())";
            $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

            return $result;
        }

        $sql = 'UPDATE `'._DB_PREFIX_.'mmo_connector_order_log` SET `date_update` = NOW() WHERE `id_order` = '.$order->id_order;
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    //Función para comprobar si existe el pedido en la tabla ps_mmo_connector_order_log
    public static function checkIsExistsOrderInMmoConnectorOrderLog($idOrder)
    {
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'mmo_connector_order_log` WHERE `id_order` = '.$idOrder;
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }

    //Función para obtener toda la información de un pedido determinado
    public static function getInfoAboutOrder($idOrder)
    {
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'orders` AS ord '
            .'LEFT JOIN `'._DB_PREFIX_.'order_detail` AS ord_detail ON (ord.id_order = ord_detail.id_order) '
            .'LEFT JOIN `'._DB_PREFIX_.'address` AS adr ON (ord.id_address_delivery = adr.id_address) '
            ."WHERE ord.id_order = '".$idOrder."';";

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }

    //Función para actualizar la fecha 
    public static function checkOrderAsProcess($idOrder)
    {
        $sql = 'UPDATE `'._DB_PREFIX_.'mmo_connector_order_log` SET `date_process` = NOW() WHERE `id_order` = '.$idOrder;
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    /**
     * Función para obtener el útlimo estado de un pedido
     * @param $id_order
     * @return false|int|null|string
     */
    public static function getLastStateOrder($id_order)
    {
        $id_order_state = \Db::getInstance()->getValue(
            'SELECT `id_order_state`
            FROM `'._DB_PREFIX_.'order_history`
            WHERE `id_order` = '.(int)$id_order.'
            ORDER BY `date_add` DESC, `id_order_history` DESC',
            false
        );

        if (!$id_order_state) {
            return 0;
        }

        return $id_order_state;
    }

    /**
     * Función para añadir el número de tracking a un pedido
     * @param int $orderId
     * @param string $trackingNumber
     * @return bool
     */
    public static function updateTrackingOrder($orderId, $trackingNumber)
    {
        $sql = 'UPDATE `'._DB_PREFIX_."order_carrier` SET `tracking_number` = '".$trackingNumber."' WHERE `id_order` = ".$orderId;
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

        return $result;
    }
	/**
     * Función para obtener el pedido
     * @param $idOrder
     *
     * @return Order
     */
    public static function getOrder($idOrder)
    {
        $order = new Order($idOrder);
        return $order;
    }

    /**
     * Función para actualizar el carrier de un pedidoi
     * @param Order $order
     * @param $idCarrier
     *
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public static function updateCarrierOrder(Order $order, $idCarrier)
    {
        $order->id_carrier = (int) $idCarrier;
        $order->update();
    }
}