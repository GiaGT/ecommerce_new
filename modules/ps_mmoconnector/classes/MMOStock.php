<?php

namespace MIP\PrestaShop;

class MMOStock extends MMOObject
{
    const MAX_QUANTITY = 10;
    public $response;

    public function __construct()
    {
        parent::__construct();
        $this->objectType = 'stock_availables';
    }

    public function exists()
    {
        return true;
    }

    public function getXMLBlankObject()
    {
        return true;
    }

    public function getXMLDefaultStructure()
    {
        return true;
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function setXMLObjectData($quantity)
    {
        $this->xmlObject->stock_available->quantity = $quantity;

        if ((int)$quantity > self::MAX_QUANTITY) {
            $this->xmlObject->stock_available->quantity = self::MAX_QUANTITY;
        }

        $this->xmlObject->stock_available->out_of_stock = 0;
        $this->setXMLObjectToWebService();
    }

    public function setXMLObjectToWebService()
    {
        $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
    }

    public function process($id, $quantity)
    {
        $this->objectId = $id;
        $this->getXMLObjectFromWebService();

        if (!is_object($this->xmlObject)) {
            $this->response = 'ERROR: ';
            $this->registerLog($this->response, $id, 'STOCK', $quantity);
            
            return $this->response;
        }

        $this->setXMLObjectData($quantity);
        $this->registerLog($this->response, $id, 'STOCK', $quantity);

        return $this->response;
    }

    public function getStockAvailableID($id_product, $id_product_attribute = null)
    {
        $id_stock_available = \StockAvailableCore::getStockAvailableIdByProductId($id_product, $id_product_attribute);

        return $id_stock_available;
    }

    public function limitMaxStock()
    {
        $sql = 'UPDATE '._DB_PREFIX_.'stock_available sa
                INNER JOIN '._DB_PREFIX_."mmo_connector_product_map pm ON sa.id_product = pm.id_product_shop 
                SET sa.quantity = '".self::MAX_QUANTITY."'
                WHERE sa.quantity > '".self::MAX_QUANTITY."';";

        \Db::getInstance()->execute($sql, false);
    }
}