<?php

namespace MIP\PrestaShop;

class MMOCombination extends MMOObject
{
    public $response;
    public $update = false;
    public $productID;
    public $productPriceTaxIncluded;
    public $taxRuleGroupId;
    public $objectType;
    public $structure = array(
        'VariationID' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => false,
            'field' => 'id',
            'value' => '',
        ),
        'SKU' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'reference',
            'value' => '',
        ),
        'EAN' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'ean13',
            'value' => '',
        ),
        'Stock' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'quantity',
            'value' => 0,
        ),
        'Weight' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'weight',
            'value' => 0,
        ),
        'Price' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'price',
            'value' => 0.00,
        ),
        'WholesalePrice' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'wholesale_price',
            'value' => 0.00,
        ),
        'VariationAttributes' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => false,
            'field' => '',
            'value' => '',
        ),
    );


    /**
     * @param $variationAttribute
     * @return bool
     */
    public static function hasValidLanguageData($variationAttribute)
    {
        if (array_key_exists('AttributeGroup', $variationAttribute)
            && array_key_exists('AttributeGroupLangs', $variationAttribute['AttributeGroup'])
            && empty($variationAttribute['AttributeGroup']['AttributeGroupLangs'])
        ) {

            return false;
        }

        if (array_key_exists('Attribute', $variationAttribute)
            && array_key_exists('AttributeLangs', $variationAttribute['Attribute'])
            && empty($variationAttribute['Attribute']['AttributeLangs'])
        ) {
            return false;
        }

        return true;
    }

    public function __construct()
    {
        parent::__construct();
        $this->objectType = 'combinations';
    }

    public function exists()
    {
        $id = \Db::getInstance()->getValue('SELECT id_product_attribute_shop FROM ' . _DB_PREFIX_ . 'mmo_connector_product_attribute_map WHERE id_product_attribute = ' . $this->objectExternalId, false);

        if (!$id) {
            return false;
        }

        $this->objectId = $id;
        return true;
    }

    public function getXMLBlankObject()
    {
        $this->xmlObject = $this->webService->getXMLModel($this->objectType);
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function getXMLDefaultStructure()
    {
        foreach ($this->structure as $key => $value) {
            try {
                if (!$this->isMapped($key)) {
                    continue;
                }

                $field = $this->getField($this->structure, $key);
                $valor = $this->getValue($this->structure, $key);
                $this->xmlObject->combination->{$field} = $valor;
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }
    }

    /**
     * @param array $data
     */
    public function setXMLObjectData($data)
    {
        unset($this->xmlObject->combination->associations->product_option_values);

        foreach ($data as $key => $value) {
            try {
                if ($key === 'VariationAttributes') {
                    $i = 0;
                    foreach ($value as $clave => $valor) {
                        $idAttribute = $this->getAttributeID($valor['Attribute']['AttributeID']);
                        $this->xmlObject->combination->associations->product_option_values->product_option_value[$i]->id = $idAttribute;
                        $i++;
                    }

                    continue;
                }

                if ($key === 'Price') {
                    $productManager = new MMOProduct();
                    $field = $this->getField($this->structure, $key);
                    $variationPriceTaxIncluded = $value - $this->productPriceTaxIncluded;
                    $variationPriceTaxExcluded = $productManager->calculatePrice($variationPriceTaxIncluded, $this->taxRuleGroupId);
                    $this->xmlObject->combination->{$field} = $variationPriceTaxExcluded;

                    continue;
                }

                if ($this->isMapped($key)) {
                    $field = $this->getField($this->structure, $key);
                    $this->xmlObject->combination->{$field} = $value;

                    continue;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }

        $this->xmlObject->combination->minimal_quantity = 1;

        if ($this->update) {
            $this->xmlObject->combination->id = $this->objectId;
        } else {
            $this->xmlObject->combination->id_product = $this->productID;
            unset($this->xmlObject->combination->id);
        }

        $this->setXMLObjectToWebService();
    }

    public function setXMLObjectToWebService()
    {
        if (!$this->update) {
            $this->response = $this->webService->addXMLId($this->objectType, $this->xmlObject);
        } else {
            $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
        }
    }

    public function process($data)
    {
        $this->response = null;
        $this->objectId = null;
        $this->objectExternalId = $data['VariationID'];
        $productCombinationMappingExists = $this->exists();

        if ($productCombinationMappingExists) {
            $this->update = true;
            $this->getXMLObjectFromWebService();
        }

        if ($this->update && !is_object($this->xmlObject)) {
            $this->update = false;
        }

        if (!$productCombinationMappingExists) {
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }
        else if ($productCombinationMappingExists && !is_object($this->xmlObject)) {
            MMOLogger::getInstance()->warning('Marca mapeada pero no existe en la tienda. Vamos a intentar crearla de todos modos. Webservice message: '.$this->xmlObject);
            $this->response = null;
            $this->objectId = null;
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }

        $this->setXMLObjectData($data);
        $this->registerLog($this->response, $data['VariationID'], 'COMBINATION', $productCombinationMappingExists);

        if (is_string($this->response)) {
            MMOLogger::getInstance()->error('Error del webservice al procesar el COMBINATION '.$this->objectExternalId);

            return $this->response;
        }

        MMOLogger::getInstance()->info('INSERCION COMBINATION: '.$this->objectId);

        if (!$productCombinationMappingExists && $this->update === false) {
            $this->createCombinationMap();
        }
        else if ($productCombinationMappingExists && $this->update === false) {
            $this->remapCombinationMap();
        }
        else {
            $this->updateCombinationMap();
        }

        $this->update = false;

        return $this->response;
    }

    public function createCombinationMap()
    {
        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'mmo_connector_product_attribute_map (id_product_attribute_shop, id_product_attribute, date_add) VALUES (' . $this->response->combination->id . ',' . $this->objectExternalId . ',NOW());';
        \Db::getInstance()->execute($sql, false);

        if (!empty(\Db::getInstance()->getMsgError())) {
            MMOLogger::getInstance()->critical('createCombinationMap ERROR: '.\Db::getInstance()->getMsgError(), $sql);
        }
    }

    public function updateCombinationMap()
    {
        \Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'mmo_connector_product_attribute_map SET date_update = NOW() WHERE id_product_attribute_shop = ' . $this->objectId, false);
    }

    public function remapCombinationMap()
    {
        \Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'mmo_connector_product_attribute_map SET id_product_attribute_shop = '.$this->response->combination->id.', date_update = NOW() WHERE id_product_attribute = ' . $this->objectExternalId, false);
    }

    public function getStockAvailableID ($id_product, $id_product_attribute = null)
    {
        $id_stock_available = \StockAvailableCore::getStockAvailableIdByProductId($id_product, $id_product_attribute);

        return $id_stock_available;
    }

    /**
     * @return mixed
     */
    public function getProductID()
    {
        return $this->productID;
    }

    /**
     * @param mixed $productID
     * @return MMOCombination
     */
    public function setProductID($productID)
    {
        $this->productID = $productID;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductPriceTaxIncluded()
    {
        return $this->productPriceTaxIncluded;
    }

    /**
     * @param mixed $productPriceTaxIncluded
     * @return MMOCombination
     */
    public function setProductPriceTaxIncluded($productPriceTaxIncluded)
    {
        $this->productPriceTaxIncluded = $productPriceTaxIncluded;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxRuleGroupId()
    {
        return $this->taxRuleGroupId;
    }

    /**
     * @param mixed $taxRuleGroupId
     * @return MMOCombination
     */
    public function setTaxRuleGroupId($taxRuleGroupId)
    {
        $this->taxRuleGroupId = $taxRuleGroupId;

        return $this;
    }
}