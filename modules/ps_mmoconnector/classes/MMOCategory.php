<?php

namespace MIP\PrestaShop;

use MIP\PrestaShop\Model\MMOCategoryReport;

class MMOCategory extends MMOObject
{
    public $response;
    public $objectType;
    public $update = false;
    public $structure = array(
        'CategoryID' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => false,
            'field' => 'id',
            'value' => '',
        ),
        'CategoryParentID' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => true,
            'field' => 'id_parent',
            'value' => 2,
        ),
        'Order' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => true,
            'field' => 'position',
            'value' => 0,
        ),
        'IsRootCategory' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => true,
            'field' => 'is_root_category',
            'value' => 0,
        ),
        'IdShopDefault' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => true,
            'field' => 'id_shop_default',
            'value' => 1,
        ),
        'Active' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => false,
            'field' => 'active',
            'value' => 0,
        ),
        'DateAdd' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => true,
            'field' => 'date_add',
            'value' => '',
        ),
        'DateUpd' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => true,
            'field' => 'date_upd',
            'value' => '',
        ),
        'ImageURL' => array(
            'mapped' => false,
            'required' => false,
            'nullable' => true,
            'field' => '',
            'value' => '',
        ),
        'CategoryLangs' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => false,
            'field' => '',
            'IsoCode' => array(
                'field' => 'language',
                'value' => '',
            ),
            'CategoryName' => array(
                'field' => 'name',
                'value' => '-',
            ),
            'CategoryURL' => array(
                'field' => 'link_rewrite',
                'value' => '-',
            ),
        ),
    );

    public function __construct()
    {
        parent::__construct();
        $this->objectType = 'categories';
    }

    /**
     * @return bool
     */
    public function exists()
    {
        $currentVersionData = array();
        $this->not_process = false;
        $this->version = array();

        $result = \Db::getInstance()->getRow(
            'SELECT id_category_shop, version FROM '._DB_PREFIX_.'mmo_connector_category_map WHERE id_category = '.$this->objectExternalId,
            false
        );

        if (empty($result)) {
            foreach ($this->objeto["CategoryLangs"] as $categoryLang) {
                $this->lang_to_process[$categoryLang['IsoCode']] = $categoryLang['Version'];
            }

            $this->version = $this->lang_to_process;
            $this->version['version'] = $this->objeto['Version'];
            $this->version['active'] = $this->objeto['Active'];

            if (!((int)$this->objeto['Active'])) {
                $this->not_process = true;
            }

            return false;
        }

        if (null !== $result['version']) {
            $currentVersionData = json_decode($result['version'],true);
        }

        $this->objectId = $result['id_category_shop'];
        $langaugeVersionsToProcess = $this->getLanguageVersionsToProcess($this->objeto['CategoryLangs'], $currentVersionData);
        $this->lang_to_process = $this->getLanguagesToProcess($langaugeVersionsToProcess);

        foreach ($langaugeVersionsToProcess as $isoCode => $version) {
            $this->version[$isoCode] = $version;
        }

        if (!isset($this->version['version'], $this->version['active'])
            || ((int)$this->version['version']) < ((int)$this->objeto['Version'])
            || ((int)$this->version['version'] === (int)$this->objeto['Version'] && ((int)$this->version['active']) !== ((int)$this->objeto['Active']))
        ) {
            MMOLogger::getInstance()->warning('PROCESSING CATEGORY NEW STATE', [$this->objeto['Version'], $this->objeto['Active']]);
            $this->version['version'] = $this->objeto['Version'];
            $this->version['active'] = $this->objeto['Active'];

            return true;
        }

        if (!empty($this->lang_to_process)) {
            MMOLogger::getInstance()->warning('PROCESSING CATEGORY LANGS', [$this->lang_to_process]);

            return true;
        }

        $this->not_process = true;

        return true;
    }

    /**
     * @param array $categoryLangsData
     * @param array $currentLanguageVersions
     *
     * @return array
     */
    private function getLanguageVersionsToProcess(array $categoryLangsData, array $currentLanguageVersions)
    {
        $languagesVersionsToProcess = [];

        if (isset($currentLanguageVersions['version'])) {
            $languagesVersionsToProcess['version'] = $currentLanguageVersions['version'];
        }

        if (isset($currentLanguageVersions['active'])) {
            $languagesVersionsToProcess['active'] = $currentLanguageVersions['active'];
        }

        if (empty($categoryLangsData)) {
            return $languagesVersionsToProcess;
        }

        foreach ($categoryLangsData as $categoryLangData) {
            if (array_key_exists($categoryLangData['IsoCode'], $currentLanguageVersions) && $currentLanguageVersions[$categoryLangData['IsoCode']] <= $categoryLangData['Version']) {
                continue;
            }

            $languagesVersionsToProcess[$categoryLangData['IsoCode']] = $categoryLangData['Version'];
        }

        return $languagesVersionsToProcess;
    }

    /**
     * @param array $languagesVersionsToProcess
     * @return array
     */
    private function getLanguagesToProcess(array $languagesVersionsToProcess)
    {
        $languagesToProcess = [];

        foreach ($languagesVersionsToProcess as $languageIsoCode => $version) {
            if ($languageIsoCode === 'version' || $languageIsoCode === 'active') {
                continue;
            }

            $languagesToProcess[$languageIsoCode] = $languageIsoCode;
        }

        return $languagesToProcess;
    }

    public function getIdCategoryShop($idCategory)
    {
        $id_category_shop = \Db::getInstance()->getValue(
            'SELECT id_category_shop FROM '._DB_PREFIX_.'mmo_connector_category_map WHERE id_category = '.$idCategory,
            false
        );

        return $id_category_shop;
    }

    public function getXMLBlankObject()
    {
        $this->xmlObject = $this->webService->getXMLModel($this->objectType);
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function setXMLObjectToWebService()
    {
        if (!$this->update) {
            unset($this->xmlObject->category->id);
            $this->response = $this->webService->addXMLId($this->objectType, $this->xmlObject);
        } else {
            $this->xmlObject->category->id = $this->objectId;
            $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
        }
    }

    /**
     * @param array $data
     */
    public function setXMLObjectData($data)
    {
        $productManager = new MMOProduct();

        if (!$this->checkMinimumFormatCategory($data)) {
            return;
        }

        if (isset($data['Active'])) {
            $this->xmlObject->category->active = (int)$data['Active'];
        }

        if (isset($data['CategoryLangs'])) {
            foreach ($data['CategoryLangs'] as $clave => $valor) {
                $languageId = $this->getIdLangFromIso($valor['IsoCode']);

                for ($i = 0, $maxCategoryLangs = count($this->xmlObject->category->name->language); $i < $maxCategoryLangs; $i++) {
                    foreach ($this->xmlObject->category->name->language[$i]->attributes() as $xmlAttributeName => $xmlAttributeValue) {
                        if ($xmlAttributeName !== 'id') {
                            continue;
                        }

                        if ((int)$xmlAttributeValue !== $languageId) {
                            continue;
                        }

                        /**
                         * Skip language processing but fix URL slug if user disabled accents
                         */
                        if (!array_key_exists($valor['IsoCode'], $this->lang_to_process)) {
                            $this->xmlObject->category->link_rewrite->language[$i] = $productManager->slugify((string)$this->xmlObject->category->link_rewrite->language[$i]);

                            continue;
                        }

                        $this->version[$valor['IsoCode']] = $valor['Version'];
                        $this->xmlObject->category->name->language[$i] = $valor['CategoryName'];
                        $this->xmlObject->category->link_rewrite->language[$i] = $productManager->slugify(trim($valor['CategoryURL']));
                    }
                }
            }
        }

        foreach ($data as $dataField => $value) {
            if ($dataField === 'Version') {
                continue;
            }

            try {
                if ($this->isMapped($dataField)) {
                    $field = $this->getField($this->structure, $dataField);
                    $this->xmlObject->category->{$field} = $value;

                    continue;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning('MMOCategory->setXMLObjectData: '.$e->getMessage(), ['DATA' => [$dataField => $value]]);
                continue;
            }

            if ($dataField === 'CategoryParentID') {
                $idParent = $this->getCategoryID($value);

                continue;
            }
        }

        $this->unsetCategory();

        $this->xmlObject->category->id_parent = 2;

        if (!empty($idParent)) {
            $this->xmlObject->category->id_parent = $idParent;
        }

        $this->setXMLObjectToWebService();
    }

    public function getXMLDefaultStructure()
    {
        $lang = $this->getLanguagesShop();

        foreach ($this->structure as $key => $value) {
            try {
                if ($this->isMapped($key)) {
                    $field = $this->getField($this->structure, $key);
                    $valor = $this->getValue($this->structure, $key);
                    $this->xmlObject->category->{$field} = $valor;

                    continue;
                }

                if ($key === 'CategoryLangs') {
                    foreach ($value as $ky => $vl) {
                        if ($ky !== 'IsoCode') {
                            continue;
                        }

                        $a = $vl['field'];

                        for ($i = 0, $maxLangs = count($lang); $i < $maxLangs; $i++) {
                            $this->xmlObject->category->{$a}->language[$i] = $vl['value'];
                        }
                    }
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning('MMOCategory->getXMLDefaultStructure: '.$e->getMessage(), ['KEY' => $key, 'VALUE' => $value]);

                continue;
            }
        }
    }

    /**
     * @param array $categoryData
     * @return null|\stdClass|string
     */
    public function process($categoryData)
    {
        $this->response = null;
        $this->objectId = null;
        $this->objectExternalId = $categoryData['CategoryID'];
        $this->objeto = $categoryData;
        $categoryMappingExists = $this->exists();

        if ($categoryMappingExists) {
            $this->update = true;
            $this->getXMLObjectFromWebService();
        }

        /**
         * Skip non mapped categories
         */
        if ($this->not_process && !$categoryMappingExists) {
            MMOLogger::getInstance()->info("SKIP CATEGORY ID {$this->objectExternalId}");
            $this->not_process = true;
            $obj = new \stdClass();

            return $obj;
        }

        /**
         * Skip mapped and existing categories
         */
        if ($this->not_process && is_object($this->xmlObject)) {
            MMOLogger::getInstance()->info("SKIP CATEGORY ID {$this->objectExternalId}");
            $this->not_process = true;
            $obj = new \stdClass();

            return $obj;
        }

        if ($this->update && !is_object($this->xmlObject)) {
            $this->update = false;
        }

        /**
         * Skip inactive categories which are mapped and does not exist in shop
         */
        if ($categoryMappingExists && !is_object($this->xmlObject) && isset($categoryData['Active']) && !$categoryData['Active']) {
            MMOLogger::getInstance()->info("SKIP CATEGORY ID {$this->objectExternalId} - MAPPED DISABLED CATEGORY");
            $this->not_process = true;
            $obj = new \stdClass();

            return $obj;
        }

        if (!$categoryMappingExists) {
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }
        else if ($categoryMappingExists && !is_object($this->xmlObject)) {
            MMOLogger::getInstance()->warning('Categoria mapeada pero no existe en la tienda. Vamos a intentar crearla de todos modos. Webservice message: '.$this->xmlObject);
            $this->resetVersion($this->objectExternalId);
            $this->response = null;
            $this->objectId = null;
            $this->version = $this->getLanguageVersionsToProcess($categoryData['CategoryLangs'], []);
            $this->lang_to_process = $this->getLanguagesToProcess($this->version);
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }

        $this->setXMLObjectData($categoryData);
        $this->registerLog($this->response, $categoryData['CategoryID'], 'CATEGORY', $categoryMappingExists);

        if (is_string($this->response)) {
            MMOLogger::getInstance()->error('Error del webservice al procesar la CATEGORIA '.$categoryData['CategoryID']);

            return $this->response;
        }

        MMOLogger::getInstance()->info('INSERCION CATEGORIA: '.$this->objectId);

        if (!$categoryMappingExists && $this->update === false) {
            $this->createCategoryMap();
        }
        else if ($categoryMappingExists && $this->update === false) {
            $this->remapCategoryMap();
        }
        else {
            $this->updateCategoryMap();
        }

        $this->update = false;

        return $this->response;
    }

    /**
     * @param array $categoryData
     * @return bool
     */
    public function activeCategory($categoryData)
    {
        $this->objeto = $categoryData;
        $this->objectExternalId = $categoryData['CategoryID'];
        $categoryMappingExists = $this->exists();

        if (!$categoryMappingExists) {
            return false;
        }

        $this->update = true;
        $this->getXMLObjectFromWebService();
        $this->xmlObject->category->active = 1;

        if (isset($categoryData['Active'])) {
            $this->xmlObject->category->active = (int)$categoryData['Active'];

            if (!(int)$categoryData['Active']) {
                $this->resetVersion($this->objectExternalId);
            }
        }

        $this->unsetCategory();
        $this->setXMLObjectToWebService();

        return $this->response;
    }

    /**
     * @param int $categoryExternalId
     */
    private function resetVersion($categoryExternalId)
    {
        $sql = 'UPDATE '._DB_PREFIX_."mmo_connector_category_map 
                SET version = DEFAULT, date_update = NOW() 
                WHERE id_category = {$categoryExternalId};";

        \Db::getInstance()->execute($sql, false);
    }

    public function unsetCategory()
    {
        unset($this->xmlObject->category->level_depth);
        unset($this->xmlObject->category->nb_products_recursive);
        unset($this->xmlObject->category->associations);
    }

    public function createCategoryMap()
    {
        $versionData = json_encode($this->version);

        \Db::getInstance()->execute(
            'INSERT INTO '._DB_PREFIX_.'mmo_connector_category_map (id_category_shop, id_category, version, date_add) VALUES ('.$this->response->category->id.','.$this->objectExternalId.",'".$versionData."' ,NOW())"
        );

        $this->version = '';
    }

    public function updateCategoryMap()
    {
        $versionData = json_encode($this->version);

        \Db::getInstance()->execute(
            'UPDATE '._DB_PREFIX_."mmo_connector_category_map SET version = '".$versionData."', date_update = NOW() WHERE id_category_shop = ".$this->objectId
        );

        $this->version = '';
    }

    public function remapCategoryMap()
    {
        $versionData = json_encode($this->version);

        \Db::getInstance()->execute(
            'UPDATE '._DB_PREFIX_."mmo_connector_category_map SET id_category_shop = '".$this->response->category->id."',version = '".$versionData."', date_update = NOW() WHERE id_category = ".$this->objectExternalId
        );

        $this->version = '';
    }

    /**
     * @param array $categoryData
     * @return bool
     */
    private function checkMinimumFormatCategory($categoryData)
    {
        foreach ($this->structure as $categoryExternalField => $structureData) {
            try {
                if ($this->isRequired($categoryExternalField) && !array_key_exists($categoryExternalField, $categoryData)) {
                    $this->response = "LA CATEGORIA {$categoryData['CategoryID']} NO TIENE EL CAMPO REQUERIDO $categoryExternalField";
                    MMOLogger::getInstance()->error($this->response);

                    return false;
                }

                if (!$this->isNullable($categoryExternalField) && $this->isRequired($categoryExternalField) && !array_key_exists($categoryExternalField, $categoryData)) {
                    $this->response = "LA CATEGORIA {$categoryData['CategoryID']} NO TIENE VALOR EN EL CAMPO SIN VALOR NULO $categoryExternalField";
                    MMOLogger::getInstance()->error($this->response);

                    return false;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }

        return true;
    }

    /**
     * @return MMOCategoryReport
     * @throws \PrestaShopDatabaseException
     */
    public function getCategoryReport()
    {
        $response = new MMOCategoryReport();

        $response->TotalCategories = \Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM '._DB_PREFIX_.'mmo_connector_category_map',
            false
        );
        $response->ActiveCategories = \Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM '._DB_PREFIX_.'mmo_connector_category_map cm INNER JOIN '._DB_PREFIX_.'category c ON c.id_category = cm.id_category_shop AND c.active = 1',
            false
        );
        $response->Categories = \Db::getInstance()->executeS(
            'SELECT cm.id_category AS CategoryID, c.active AS Active FROM '._DB_PREFIX_.'mmo_connector_category_map cm INNER JOIN '._DB_PREFIX_.'category c ON c.id_category = cm.id_category_shop',
            true,
            false
        );

        return $response;
    }
}