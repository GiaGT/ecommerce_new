<?php
/**
 * Created by PhpStorm.
 * User: a4b-4
 * Date: 12/06/2017
 * Time: 18:57
 */

namespace MIP\PrestaShop;

class MMOCache
{
    const MAX_CACHED_ELEMENTS = 10000;

    public static $local = array();

    public static function store($key, $value)
    {
        if (count(self::$local) > self::MAX_CACHED_ELEMENTS) {
            self::clean();
        }

        self::$local[$key] = $value;
    }

    public static function get($key)
    {
        return self::$local[$key];
    }

    public static function isStored($key)
    {
        return array_key_exists($key, self::$local);
    }

    public static function clean()
    {
        self::$local = array();
    }

    public static function isValid($key)
    {
        return self::isStored($key) && self::$local[$key];
    }

}