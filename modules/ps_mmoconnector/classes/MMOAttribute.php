<?php

namespace MIP\PrestaShop;

class MMOAttribute extends MMOObject
{
    public $update = false;
    public $response;
    public $objectType;
    public $attributeGroupID;
    public $structure = array(
        'AttributeID' => array(
	        'mapped' => false,
	        'required' => true,
	        'nullable' => false,
            'field' => 'id',
            'value' => '',
        ),
        'AttributeLangs' => array(
	        'mapped' => false,
	        'required' => true,
	        'nullable' => false,
	        'field' => '',
            'value' => '',
            'IsoCode' => array(
                'field' => 'language',
                'value' => '',
            ),
            'AttributeName' => array(
                'field' => 'name',
                'value' => '-',
            ),
        ),
    );

    public function __construct()
    {
        parent::__construct();
        $this->objectType = 'product_option_values';
    }

    public function exists()
    {
        $result = \Db::getInstance()->getRow(
            'SELECT id_attribute_shop, version FROM '._DB_PREFIX_.'mmo_connector_attribute_map WHERE id_attribute = '.$this->objectExternalId,
            false
        );

        if (null === $result['version']) {
            $version = $this->version;
        } else {
            $version = json_decode($result['version'], true);
        }

        if (empty($result)) {
            foreach ($this->objeto['AttributeLangs'] as $attributeLang) {
                $this->lang_to_process[$attributeLang['IsoCode']] = $attributeLang['IsoCode'];
            }

            return false;
        }

        if ($this->objeto['AttributeLangs']) {
            foreach ($this->objeto['AttributeLangs'] as $attributeLang) {
                if ((array_key_exists($attributeLang['IsoCode'], $version)
                        && ($version[$attributeLang['IsoCode']] < $attributeLang['Version']))
                            || !array_key_exists($attributeLang['IsoCode'], $version)
                ) {
                    $this->lang_to_process[$attributeLang['IsoCode']] = $attributeLang['IsoCode'];
                }

                $this->version[$attributeLang['IsoCode']] = $attributeLang['Version'];
            }
        }

        $this->objectId = $result['id_attribute_shop'];

        if (!empty($this->lang_to_process)) {
            return true;
        }

        return $this->not_process = true;
    }

    public function getXMLBlankObject()
    {
        $this->xmlObject = $this->webService->getXMLModel($this->objectType);
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function getXMLDefaultStructure()
    {
        $languageIds = $this->getLanguagesShop();

        foreach ($this->structure as $key => $value) {
            try {
	            if ($key === 'AttributeLangs') {
		            foreach ($value as $clave => $valor) {
			            for ($i = 0, $countLangs = count($languageIds); $i < $countLangs; $i++) {
				            if ($clave !== 'IsoCode') {
					            $val = $this->getValue($value, $clave);
					            $this->xmlObject->product_option_value->name->language[$i] = $val;
				            }
			            }
		            }

		            continue;
	            }

	            if ($this->isMapped($key)) {
                    $field = $this->getField($this->structure, $key);
		            $valor = $this->getValue($this->structure, $key);
		            $this->xmlObject->product_option_value->{$field} = $valor;
	            }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }
    }

    public function setXMLObjectData($data)
    {
        $this->checkMinimumFormatAttribute($data);

        foreach ($data['AttributeLangs'] as $attributeLang) {
            $attributeLangIsoCode = $attributeLang['IsoCode'];
            $attributeLangName = $attributeLang['AttributeName'];
            $attributeLangVersion = $attributeLang['Version'];
            $languageId = $this->getIdLangFromIso($attributeLangIsoCode);

            for ($i = 0, $countLanguages = count($this->xmlObject->product_option_value->name->language); $i < $countLanguages; $i++) {
                foreach ($this->xmlObject->product_option_value->name->language[$i]->attributes() as $productOptionAttributeName => $productOptionAttributeValue) {
                    if ($productOptionAttributeName === 'id' && (int)$productOptionAttributeValue === $languageId && array_key_exists($attributeLangIsoCode, $this->lang_to_process)) {
                        $this->version[$attributeLangIsoCode] = $attributeLangVersion;
                        $this->xmlObject->product_option_value->name->language[$i] = $attributeLangName;
                    }
                }
            }
        }

        if ($this->update) {
            $this->xmlObject->product_option_value->id = $this->objectId;
        } else {
            unset($this->xmlObject->product_option_value->id);
        }

        $this->xmlObject->product_option_value->id_attribute_group = $this->attributeGroupID;
        $this->setXMLObjectToWebService();
    }

    public function setXMLObjectToWebService()
    {
        if (!$this->update) {
            $this->response = $this->webService->addXMLId($this->objectType, $this->xmlObject);
        } else {
            $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
        }
    }

    public function process($response, $attributeData)
    {
        if (!is_int($response)) {
            return 'RESPUESTA INCORRECTA MMOAttribute';
        }

        $this->response = null;
        $this->objectId = null;
	    $this->objectExternalId = $attributeData['AttributeID'];
        $this->objeto = $attributeData;
        $this->attributeGroupID = $response;
        $attributeMappingExists = $this->exists();

        if ($attributeMappingExists) {
            $this->update = true;
            $this->getXMLObjectFromWebService();
        }

        if ($this->not_process && is_object($this->xmlObject)) {
            $obj = new \stdClass();

            return $obj;
        }

        $this->not_process = false;

        if ($this->update && !is_object($this->xmlObject)) {
            $this->update = false;
        }

        if (!$attributeMappingExists) {
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }
        else if ($attributeMappingExists && !is_object($this->xmlObject)) {
            MMOLogger::getInstance()->warning('Atributo mapeado pero no existe en la tienda. Vamos a intentar crearlo de todos modos. Webservice message: '.$this->xmlObject);
            $this->response = null;
            $this->objectId = null;
            $this->version = $this->getLanguageVersionsToProcess($attributeData['AttributeLangs'], []);
            $this->lang_to_process = $this->getLanguagesToProcess($this->version);
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }

        $this->setXMLObjectData($attributeData);
        $this->registerLog($this->response, $attributeData['AttributeID'], 'ATTRIBUTE', $attributeMappingExists);

        if (is_string($this->response)) {
            MMOLogger::getInstance()->error('Error del webservice al procesar el ATTRIBUTE '.$this->objectExternalId);

            return $this->response;
        }

        MMOLogger::getInstance()->info('INSERCION ATTRIBUTE: '.$this->objectId);

	    if (!$attributeMappingExists && $this->update === false) {
		    $this->createAttributeMap();
	    }
	    else if ($attributeMappingExists && $this->update === false) {
		    $this->remapAttributeMap();
	    }
	    else {
		    $this->updateAttributeMap();
	    }

        $this->update = false;

        return $this->response;
    }

	public function createAttributeMap()
	{
		$versionData = json_encode($this->version);

		\Db::getInstance()->execute(
			'INSERT INTO '._DB_PREFIX_.'mmo_connector_attribute_map (id_attribute_shop ,id_attribute, version, date_add) VALUES ('.$this->response->product_option_value->id.','.$this->objectExternalId.",'".$versionData."',NOW())",
			false
		);

		$this->version = '';
	}

	public function updateAttributeMap()
	{
		$versionData = json_encode($this->version);

		\Db::getInstance()->execute(
			"UPDATE "._DB_PREFIX_."mmo_connector_attribute_map SET version = '".$versionData."' ,date_update = NOW() WHERE id_attribute_shop = ".$this->objectId,
			false
		);

		$this->version = '';
	}

	public function remapAttributeMap()
	{
		$versionData = json_encode($this->version);

		\Db::getInstance()->execute(
			'UPDATE '._DB_PREFIX_.'mmo_connector_attribute_map SET id_attribute_shop = '.$this->response->product_option_value->id.', version = '. $versionData .', date_update = NOW() WHERE id_attribute = '.$this->objectExternalId,
			false
		);

		$this->version = '';
	}

    /**
     * @param array $languagesVersionsToProcess
     * @return array
     */
    private function getLanguagesToProcess(array $languagesVersionsToProcess)
    {
        $languagesToProcess = [];

        foreach ($languagesVersionsToProcess as $languageIsoCode => $version) {
            $languagesToProcess[$languageIsoCode] = $languageIsoCode;
        }

        return $languagesToProcess;
    }

    /**
     * @param array $attributeLangsData
     * @param array $currentLanguageVersions
     *
     * @return array
     */
    private function getLanguageVersionsToProcess(array $attributeLangsData, array $currentLanguageVersions)
    {
        $languagesVersionsToProcess = [];

        if (empty($attributeLangsData)) {
            return $languagesVersionsToProcess;
        }

        foreach ($attributeLangsData as $attributeLangData) {
            if (array_key_exists($attributeLangData['IsoCode'], $currentLanguageVersions) && $currentLanguageVersions[$attributeLangData['IsoCode']] <= $attributeLangData['Version']) {
                continue;
            }

            $languagesVersionsToProcess[$attributeLangData['IsoCode']] = $attributeLangData['Version'];
        }

        return $languagesVersionsToProcess;
    }

	private function checkMinimumFormatAttribute($attributeData)
	{
		foreach ($this->structure as $attributeExternalField => $structureData) {
			try {
				if ($this->isRequired($attributeExternalField) && !array_key_exists($attributeExternalField, $attributeData)) {
					$this->response = "EL ATRIBUTO {$attributeData['AttributeID']} NO TIENE EL CAMPO REQUERIDO $attributeExternalField";
					MMOLogger::getInstance()->error($this->response);

					return false;
				}

				if (!$this->isNullable($attributeExternalField) && $this->isRequired($attributeExternalField) && !array_key_exists($attributeExternalField, $attributeData)) {
					$this->response = "EL ATRIBUTO {$attributeData['AttributeID']} NO TIENE VALOR EN EL CAMPO SIN VALOR NULO $attributeExternalField";
					MMOLogger::getInstance()->error($this->response);

					return false;
				}
			}
			catch (\Exception $e) {
				MMOLogger::getInstance()->warning($e->getMessage());
				continue;
			}
		}

		return true;
	}
}