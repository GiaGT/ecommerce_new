<?php

namespace MIP\PrestaShop;

use Cocur\Slugify\Slugify;
use MIP\PrestaShop\Model\MMOProductReport;
use MIP\PrestaShop\Model\MMOProductVariationReport;

class MMOProduct extends MMOObject
{
    public $response;
    public $update = false;
    public $fatherProduct = false;
    public $objectType;
    public $replace_product_name = array(
        '<',
        '>',
        ';',
        '=',
        '#',
        '{',
        '}',
        '|',
        '&nbsp;',
        ' '
    );
    public $structure = array(
        'ProductID' => array(
            'field' => 'id',
            'value' => '',
        ),
        'Brand' => array(
            'BrandID' => array(
                'field' => 'id_manufacturer',
                'value' => 0,
            ),
        ),
        'id_supplier' => array(
            'field' => 'id_supplier',
            'value' => 0,
        ),
        'DefaultCategory' => array(
            'field' => 'id_category_default',
            'value' => 2,
        ),
        'id_tax_rules_group' => array(
            'field' => 'id_tax_rules_group',
            'value' => 1,
        ),
        'id_shop_default' => array(
            'field' => 'id_shop_default',
            'value' => '',
        ),
        'id_default_image' => array(
            'field' => 'id_default_image',
            'value' => '',
        ),
        'Images' => array(
            'field' => 'images',
            'value' => '',
        ),
        'SKU' => array(
            'field' => 'reference',
            'value' => '',
        ),
        'EAN' => array(
            'field' => 'ean13',
            'value' => '0123456789',
        ),
        'Width' => array(
            'field' => 'width',
            'value' => 0,
        ),
        'Height' => array(
            'field' => 'height',
            'value' => 0,
        ),
        'Length' => array(
            'field' => 'depth',
            'value' => 0,
        ),
        'Weight' => array(
            'field' => 'weight',
            'value' => 0,
        ),
        'Active' => array(
            'field' => 'active',
            'value' => 0,
        ),
        'state' => array(
            'field' => 'state',
            'value' => 1,
        ),
        'Price' => array(
            'field' => 'price',
            'value' => 0.00,
        ),
        'WholesalePrice' => array(
            'field' => 'wholesale_price',
            'value' => 0.00,
        ),
        'date_add' => array(
            'field' => 'date_add',
            'value' => '',
        ),
        'date_upd' => array(
            'field' => 'date_upd',
            'value' => '',
        ),
        'ProductLangs' => array(
            'IsoCode' => array(
                'field' => 'language',
                'value' => '',
            ),
            'Title' => array(
                'field' => 'name',
                'value' => '-',
            ),
            'Description' => array(
                'field' => 'description',
                'value' => '-',
            ),
        ),
        'PriceRatePerUnity' => array(
            'field' => 'unit_price_ratio',
            'value' => 0
        ),
        'Unidad' => array(
            'field' => 'unity',
            'value' => ''
        )
    );

    public function __construct()
    {
        parent::__construct();

        $this->objectType = 'products';
    }

    /**
     *
     * @param bool $update
     * @return bool
     */
    public function exists($update = false)
    {
        $currentVersionData = array();
        $this->version = array();
        $sql = 'SELECT id_product_shop, version 
                FROM '._DB_PREFIX_.'mmo_connector_product_map 
                WHERE id_product = '.$this->objectExternalId;

        $result = \Db::getInstance()->getRow($sql, false);

        if (empty($result)) {
            if (!isset($this->objeto['ProductLangs'])) {
                MMOLogger::getInstance()->warning(
                    'PRODUCTO CON ID_SHOP '.$this->objectExternalId.' NO TIENE IDIOMAS');

                return false;
            }

            foreach ($this->objeto["ProductLangs"] as $productLang) {
                $this->lang_to_process[$productLang['IsoCode']] = $productLang['Version'];
            }
            $this->version = $this->lang_to_process;

            return false;
        }

        $this->objectId = $result['id_product_shop'];

        if (!empty($result['version'])) {
            $currentVersionData = json_decode($result['version'],true);
        }

        $languageVersionsToProcess = $this->getLanguageVersionsToProcess($this->objeto, $currentVersionData);
        $this->lang_to_process = $this->getLanguagesToProcess($languageVersionsToProcess);

        foreach ($languageVersionsToProcess as $isoCode => $version) {
            $this->version[$isoCode] = $version;
        }

        $this->update = true;

        return true;
    }

    /**
     * @return bool|false|null|string
     */
    public function getCurrentMessageVersion($productId)
    {
        $sql = 'SELECT message_version 
                FROM '._DB_PREFIX_.'mmo_connector_product_map 
                WHERE id_product = '.$productId;

        $result = \Db::getInstance()->getValue($sql, false);


        if (!$result) {
            return false;
        }

	    $currentMessageVersion = \DateTime::createFromFormat('Y-m-d H:i:s', $result, new \DateTimeZone('UTC'));

        return $currentMessageVersion;


    }

    /**
     * @param $idProductShop
     * @return bool
     */
    public function deleteProductMap($idProductShop)
    {
        return (bool) \Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'mmo_connector_product_map WHERE id_product_shop ='.(int) $idProductShop, false);
    }

    /**
     * @param $productId
     * @return array
     * @throws \Exception
     */
    public function getImages($productId)
    {
        $imageIds = [];
        $xmlObject = null;

        try{
            $xmlObject = $this->webService->getXMLId('images/products', $productId);
        }
        catch (\Exception $e){
            if ($e->getCode() === 500) {
                MMOLogger::getInstance()->info('PRODUCTO CON ID_SHOP '.$productId.' NO TIENE IMÁGENES PREVIAS. MOTIVO DEL ERROR: '.$e->getMessage());
            }

            MMOLogger::getInstance()->warning('ERROR AL RECUPERAR LAS IMÁGENES PREVIAS  DEL PRODUCTO CON ID_SHOP '.$productId.': '.$e->getMessage());

            if ($e->getCode() === 401) {
                throw $e;
            }
        }

        if (!is_object($xmlObject)) {
            MMOLogger::getInstance()->info("NO SE PUDIERON RECUPRERAR LAS IMÁGENES PREVIAS DEL PRODUCTO CON ID $productId. $xmlObject");
            return $imageIds;
        }

        foreach ($xmlObject->image->declination as $productImage) {
            $attributes = $productImage->attributes();
            $imageIds[] = (int)$attributes['id'];
        }

        return $imageIds;
    }

    /**
     * @param $productId
     * @return bool|mixed
     * @throws \Exception
     */
    public function getCoverImageId($productId)
    {
        MMOLogger::getInstance()->debug('Recuperando cover image del WSgetCoverImageId.');
        $imageIds = $this->getImages($productId);

        if (empty($imageIds)) {
            return false;
        }

        return $imageIds[0];
    }

    /**
     * @param $productId
     * @param $coverImageId
     * @return \SimpleXMLElement|string
     * @throws \Exception
     */
    public function updateCoverImage($productId, $coverImageId)
    {
        try {
            $productXmlData = $this->webService->getXMLId('products', $productId);
            $productXmlData->product->id_default_image = $coverImageId;
            $response = $this->updateProduct($productId, $productXmlData);
            return $response;
        }
        catch (\Exception $e) {
            MMOLogger::getInstance()->warning(
                'NO SE PUEDE ACTUALIZAR LA COVER '. $e->getMessage()
            );

            if ($e->getCode() === 401) {
                throw $e;
            }
        }

        return null;
    }

    /**
     * @param $idProduct
     * @return bool|false|null|string
     */
    public function getIdProductShop($idProduct)
    {
        $id_product_shop = \Db::getInstance()->getValue(
            'SELECT id_product_shop FROM '._DB_PREFIX_.'mmo_connector_product_map WHERE id_product = '.$idProduct,
            false
        );

        if (!$id_product_shop) {
            return false;
        }

        return $id_product_shop;
    }

    public function getXMLBlankObject()
    {
        $this->xmlObject = $this->webService->getXMLModel($this->objectType);
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function getXMLDefaultStructure()
    {
        $lang = $this->getLanguagesShop();
        $structure = $this->structure;
        unset($structure['ProductLangs']['IsoCode']);

        foreach ($structure as $key => $value) {
            try {
                if ($key !== 'ProductLangs' && $key !== 'Brand') {
                    $field = $this->getField($structure, $key);
                    $valor = $this->getValue($structure, $key);
                    $this->xmlObject->product->{$field} = $valor;

                    continue;
                }

                foreach ($value as $clave => $valor) {
                    if ($clave !== 'BrandID') {
                        for ($i = 0, $maxLangs = count($lang); $i < $maxLangs; $i++) {
                            $field = $this->getField($value, $clave);
                            $val = $this->getValue($value, $clave);
                            $this->xmlObject->product->{$field}->language[$i] = $val;
                        }

                        continue;
                    }

                    $field = $this->getField($value, $clave);
                    $val = $this->getValue($value, $clave);
                    $this->xmlObject->product->{$field} = $val;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }
    }

    public function setXMLObjectToWebService()
    {
        $this->unsetProduct();

        if (!$this->update) {
            $this->response = $this->webService->addXMLId($this->objectType, $this->xmlObject);
        } else {
            $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
        }
    }

    /**
     * @param $productId
     * @param $productXmlData
     * @return \SimpleXMLElement|string
     * @throws MMOPrestaShopWebserviceException
     */
    public function updateProduct($productId, $productXmlData)
    {
        $this->xmlObject = $productXmlData;
        $this->unsetProduct();

        return $this->webService->setXMLId($this->objectType, $productId, $this->xmlObject);
    }

    /**
     * @param $productData
     * @return bool|string
     */
    public function setXMLObjectData($productData)
    {
        /*/if (!$this->checkMinimumFormatProduct($productData)) {
            return;
        }*/

        $videoID = '';

        if (isset($productData['Tax']['TaxID'])) {
            $taxId = $productData['Tax']['TaxID'];
            $this->xmlObject->product->id_tax_rules_group = $taxId;
        } else {
            return $this->response = '404 - No existen tasas';
        }

        if (isset($productData['VideoYouTubeID'])) {
            $videoID = $productData['VideoYouTubeID'];
        }

        foreach ($productData as $productField => $productFieldValue) {
            if (!array_key_exists($productField, $this->structure)) {
                if ($productField === 'Tags') {
                    $j = 0;
                    $associatedTags = [];
                    unset($this->xmlObject->product->associations->tags);
                    foreach ($productFieldValue as $productLangField => $productLangValue) {
                        $tagId = $this->getTagID($productLangValue['TagID']);

                        if (!$tagId || array_key_exists($tagId, $associatedTags)) {
                            continue;
                        }

                        $this->xmlObject->product->associations->tags->tag[$j]->id = $tagId;
                        $associatedTags[$tagId] = true;
                        $j = $j + 1;
                    }
                }

                if ($productField === 'Categories') {
                    $this->associateProductCategories($productFieldValue);
                }

                if ($productField === 'Stock') {
                    $this->xmlObject->product->available_for_order = 0;

                    if ($productFieldValue > 0) {
                        $this->xmlObject->product->available_for_order = 1;
                    }
                }

                continue;
            }

            if ($productField === 'ProductLangs') {
                if (!empty($this->lang_to_process)) {
                    $productLangsData = $productFieldValue;

                    foreach ($productLangsData as $productLang) {
                        $idLang = $this->getIdLangFromIso($productLang['IsoCode']);

                        foreach ($productLang as $productLangField => $productLangValue) {
                            if ($productLangField === 'IsoCode'
                                || $productLangField === 'Version'
                                || $productLangField === 'Link') {
                                continue;
                            }

                            for ($i = 0, $iMax = count($this->xmlObject->product->name->language); $i < $iMax; $i++) {
                                foreach ($this->xmlObject->product->name->language[$i]->attributes() as $keyAtrr => $valueAtt) {
                                    if ($valueAtt != $idLang || $keyAtrr != 'id' || !array_key_exists($productLang['IsoCode'], $this->lang_to_process)) {
                                        continue;
                                    }

                                    $this->version[$productLang['IsoCode']] = $productLang['Version'];

                                    try {
                                        $field = $this->getField($this->structure['ProductLangs'],$productLangField);
                                    }
                                    catch (\Exception $e) {
                                        MMOLogger::getInstance()->warning($e->getMessage());
                                        continue;
                                    }

                                    $this->xmlObject->product->{$field}->language[$i] = str_replace($this->replace_product_name, '', $productLangValue);

                                    if ($productLangField === 'Title') {
                                        $this->xmlObject->product->link_rewrite->language[$i] = $this->slugify($productLangValue);
                                    }

                                    if ($productLangField === 'Description') {
                                        $node= dom_import_simplexml($this->xmlObject->product->description->language[$i]);
                                        $no = $node->ownerDocument;

                                        if (!empty($videoID)) {
                                            $description = '<div class="col-md-6">'.$productLangValue.'</div><div class="col-md-6"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$videoID.'" frameborder="0" allowfullscreen></iframe></div></div>';
                                            $node->replaceChild($no->createCDATASection($description), $node->firstChild);
                                        } else {
                                            $description = '<div class="col-md-6">'.$productLangValue.'</div>';
                                            $node->replaceChild($no->createCDATASection($description), $node->firstChild);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if ($productField === 'Brand') {
                if (!empty($productFieldValue) && isset($productFieldValue['BrandID'])) {
                    $this->xmlObject->product->id_manufacturer = $this->getManufacturerID($productFieldValue['BrandID']);
                }
            }
            else if ($productField === 'DefaultCategory') {
                $this->xmlObject->product->id_category_default = $this->getCategoryID($productFieldValue);
            }
            else if ($productField === 'Price') {
                $price = $this->calculatePrice($productFieldValue, $taxId);

                if (null === $price) {
                    return $this->response = "404 - No se ha configurado el grupo de tasas con ID $taxId para el país por defecto de la tienda " . \Configuration::get('PS_COUNTRY_DEFAULT');
                }

                $this->xmlObject->product->price = $price;
            }
            else if ($productField === 'Images') {
                continue;
            }
            else {
                try {
                    $field = $this->getField($this->structure, $productField);
                }
                catch (\Exception $e) {
                    MMOLogger::getInstance()->warning($e->getMessage());
                    continue;
                }

                try {
                    $this->xmlObject->product->{$field} = $productFieldValue;
                } catch (\Exception $e) {
                    MMOLogger::getInstance()->critical("No se puede asignar el elemento {$field} al producto con ID {$productData['ProductID']}. ".$e->getMessage(), [$field => $productFieldValue, 'Trace' => $e->getTrace()]);
                }
            }
        }

        $this->xmlObject->product->show_price = 1;
        $this->xmlObject->product->show_condition = 1;
        $this->xmlObject->product->id_supplier = $this->getSupplierID();
        $this->xmlObject->product->minimal_quantity = 1;
        $this->xmlObject->product->supplier_reference = $this->objectExternalId;

        if ($this->update) {
            $this->xmlObject->product->id = $this->objectId;
        } else {
            unset($this->xmlObject->product->id);
        }

        $this->setXMLObjectToWebService();

        return true;
    }

    /**
     * @param $objectData
     * @return null|string
     */
    public function process($objectData)
    {
        $logger = MMOLogger::getInstance();
        $this->update = false;
        $this->response = null;
        $this->objectId = null;
        $this->objectExternalId = $objectData['ProductID'];
        $this->objeto = $objectData;
        $this->messageVersion = $objectData['MessageVersion'];
        $productMappingExists = $this->exists();

        if (!$productMappingExists) {
            $this->removeExistingUnmappedProducts($this->objectExternalId);
        }

        if (isset($objectData['Variations'])) {
            $this->fatherProduct = true;
        }

        if ($productMappingExists) {
            $this->getXMLObjectFromWebService();
        }

        if ($this->update && !is_object($this->xmlObject)) {
            $this->update = false;
        }

        if (!$productMappingExists) {
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }
        else if ($productMappingExists && !is_object($this->xmlObject)) {
            $this->removeExistingUnmappedProducts($this->objectExternalId);
            $this->resetVersion($this->objectExternalId);
            $this->version = $this->getLanguageVersionsToProcess($objectData, []);
            $this->lang_to_process = $this->getLanguagesToProcess($this->version);
            $logger->warning('Producto mapeado pero no existe en la tienda. Vamos a intentar crearlo de todos modos. Webservice message: '.$this->xmlObject);
            $this->response = null;
            $this->objectId = null;
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }

        $this->setXMLObjectData($objectData);

        if (is_string($this->response)) {
            $logger->error('NO SE PUEDE CREAR EL PRODUCTO : '.$this->response);
            return $this->response;
        }

        if (!$productMappingExists && $this->update === false) {
            $this->createProductMap();
        }
        else if ($productMappingExists && $this->update === false) {
            $this->remapProductMap();
        }
        else {
            $this->updateProductMap();
        }

        $this->update = false;
        $this->fatherProduct = false;
        $this->registerLog($this->response, $objectData['ProductID'], 'PRODUCT', $productMappingExists);

        return $this->response;
    }

    /**
     * Activa/desactiva un producto existente.
     *
     * @param array $objectData
     * @return bool
     */
    public function setActiveProduct($objectData)
    {
        if (!empty($objectData['ProductID'])) {
            $this->objectExternalId = $objectData['ProductID'];
        }

        if (empty($objectData['ProductExternalID']) && empty($objectData['ProductID'])) {
            MMOLogger::getInstance()->warning("Los datos del producto con ID {$objectData['ProductID']} no son validos.");
            return false;
        }

        $this->objeto = $objectData;

        if (!$this->exists()) {
            MMOLogger::getInstance()->warning("El producto con ID {$objectData['ProductID']} no se encontró en el mapeo.");
            return false;
        }

        $this->getXMLObjectFromWebService();

        if (!is_object($this->xmlObject)) {
            MMOLogger::getInstance()->warning("Error del WS al recuperar el producto con ID {$objectData['ProductID']}.");
            return false;
        }

        $this->xmlObject->product->active = 1;

        if (!empty($objectData['Active'])) {
            $this->xmlObject->product->active = $objectData['Active'];
        }

        $this->setXMLObjectToWebService();

        return $this->response;
    }

    /**
     * Devuelve el array de Product IDs que con el mismo SKU y supplier SKU
     *
     * @param string $reference
     * @param string $supplierReference
     * @return array
     */
    public function getIdsByReferenceAndSupplierReference ($reference, $supplierReference)
    {
        $productIds = [];

        if (!$reference || empty ($reference) || !$supplierReference || empty($supplierReference)) {
            return $productIds;
        }

        try {
            $productsXml = $this->webService->getXMLList(
                [
                    'resource' => $this->objectType,
                    'filter[supplier_reference]' => "[$supplierReference]",
                    'filter[reference]' => "[$reference]",
                ]
            );

            foreach ($productsXml->products->product as $product) {
                $productId = (int)$product['id'];

                if(!$productId) {
                    continue;
                }

                $productIds[] = $productId;
            }
        }
        catch (\Exception $e) {
            MMOLogger::getInstance()->critical("Could not retrieve product IDS for supplier reference $supplierReference");
        }

        return $productIds;
    }

    /**
     * @param string $supplierReference
     * @return array
     */
    public function getIdsBySupplierReference ($supplierReference)
    {
        $productIds = [];

        if (!$supplierReference || empty($supplierReference)) {
            return $productIds;
        }

        try {
            $productsXml = $this->webService->getXMLList(['resource' => $this->objectType, 'filter[supplier_reference]' => "[$supplierReference]"]);

            foreach ($productsXml->products->product as $product) {
                $productId = (int)$product['id'];

                if(!$productId) {
                    continue;
                }

                $productIds[] = $productId;
            }
        }
        catch (\Exception $e) {
            MMOLogger::getInstance()->critical("Could not retrieve product IDS for supplier reference $supplierReference");
        }

        return $productIds;
    }

    /**
     * @param array $productIds
     */
    public function deleteProducts(array $productIds)
    {
        foreach ($productIds as $productId) {
            try {
                $this->webService->delete(['resource' => $this->objectType, 'id' => $productId]);

                MMOLogger::getInstance()->critical("PRODUCT DELETED - ID $productId");
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->critical("Could not delete product with ID $productId. ".$e->getMessage());
            }
        }
    }

    /**
     * Desactiva un producto existente.
     *
     * @param array $objectData
     * @return bool
     */
    public function checkMessageVersion($objectData)
    {
        if (!empty($objectData['ProductID'])) {
            $this->objectExternalId = $objectData['ProductID'];
        }

        if (empty($objectData['ProductExternalID']) && empty($objectData['ProductID'])) {
            MMOLogger::getInstance()->warning("Los datos del producto con ID {$objectData['ProductID']} no son validos.");
            return false;
        }

        if (empty($objectData['MessageVersion'])) {
            MMOLogger::getInstance()->warning("Los datos del producto con ID {$objectData['ProductID']} no tienen version.");
            return false;
        }

        $result =  $this->getCurrentMessageVersion($objectData['ProductID']);

        if ($result) {
            $this->messageVersion = $result;
        } else {
            $this->messageVersion = $objectData['MessageVersion'];
        }

        $this->objeto = $objectData;

        if (!$this->exists()) {
            MMOLogger::getInstance()->warning("El producto con ID {$objectData['ProductID']} no se encontró en el mapeo.");
            return true;
        }

        $objectMessageVersion = new \DateTime($objectData['MessageVersion']);

        if ($result) {
            MMOLogger::getInstance()->warning("Current message version ".$result->format('Y-m-d H:i:s'));
        }

        if ($objectMessageVersion) {
            MMOLogger::getInstance()->warning("ObjectData message version ".$objectMessageVersion->format('Y-m-d H:i:s'));
        }

        if (!empty($result) && $result > $objectMessageVersion) {
            MMOLogger::getInstance()->warning("{$objectData['ProductID']} desactualizado");
            return false;
        }

        MMOLogger::getInstance()->warning("{$objectData['ProductID']} a procesar");

        return true;
    }

    /**
     * Desactiva un producto existente.
     *
     * @param array $objectData
     * @return bool
     */
    public function disableProduct($objectData)
    {
        if (!empty($objectData['ProductID'])) {
            $this->objectExternalId = $objectData['ProductID'];
        }

        if (empty($objectData['ProductExternalID']) && empty($objectData['ProductID'])) {
            MMOLogger::getInstance()->warning("Los datos del producto con ID {$objectData['ProductID']} no son validos.");
            return false;
        }

        $this->objeto = $objectData;

        if (!$this->exists()) {
            MMOLogger::getInstance()->warning("El producto con ID {$objectData['ProductID']} no se encontró en el mapeo.");
            return false;
        }

        $this->getXMLObjectFromWebService();

        if (!is_object($this->xmlObject)) {
            MMOLogger::getInstance()->warning("Error del WS al recuperar el producto con ID {$objectData['ProductID']}.");
            return false;
        }

        $productStockAvailableId = $this->xmlObject->product->associations->stock_availables->stock_available->id;

        /**
         * Forzamos el stock a 0 en los productos desactivados para evitar que pase el dato en modulos de terceros.
         */
        $stockManager = new MMOStock();
        $this->response = $stockManager->process($productStockAvailableId, 0);

        if ((int)$this->xmlObject->product->active === 1) {
            $this->xmlObject->product->active = 0;
            $this->setXMLObjectToWebService();
            $this->resetVersion($objectData['ProductID']);
        }

        return $this->response;
    }

    /**
     * @param $price
     * @param $taxRuleGroupId
     * @return int|string
     */
    public function calculatePrice($price, $taxRuleGroupId)
    {
        $taxRuleManager = new MMOTaxesRules();
        $taxRate = $taxRuleManager->getTaxRateByTaxRuleGroup($taxRuleGroupId);

        if (null === $taxRate) {
            return $taxRate;
        }

        $taxRateDiscount = 1 + ($taxRate / 100);
        $priceWithoutTax = $price / $taxRateDiscount;

        return number_format($priceWithoutTax, 6, '.', '');
    }

    /**
     * Resetea las propriedades de sola lectura del producto cargado desde WS.
     */
    public function unsetProduct()
    {
        unset($this->xmlObject->product->manufacturer_name);
        unset($this->xmlObject->product->quantity);
        unset($this->xmlObject->product->id_default_image);
        unset($this->xmlObject->product->associations->product_option_values);
        unset($this->xmlObject->product->associations->product_features);
        unset($this->xmlObject->product->associations->accessories);
        unset($this->xmlObject->product->associations->product_bundle);
        unset($this->xmlObject->product->associations->combinations);
        unset($this->xmlObject->product->position_in_category);
    }

    /**
     * Función para crear el mapping del producto
     */
    public function createProductMap()
    {
        $vers = json_encode($this->version);
        $messageVersion = new \DateTime($this->messageVersion);

        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'mmo_connector_product_map (id_product_shop ,id_product, version, date_add, message_version) 
                VALUES (' . $this->response->product->id . ',' . $this->objectExternalId .",'". $vers . "', NOW(), '" . $messageVersion->format('Y-m-d H:i:s') . "' );";
        \Db::getInstance()->execute($sql, false);
        $this->version = '';
    }

    /**
     * Función para actualizar el mapping de producto
     */
    public function updateProductMap()
    {
        $vers = json_encode($this->version);
        $messageVersion = new \DateTime($this->messageVersion);

        $sql = 'UPDATE '._DB_PREFIX_."mmo_connector_product_map 
                SET version = '".$vers."' ,date_update = NOW(), message_version = '".$messageVersion->format('Y-m-d H:i:s')."' WHERE id_product_shop = {$this->objectId};";
        \Db::getInstance()->execute($sql, false);
        $this->version = '';
    }

    /**
     * Función para actualizar el mapping de producto
     */
    public function remapProductMap()
    {
        $vers = json_encode($this->version);
        $messageVersion = new \DateTime($this->messageVersion);

        $sql = 'UPDATE '._DB_PREFIX_."mmo_connector_product_map 
                SET version = '".$vers."' , date_update = NOW(), id_product_shop = {$this->response->product->id}, message_version = '".$messageVersion->format('Y-m-d H:i:s')."' WHERE id_product = {$this->objectExternalId};";
        \Db::getInstance()->execute($sql, false);
        $this->version = '';
    }

    public function createSupplierAssociation()
    {
        $supplierId = \Configuration::get('MMO_CONNECTOR_SUPPLIER');

        \Db::getInstance()->execute(
            'UPDATE '._DB_PREFIX_.'product as p INNER JOIN '._DB_PREFIX_.'mmo_connector_product_map AS m SET p.id_supplier = '.$supplierId.' WHERE p.id_product = m.id_product_shop AND p.id_supplier <> '.$supplierId.';',
            false
        );

        \Db::getInstance()->execute(
            'INSERT INTO `'._DB_PREFIX_.'product_supplier` (`id_product`, `id_product_attribute`, `id_supplier`, `product_supplier_reference`, `product_supplier_price_te`) 
                    SELECT p.id_product, 0, '.$supplierId.', reference, 0.000000 
                    FROM '._DB_PREFIX_.'product as p 
                    INNER JOIN '._DB_PREFIX_.'mmo_connector_product_map AS m ON p.id_product = m.id_product_shop
	                WHERE p.id_product NOT IN (SELECT id_product FROM '._DB_PREFIX_.'product_supplier);',
            false
        );
    }


    /**
     * @param int $productExternalId
     */
    private function removeExistingUnmappedProducts($productExternalId)
    {
        /**
         * Buscamos productos creados sin mapear en procesos anteriores
         */
        $productIds = $this->getIdsBySupplierReference($productExternalId);

        if (!empty($productIds)) {
            /**
             * Borramos los productos sin mapear
             */
            $this->deleteProducts($productIds);
        }
    }

    /**
     * @param int $productExternalId
     */
    private function resetVersion($productExternalId)
    {
        $sql = 'UPDATE '._DB_PREFIX_."mmo_connector_product_map 
                SET version = DEFAULT, version_img = DEFAULT, date_update = NOW() WHERE id_product = {$productExternalId};";

        \Db::getInstance()->execute($sql, false);
    }

    /**
     * @param array $productData
     * @param array $currentLanguageVersions
     *
     * @return array
     */
    private function getLanguageVersionsToProcess(array $productData, array $currentLanguageVersions)
    {
        $languagesVersionsToProcess = [];

        if (empty($productData['ProductLangs'])) {
            return $languagesVersionsToProcess;
        }

        foreach ($productData['ProductLangs'] as $productLangData) {
            if (array_key_exists($productLangData['IsoCode'], $currentLanguageVersions) && $currentLanguageVersions[$productLangData['IsoCode']] <= $productLangData['Version']) {
                continue;
            }

            $languagesVersionsToProcess[$productLangData['IsoCode']] = $productLangData['Version'];
        }

        return $languagesVersionsToProcess;
    }

    /**
     * @param array $languagesVersionsToProcess
     * @return array
     */
    private function getLanguagesToProcess(array $languagesVersionsToProcess)
    {
        $languagesToProcess = [];

        foreach ($languagesVersionsToProcess as $languageIsoCode => $version) {
            $languagesToProcess[$languageIsoCode] = $languageIsoCode;
        }

        return $languagesToProcess;
    }

    /**
     * @param array $productCategoriesData
     */
    private function associateProductCategories($productCategoriesData)
    {
        $isHome = false;
        foreach ($this->xmlObject->product->associations->categories->category as $value) {
            if (2 !== (int)$value->id) {
				continue;
			}
			
			$isHome = true;
			break;
        }

        unset($this->xmlObject->product->associations->categories);

        $i = 0;

        foreach ($productCategoriesData as $productCategoryData) {
            if (empty($productCategoryData['CategoryID'])) {
                continue;
            }

            $categoryId = (int)$this->getCategoryID($productCategoryData['CategoryID']);

            if (!$categoryId) {
                continue;
            }

            $this->xmlObject->product->associations->categories->category[$i]->id = $categoryId;
            $i++;
        }

        if ($isHome) {
            $this->xmlObject->product->associations->categories->category[]->id = 2;
        }
    }

    /**
     * @param array $productData
     * @return bool
     */
    private function checkMinimumFormatProduct($productData)
    {
        $formatOk = true;
        $typeEmpty = '';
        $arrayImportProduct = array();
        $arrayMinimumFormat = array(
            'ProductID',
            'EAN',
            'SKU',
            'Currency',
            'WholesalePrice',
            'Price',
            'Stock',
            'Width',
            'Height',
            'Length',
            'Weight',
            'Categories',
            'DefaultCategory',
            'ProductLangs',
            'Images',
            'Variations',
        );
        $valueEmpty = array('Stock', 'Active', 'EAN');

        if($this->fatherProduct){
            $arrayMinimumFormat = array('ProductID', 'EAN', 'SKU', 'Currency', 'WholesalePrice', 'Price', 'Stock', 'Width',
                'Height', 'Length', 'Weight', 'Categories', 'DefaultCategory', 'ProductLangs', 'Images', 'Variations');
        }

        foreach ($productData as $productField => $productFieldValue) {
            if (!in_array($productField, $valueEmpty)) {
                $formatOk = false;
                $typeEmpty = $productField;
            }

            $arrayImportProduct[] = $productField;
        }

        $misingData = array_diff($arrayMinimumFormat, $arrayImportProduct);

        if ((!$formatOk) || (!empty($misingData))) {
            $formatOk = false;
            $this->response = 'PRODUCTO NO TIENE TODOS LOS DATOS MÍNIMOS PARA SER PROCESADO '.$typeEmpty.' '.json_encode(
                    $misingData
                );
            MMOLogger::getInstance()->error(
                'PRODUCTO NO TIENE TODOS LOS DATOS MÍNIMOS PARA SER PROCESADO '.$typeEmpty.' '.json_encode($misingData)
            );
        }

        return $formatOk;
    }

    /**
     * @param \ProductCore $product
     * @throws \PrestaShopException
     */
    public function getProductUrl($product)
    {
        $idProduct = $product->id;
        $productAttributes = $product->getAttributeCombinations();
        $this->updateProductUrl($idProduct);

        foreach ($productAttributes as $productAttribute) {
            $idCombination = $productAttribute['id_product_attribute'];
            $this->updateProductUrl($idProduct, $idCombination);
        }
    }

    public function cleanProductUrls()
    {
        try {
            $sql = 'DELETE u.* 
                    FROM '._DB_PREFIX_.'mmo_connector_product_url u 
                    INNER JOIN '._DB_PREFIX_.'product p ON u.id_product = p.id_product 
                    WHERE p.active = 0;';

            \Db::getInstance()->execute($sql);

            $sql = 'DELETE u.* 
                    FROM '._DB_PREFIX_.'mmo_connector_product_url u 
                    INNER JOIN '._DB_PREFIX_.'product p ON u.id_product = p.id_product 
                    WHERE p.active = 0;';

            \Db::getInstance()->execute($sql);

            $sql = 'DELETE i.*, ppi.* 
                    FROM '._DB_PREFIX_.'mmo_connector_image_url AS i 
                    INNER JOIN '._DB_PREFIX_.'mmo_connector_product_image_url as ppi ON i.id_image = ppi.id_image 
                    INNER JOIN '._DB_PREFIX_.'mmo_connector_product_map pm ON ppi.id_product = pm.id_product
                    INNER JOIN '._DB_PREFIX_.'product p ON p.id_product = pm.id_product_shop
                    WHERE p.active = 0';

            \Db::getInstance()->execute($sql);

            $activeLanguageIds = MMOLang::getActiveLanguageIds();

            if (count($activeLanguageIds)) {
                $sql = 'DELETE u.* FROM '._DB_PREFIX_.'mmo_connector_product_url u WHERE u.id_lang NOT IN ('.implode(',', $activeLanguageIds).');';
                \Db::getInstance()->execute($sql);
            }
        } catch (\Exception $exception) {
            MMOLogger::getInstance()->critical(__METHOD__.' - '.$exception->getMessage());
        }
    }

    /**
     * @param $idProduct
     * @param int $idCombination
     * @throws \PrestaShopException
     */
    public function updateProductUrl($idProduct, $idCombination = 0)
    {
        $link = new \Link();
        $cacheId = 'MMOProduct-updateProductUrl-langs-active';

        if (MMOCache::isValid($cacheId)) {
            $languageIds = MMOCache::get($cacheId);
        }
        else {
            $languages = \Language::getLanguages(true);
            $languageIds = array_column($languages, 'id_lang');

            MMOCache::store($cacheId, $languageIds);
        }

        if (!count($languageIds)) {
            return;
        }

        foreach ($languageIds as $languageId) {
           \Context::getContext()->language->id = $languageId;
            $enlace = $link->getProductLink($idProduct, null, null, null, $languageId, 1, $idCombination, false, false, true);

            $result = \Db::getInstance()->getRow('SELECT id_product, url FROM ' . _DB_PREFIX_ . 'mmo_connector_product_url WHERE id_product = ' . $idProduct . ' AND id_combination = '.$idCombination.' AND id_lang = ' . $languageId, false);

            if (!$result) {
                \Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'mmo_connector_product_url (id_product ,id_combination, url, id_lang, date_add, date_update) VALUES ('.$idProduct.','.$idCombination.",'".$enlace."',".$languageId.', NOW(), NOW())', false);

                continue;
            }

            if (mb_strtolower($enlace) !== mb_strtolower($result['url'])) {
                \Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . "mmo_connector_product_url SET url = '" . $enlace . "', date_update = NOW() WHERE id_product = " . $idProduct . ' AND id_combination = '.$idCombination.' AND id_lang = ' . $languageId, false);

                continue;
            }
        }
    }

    /**
     * @param $product
     */
    public function getImagesUrl($product)
    {
        $idLang = \Configuration::get('PS_LANG_DEFAULT');

        $this->updateImageUrl($product, $idLang);
    }

    /**
     * @param $product
     * @param $idLang
     */
    public function updateImageUrl($product, $idLang)
    {
        $link = new \Link();
        $imagesArray = \Image::getImages($idLang, $product->id, null);
        $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $urlProtocol = ($forceSsl ? 'https://' : 'http://');
        $imageIds = array_column($imagesArray, 'id_image');
        \Db::getInstance()->delete('mmo_connector_product_image_url', "id_product = {$product->id} AND id_combination = 0");

        if (empty($imageIds)) {
            return;
        }

        $mmoImageUrlsQuery = 'SELECT id_image, url 
                              FROM ' . _DB_PREFIX_ . 'mmo_connector_image_url 
                              WHERE id_image IN ('.implode(', ',$imageIds).')';

        $mmoImageUrls = \Db::getInstance()->executeS($mmoImageUrlsQuery, true, false);
        $mmoImageUrlsIndexedByImageId = array_column($mmoImageUrls, 'url', 'id_image');
        $addProductImageUrlsQueryValues = [];

        foreach ($imagesArray as $image) {
            $enlace = $urlProtocol.$link->getImageLink($product->link_rewrite, $image['id_image'],'medium_default');
            $cover = $image['cover'];

            if (null === $image['cover']) {
                $cover = 0;
            }

            if (!array_key_exists($image['id_image'], $mmoImageUrlsIndexedByImageId)) {
                \Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'mmo_connector_image_url (id_image, cover, url, date_add, date_update) VALUES ('.$image['id_image'].','.$cover.",'".$enlace."',NOW(),NOW())", false);
            }
            else if (mb_strtolower($enlace) !== mb_strtolower($mmoImageUrlsIndexedByImageId[$image['id_image']])) {
                \Db::getInstance()->execute('UPDATE '._DB_PREFIX_."mmo_connector_image_url SET url = '" . $enlace . "', cover = ".$cover.', date_update = NOW() WHERE id_image = ' .$image['id_image'], false);
            }

            $addProductImageUrlsQueryValues[] = '('.$product->id.', 0,'.$image['id_image'].')';
        }

        if (empty($addProductImageUrlsQueryValues)) {
            return;
        }

        $addProductImageUrlsQuery = 'INSERT INTO '._DB_PREFIX_.'mmo_connector_product_image_url (id_product, id_combination, id_image) 
                                     VALUES '.implode(', ', $addProductImageUrlsQueryValues);
        \Db::getInstance()->execute($addProductImageUrlsQuery, false);
    }

    /**
     * @param $date
     * @return mixed
     */
    public static function getUrlsImages ($date)
    {
        $sql = 'SELECT distinct (pu.id_product), iu.url, iu.cover, iu.id_image 
                FROM ' . _DB_PREFIX_ . 'mmo_connector_product_image_url as piu 
                INNER JOIN ' . _DB_PREFIX_ . 'mmo_connector_product_map as pu 
                    ON pu.id_product_shop = piu.id_product 
                INNER JOIN ' . _DB_PREFIX_ . "mmo_connector_image_url as iu 
                    ON iu.id_image = piu.id_image 
                WHERE iu.date_add > '".$date."' OR iu.date_update > '".$date."';";

        $result = \Db::getInstance()->query($sql);

        return $result;
    }

    /**
     * @param $date
     * @return mixed
     */
    public static function getUrlsProducts ($date)
    {
        $sql = 'SELECT distinct (pm.id_product), pu.id_combination, pu.url, pu.id_lang 
                FROM ' . _DB_PREFIX_ . 'mmo_connector_product_url AS pu 
                INNER JOIN ' . _DB_PREFIX_ . "mmo_connector_product_map as pm 
                    ON pm.id_product_shop = pu.id_product 
                WHERE pu.date_update > '".$date."' OR pu.date_add > '".$date."';";

        $result = \Db::getInstance()->query($sql);

        return $result;
    }

    /**
     * @param null $date
     * @return mixed
     */
    public static function getIdProductUrl ($date = null)
    {
        $result = \Db::getInstance()->executeS("SELECT distinct id_product FROM " . _DB_PREFIX_ . "mmo_connector_product_url WHERE date_update > '" .$date."'", true, false);

        return $result;
    }

    /**
     * @param null $date
     * @return mixed
     */
    public static function getIdsLangUrl ($date = null)
    {
        $result = \Db::getInstance()->query(
            'SELECT distinct id_lang 
             FROM ' . _DB_PREFIX_ . "mmo_connector_product_url 
             WHERE date_add > '" .$date."' OR date_update > '" .$date."'"
        );

        return $result;
    }

    /**
     * Crea un slug a partir del texto.
     *
     * Los pasos de convesión:
     * 1- sustituye los caracteres unicode
     * 2- convierte los caracteres con accento a caracteres sin accento
     * 3- ejecuta la funcción str2url de PrestaShop
     *
     * @param $text
     * @return mixed|string
     */
    public function slugify ($text)
    {
        $slug = $this->sanitizeUnicodeCharacters($text);
        $slugifier = new Slugify();
        $slug = $slugifier->slugify($slug);
        $slug = \ToolsCore::str2url($slug);

        return $slug;
    }

    /**
     * Sustituye los caracteres unicode
     *
     * @param $text
     * @return mixed
     */
    private function sanitizeUnicodeCharacters ($text)
    {
        $result = str_replace('\\/', '/', $text);

        if (!function_exists('mb_convert_encoding')) {
            return $result;
        }

        // http://stackoverflow.com/questions/2934563/how-to-decode-unicode-escape-sequences-like-u00ed-to-proper-utf-8-encoded-cha
        $result = preg_replace_callback('/(\\\\+)u([0-9a-f]{4})/i', function($match) {
            $l = strlen($match[1]);

            if ($l % 2) {
                return str_repeat('\\', $l - 1) . mb_convert_encoding(
                        pack('H*', $match[2]),
                        'UTF-8',
                        'UCS-2BE'
                    );
            }

            return $match[0];
        }, $result);

        return $result;
    }

    /**
     * @return MMOProductReport
     */
    public function getProductReport()
    {
        $response = new MMOProductReport();

        $response->TotalProducts = \Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM '._DB_PREFIX_.'mmo_connector_product_map',
            false
        );
        $response->ActiveProducts = \Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM '._DB_PREFIX_.'mmo_connector_product_map pm 
            INNER JOIN '._DB_PREFIX_.'product p ON p.id_product = pm.id_product_shop AND p.active = 1', false
        );
        $response->Products = \Db::getInstance()->executeS(
            'SELECT DISTINCT(pm.id_product) AS ProductID, p.reference AS SKU, p.active AS Active, IF(pa.reference IS NULL, sa.quantity, NULL) AS Stock
            FROM '._DB_PREFIX_.'mmo_connector_product_map pm 
            INNER JOIN '._DB_PREFIX_.'product p ON p.id_product = pm.id_product_shop 
            INNER JOIN '._DB_PREFIX_.'stock_available sa ON sa.id_product = pm.id_product_shop
            LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product = p.id_product',
            true,
            false
        );

        return $response;
    }

    /**
     * @return MMOProductVariationReport
     */
    public function getProductVariationReport()
    {
        $response = new MMOProductVariationReport();

        $response->TotalProductVariations = \Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM '._DB_PREFIX_.'mmo_connector_product_attribute_map',
            false
        );
        $response->ActiveProductVariations = \Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM '._DB_PREFIX_.'mmo_connector_product_attribute_map pam 
            INNER JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product_attribute = pam.id_product_attribute_shop
            LEFT JOIN '._DB_PREFIX_.'product p ON p.id_product = pa.id_product
            WHERE p.active = 1',
            false
        );
        $response->ProductVariations = \Db::getInstance()->executeS(
            'SELECT DISTINCT(pam.id_product_attribute) AS ProductVariationID, pa.reference AS SKU, p.active AS Active, pa.quantity AS Stock
            FROM '._DB_PREFIX_.'mmo_connector_product_attribute_map pam 
            INNER JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product_attribute = pam.id_product_attribute_shop
            INNER JOIN '._DB_PREFIX_.'stock_available sa ON sa.id_product = pa.id_product
            LEFT JOIN '._DB_PREFIX_.'product p ON p.id_product = pa.id_product',
            true,
            false
        );

        return $response;
    }

    /**
     * @param int $idProduct
     * @param \DateTime $nextVersion
     * @return bool
     */
    public function setMessageVersion($idProduct, $nextVersion)
    {
        $sql = 'UPDATE '._DB_PREFIX_.'mmo_connector_product_map 
                    SET message_version = \''.$nextVersion->format('Y-m-d H:i:s').'\'
                WHERE id_product = '.$idProduct;

        $result = \Db::getInstance()->execute($sql, false);

        return $result;
    }
}