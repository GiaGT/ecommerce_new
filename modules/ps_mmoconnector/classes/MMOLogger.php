<?php

namespace MIP\PrestaShop;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger as MonologLogger;

/**
 * Class Logger
 *
 * @method emergency($message, $data = [])
 * @method alert($message, $data = [])
 * @method critical($message, $data = [])
 * @method error($message, $data = [])
 * @method warning($message, $data = [])
 * @method notice($message, $data = [])
 * @method info($message, $data = [])
 * @method debug($message, $data = [])
 */
class MMOLogger
{
    const MAX_FILES = 7;

    /**
     * @var MMOLogger
     */
    private static $logger;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $appLogger;

    /**
     * MMOLogger constructor.
     * @throws \Exception
     */
    private function __construct()
    {
        $oldPermissions = umask(0000);
        $loggingDir = \Ps_Mmoconnector::MODULE_DIR."/logs";

        if (!@mkdir($loggingDir) && !is_dir($loggingDir)) {
            throw new \Exception("No se pudo crear la carpeta de logs {$loggingDir}");
        }

        @chmod($loggingDir, 0775);

        $loggingFilename = $loggingDir."/".\Ps_Mmoconnector::MODULE_NAME.".log";

        if (!file_exists($loggingFilename)) {
            file_put_contents($loggingFilename, "");
            @chmod($loggingFilename, 0775);
        }

        $this->appLogger = new MonologLogger(\Ps_Mmoconnector::MODULE_NAME);
        /**
         * Use MonologLogger::DEBUG level to dump extended processing information
         */
        $this->appLogger->pushHandler(new RotatingFileHandler($loggingFilename, self::MAX_FILES, MonologLogger::ERROR));
        umask($oldPermissions);
    }

    /**
     * @return MMOLogger
     */
    public static function getInstance()
    {
        if (!self::$logger) {
            self::$logger = new self();
        }

        return self::$logger;
    }

    /**
     * @param $levelName
     * @param $arguments
     * @return mixed
     */
    public function __call($levelName, $arguments)
    {
        if (!$this->isValidLevel($levelName)) {
            throw new \InvalidArgumentException("$levelName logging level is invalid.");
        }

        $argumentsCount = count($arguments);

        if (0 === $argumentsCount) {
            throw new \InvalidArgumentException("Logger needs at least a message.");
        }

        $message = $arguments[0];
        $data = [];

        if (2 === $argumentsCount) {
            $data = $arguments[1];

            if (!is_array($data)) {
                $data = [$data];
            }
        }

        return $this->appLogger->{$levelName}($message, $data);
    }

    /**
     * @param string $levelName
     * @return bool
     */
    private function isValidLevel($levelName)
    {
        $levels = MonologLogger::getLevels();

        if (!isset($levels[strtoupper($levelName)])) {
            return false;
        }

        return true;
    }
}