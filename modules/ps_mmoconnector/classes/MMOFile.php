<?php

namespace MIP\PrestaShop;

use MIP\PrestaShop\Model\MMOFileReport;

class MMOFile
{
    /**
     * Función para almacenar la información de un nuevo fichero
     *
     * @param $fileName
     * @param $version
     * @return bool
     */
    public static function insertMMOConectorFileLog($fileName, $version)
    {
        if (!$version) {
            $version = 0;
        }

        $sql = 'INSERT INTO `'._DB_PREFIX_.'mmo_connector_file_log` (`name`, `version`, `date_add`)'
            ."VALUES ('".$fileName."','".$version."',NOW())";

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    /**
     * Función para indicar un fichero como procesado.
     *
     * @param $idFile
     * @return bool
     */
    public static function updateMMOConectorFileLog($idFile)
    {
        $sql = 'UPDATE `'._DB_PREFIX_."mmo_connector_file_log` SET date_process = NOW() WHERE id_file = '$idFile'";
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    /**
     * Función para obtener los ficheros que no han sido procesados
     *
     * @return \stdClass
     * @throws \PrestaShopDatabaseException
     */
    public static function getFilesWithoutProcess()
    {
        $file = new \stdClass;
        $file->id_file = 0;
        $file->name = null;
        $file->version = 0;

        $sql = 'SELECT `id_file`, `name`, `version` FROM `'._DB_PREFIX_.'mmo_connector_file_log` WHERE date_process IS NULL AND date_processing_start IS NULL AND in_process = 0 LIMIT 1';

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        if(!$result) {
            return $file;
        }

        foreach ($result as $r) {
            $file->id_file = $r['id_file'];
            $file->name = $r['name'];
            $file->version = $r['version'];
        }

        return $file;
    }

    /**
     * Función para obtener el estado del fichero de importación
     *
     * @param $submissionId
     * @return \stdClass
     * @throws \PrestaShopDatabaseException
     */
    public static function getStateFileImport($submissionId)
    {
        $fileState = new \stdClass;
        $fileState->state = '400';
        $fileState->message = 'Invalid submission ID';

        if (empty($submissionId)) {
            return $fileState;
        }

        $sql = 'SELECT * FROM `'._DB_PREFIX_."mmo_connector_file_log` WHERE name = '$submissionId' LIMIT 1";
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        if (empty($result) || empty($result[0]) || empty($result[0]['id_file'])) {
            $fileState->state = '404';
            $fileState->message = 'Fichero no encontrado';

            return $fileState;
        }

        if (empty($result[0]['date_process'])) {
            $fileState->state = '200';
            $fileState->message = 'Fichero no procesado';

            return $fileState;
        }

        $fileState->state = '200';
        $fileState->message = 'Fichero procesado';

        return $fileState;
    }

    public static function getFile($fileName)
    {
        $file = new \stdClass;
        $file->id_file = 0;
        $file->name = null;
        $file->version = 0;

        $sql = 'SELECT `id_file`, `name`, `version` FROM `'._DB_PREFIX_.'mmo_connector_file_log` WHERE name = \''.$fileName.'\' LIMIT 1';

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        if(!$result) {
            return $file;
        }

        foreach ($result as $r) {
            $file->id_file = $r['id_file'];
            $file->name = $r['name'];
            $file->version = $r['version'];
        }

        return $file;
    }

    public static function updateInProcessState($idFile)
    {
        $sql = 'UPDATE `'._DB_PREFIX_."mmo_connector_file_log` SET in_process = 1, date_processing_start = NOW() WHERE id_file = '$idFile'";

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    public static function updateFinishedProcessingState($idFile)
    {
        $sql = 'UPDATE `'._DB_PREFIX_."mmo_connector_file_log` SET in_process = 0 WHERE id_file = '$idFile'";

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    public static function existFileInProcess()
    {
        $in_process = 0;
        $sql = 'SELECT in_process AS in_process FROM `'._DB_PREFIX_.'mmo_connector_file_log` WHERE in_process = 1';

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        if (empty($result[0]['in_process'])) {
            return $in_process;
        }

        $in_process = 1;

        return $in_process;
    }

    /**
     * @return array|false|\mysqli_result|null|\PDOStatement|resource
     * @throws \PrestaShopDatabaseException
     */
    public static function unlockFileProcesing()
    {
        $sql = 'SELECT 
                file_log.id_file AS id_file, 
                import_process.id AS import_process_id, 
                file_log.date_processing_start, 
                MAX(import_process.date_add) AS last_add, 
                MAX(import_process.date_update) AS last_update
            FROM '._DB_PREFIX_.'mmo_connector_file_log file_log
            LEFT JOIN '._DB_PREFIX_.'mmo_connector_import_process import_process ON file_log.id_file = import_process.id_file
            WHERE 
                file_log.in_process = 1
            GROUP BY file_log.id_file
            HAVING (import_process.id IS NOT NULL AND GREATEST(COALESCE(last_add, last_update), COALESCE(last_update, last_add)) < (NOW() - INTERVAL 5 MINUTE))
                OR (import_process.id IS NULL AND file_log.date_processing_start < (NOW() - INTERVAL 10 MINUTE));';

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }

    /**
     * @param $id_file
     */
    public static function resetStateFile($id_file)
    {
        MMOLogger::getInstance()->critical("MMOFile->resetStateFile: $id_file");

        $sql = 'UPDATE '._DB_PREFIX_.'mmo_connector_file_log SET `in_process` = 0, `date_processing_start`=NULL, `date_process`=NULL WHERE id_file ='.$id_file;
        \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);
    }

    /**
     * @param $id_file
     */
    public static function sendEmailResetStateFile($id_file)
    {
        /*$smtpChecked = ('smtp');
        $smtpServer = \Configuration::get('PS_MAIL_SERVER');
        $content = urldecode(
            "SE HA RESETEADO EL FICHERO CON ID: ".$id_file."\nURL DE LA TIENDA: ".\Configuration::get('PS_SHOP_DOMAIN')
        );
        $content = html_entity_decode($content);
        $subject = urldecode('Reset State File MIP '.\Configuration::get('PS_SHOP_DOMAIN'));
        $type = 'text/html';
        $to = "support@bigbuy.eu";

        $from = \Configuration::get('PS_MAIL_USER');
        $smtpLogin = \Configuration::get('PS_MAIL_USER');
        $smtpPassword = \Configuration::get('PS_MAIL_PASSWD');
        $smtpPassword = (!empty($smtpPassword)) ? urldecode($smtpPassword) : \Configuration::get('PS_MAIL_PASSWD');
        $smtpPassword = str_replace(
            array('&lt;', '&gt;', '&quot;', '&amp;'),
            array('<', '>', '"', '&'),
            \Tools::htmlentitiesUTF8($smtpPassword)
        );

        $smtpPort = \Configuration::get('PS_MAIL_SMTP_PORT');
        $smtpEncryption = \Configuration::get('PS_MAIL_SMTP_ENCRYPTION');

        try {
            \Mail::sendMailTest(
                \Tools::htmlentitiesUTF8($smtpChecked),
                \Tools::htmlentitiesUTF8($smtpServer),
                \Tools::htmlentitiesUTF8($content),
                \Tools::htmlentitiesUTF8($subject),
                \Tools::htmlentitiesUTF8($type),
                \Tools::htmlentitiesUTF8($to),
                \Tools::htmlentitiesUTF8($from),
                \Tools::htmlentitiesUTF8($smtpLogin),
                $smtpPassword,
                \Tools::htmlentitiesUTF8($smtpPort),
                \Tools::htmlentitiesUTF8($smtpEncryption)
            );
        } catch (\Exception $e) {
            MMOLogger::getInstance()->error(
                'NO SE PUEDE ENVIAR EL CORREO DE RESET STATE FILE '.$e->getMessage()
            );
        }*/
    }

    /**
     * @return array|false|\mysqli_result|null|\PDOStatement|resource
     * @throws \PrestaShopDatabaseException
     */
    public static function getAllFilesProcessed()
    {
        $sql = 'SELECT `name`'
            .'FROM '._DB_PREFIX_.'mmo_connector_file_log file '
            .'WHERE file.date_process < (NOW() - INTERVAL 1 WEEK) '
            .'LIMIT 1000';

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }

    /**
     * @param $filename
     */
    public static function removeFileLog($filename)
    {
        if (empty($filename)) {
            return;
        }

        $sql = 'DELETE FROM '._DB_PREFIX_.'mmo_connector_file_log '
            .'WHERE name = "'.$filename.'" AND date_process < (NOW() - INTERVAL 1 WEEK)';


        \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);
    }

    /**
     * @return MMOFileReport
     */
    public function getFileReport()
    {
        $response = new MMOFileReport();

        $response->TotalFiles =  \Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'mmo_connector_file_log', false);
        $response->PendingFiles = \Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'mmo_connector_file_log WHERE date_process IS NULL AND date_processing_start IS NULL', false);
        $response->ProcessingFiles = \Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'mmo_connector_file_log WHERE in_process = 1', false);

        $lastStartTime = \Db::getInstance()->getValue('SELECT MAX(date_processing_start) FROM ' . _DB_PREFIX_ . 'mmo_connector_file_log', false);
        if ($lastStartTime) {
            $timezone = new \DateTimeZone('UTC');
            $lastStartTime = new \DateTime($lastStartTime, $timezone);
            $response->LastStartTime = $lastStartTime->format(DATE_W3C);
        }else{
            $response->LastStartTime = false;
        }

        $lastAddTime = \Db::getInstance()->getValue('SELECT MAX(date_add) FROM ' . _DB_PREFIX_ . 'mmo_connector_file_log', false);
        if ($lastAddTime) {
            $timezone = new \DateTimeZone('UTC');
            $lastAddTime = new \DateTime($lastAddTime, $timezone);
            $response->LastAddTime = $lastAddTime->format(DATE_W3C);
        }else{
            $response->LastAddTime = false;
        }

        return $response;
    }

    /**
     * @return array|false|\mysqli_result|null|\PDOStatement|resource
     * @throws \PrestaShopDatabaseException
     */
    public static function getAllFilesNotProcessed()
    {
        $sql = 'SELECT `name` '
            .'FROM '._DB_PREFIX_.'mmo_connector_file_log file '
            .'WHERE file.date_process IS NULL';
        MMOLogger::getInstance()->info($sql);
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }
}
