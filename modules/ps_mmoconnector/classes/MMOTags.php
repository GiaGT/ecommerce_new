<?php

namespace MIP\PrestaShop;

class MMOTags extends MMOObject
{
    public $language;
    public $response = false;
    public $update = false;
    public $submittedVersion;
    public $currentVersion;
    public $structure = array(
        'TagID' => array(
            'required' => true,
            'nullable' => false,
            'mapped' => false,
            'field' => 'id',
            'value' => '',
        ),
        'TagLangs' => array(
            'required' => true,
            'nullable' => false,
            'mapped' => false,
            'field' => '',
            'IsoCode' => array(
                'required' => true,
                'nullable' => false,
                'mapped' => true,
                'field' => 'id_lang',
                'value' => '1',
            ),
            'TagName' => array(
                'required' => true,
                'nullable' => false,
                'mapped' => true,
                'field' => 'name',
                'value' => '-',
            ),
        ),
    );

    public function __construct()
    {
        parent::__construct();
        $this->objectType = 'tags';
    }

    public function exists()
    {
        $result = \Db::getInstance()->getRow(
            'SELECT id_tag_shop, version FROM '._DB_PREFIX_.'mmo_connector_tag_map WHERE id_tag = '.$this->objectExternalId.' AND id_lang = '.$this->language,
            false
        );

        if (!$result) {
            return false;
        }

        $this->objectId = $result['id_tag_shop'];

        if (null !== $result['version']) {
            $this->currentVersion = $result['version'];
        }

        return true;
    }

    public function getXMLBlankObject()
    {
        $this->xmlObject = $this->webService->getXMLModel($this->objectType);
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function setXMLObjectData($data)
    {
        if (!$this->checkMinimumFormatTag($data)) {
            return;
        }

        foreach ($data as $dataField => $value) {
            try {
                if ($dataField === 'Version') {
                    continue;
                }

                if ($dataField === 'TagLangs') {
                    foreach ($value as $tagLang) {
                        if (empty($tagLang['IsoCode'])) {
                            continue;
                        }

                        $idLang = $this->getIdLangFromIso($tagLang['IsoCode']);

                        if ($idLang !== $this->language) {
                            continue;
                        }

                        $this->xmlObject->tag->id_lang = $idLang;

                        if (empty($tagLang['TagName']) || strlen($tagLang['TagName']) > 32) {
                            continue;
                        }

                        $this->xmlObject->tag->name = $tagLang['TagName'];
                    }

                    continue;
                }

                if ($this->isMapped($dataField)) {
                    $field = $this->getField($this->structure, $dataField);
                    $this->xmlObject->tag->{$field} = $value;
                    continue;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->critical($e->getMessage());
            }
        }

        if ($this->update) {
            $this->xmlObject->tag->id = $this->objectId;
        } else {
            unset($this->xmlObject->tag->id);
        }

        $this->setXMLObjectToWebService();
    }

    public function getXMLDefaultStructure()
    {
        foreach ($this->structure as $key => $value) {
            try {
                if ($key === 'Version') {
                    continue;
                }

                if ($key === 'TagLangs') {
                    foreach ($value as $ky => $vl) {
                        if ($ky !== 'IsoCode' && $ky !== 'TagName') {
                            continue;
                        }

                        $field = $vl['field'];

                        if ($vl['field'] === 'id_lang') {
                            $this->xmlObject->tag->{$field} = $this->language;
                            continue;
                        }

                        $this->xmlObject->tag->{$field} = $vl['value'];
                    }

                    continue;
                }

                if ($this->isMapped($key)) {
                    $field = $this->getField($this->structure, $key);
                    $valor = $this->getValue($this->structure, $key);
                    $this->xmlObject->tag->{$field} = $valor;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->critical($e->getMessage());
            }
        }
    }

    public function setXMLObjectToWebService()
    {
        try {
            if (!$this->update) {
                $this->response = $this->webService->addXMLId($this->objectType, $this->xmlObject);
            } else {
                $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
            }
        }
        catch (MMOPrestaShopWebserviceException $e) {
            MMOLogger::getInstance()->critical("SALTANDO TAG CON ID {$this->objectExternalId} POR ERROR DEL WS. ".$e->getMessage());

            /**
             * Si el WS nos ha denegado el acceso, propagamos la excepción para gestionarla en el proceso del importación.
             */
            if ($e->getCode() === 401) {
                throw $e;
            }
        }
    }

    /**
     * @param array $tagData
     * @return bool|\stdClass
     */
    public function process($tagData)
    {
        $this->response = false;
        $this->objectId = null;
        $this->objectExternalId = $tagData['TagID'];
        $this->objeto = $tagData;
        $cacheId = 'MMOTags-Language-getIDs';

        if (MMOCache::isValid($cacheId)) {
            $langsShop = MMOCache::get($cacheId);
        }
        else {
            $langsShop = \Language::getIDs();
            MMOCache::store($cacheId, $langsShop);
        }

        if (empty($tagData['TagLangs'])) {
            MMOLogger::getInstance()->info("NO SE ENCONTRARON IDIOMAS PARA EL TAG CON ID {$this->objectExternalId}");
            return new \stdClass();
        }

        foreach ($tagData['TagLangs'] as $tagLangData) {
            if(isset($tagLangData['Version'])) {
                $this->submittedVersion = $tagLangData['Version'];
            }

            $this->language = $this->getIdLangFromIso($tagLangData['IsoCode']);

            if (!in_array($this->language, $langsShop)) {
                continue;
            }

            $tagMappingExists = $this->exists();

            if ($tagMappingExists) {
                $this->update = true;
                $this->getXMLObjectFromWebService();
            }

            if (null !== $this->currentVersion
                && $this->update
                && is_object($this->xmlObject)
                && $this->currentVersion >= $this->submittedVersion) {
                MMOLogger::getInstance()->info("SKIP TAG LANG ID {$this->objectExternalId} LANG ID: {$this->language}. La versión {$this->submittedVersion} es la misma.");
                return new \stdClass();
            }

            if ($this->update && !is_object($this->xmlObject)) {
                $this->update = false;
            }

            if (!$tagMappingExists) {
                $this->getXMLBlankObject();
                $this->getXMLDefaultStructure();
            }
            else if ($tagMappingExists && !is_object($this->xmlObject)) {
                MMOLogger::getInstance()->warning('Producto mapeado pero no existe en la tienda. Vamos a intentar crearlo de todos modos. Webservice message: '.$this->xmlObject);
                $this->response = null;
                $this->objectId = null;
                $this->currentVersion = null;
                $this->getXMLBlankObject();
                $this->getXMLDefaultStructure();
            }

            $this->setXMLObjectData($tagData);
            $this->registerLog($this->response, $tagData['TagID'], 'TAG', $tagMappingExists);

            if (is_string($this->response)) {
                MMOLogger::getInstance()->error(
                    'Error del webservice al procesar el TAG '.$tagData['TagID'].'con isoCode '.$tagLangData['IsoCode']
                );

                return $this->response;
            }

            MMOLogger::getInstance()->info('INSERCION TAG: '.$this->objectExternalId.' - ISO CODE: '.$tagLangData['IsoCode']);

            if (!$tagMappingExists && $this->update === false) {
                $this->createTagMap();
            }
            else if ($tagMappingExists && $this->update === false) {
                $this->remapTagMap();
            }
            else {
                $this->updateTagMap();
            }

            $this->update = false;
        }

        return $this->response;
    }

    public function createTagMap()
    {
        \Db::getInstance()->execute(
			'INSERT INTO '._DB_PREFIX_.'mmo_connector_tag_map (id_tag_shop ,id_tag, version, date_add, id_lang) VALUES ('.$this->response->tag->id.','.$this->objectExternalId.','.$this->submittedVersion.',NOW(),'.$this->language.')',
			false
		);
    }

    public function updateTagMap()
    {
        \Db::getInstance()->execute(
			'UPDATE '._DB_PREFIX_.'mmo_connector_tag_map SET version = '. $this->submittedVersion .', date_update = NOW() WHERE id_tag_shop = '.$this->objectId.' AND id_lang = '.$this->language,
			false
		);
    }

    public function remapTagMap()
    {
        \Db::getInstance()->execute(
			'UPDATE '._DB_PREFIX_.'mmo_connector_tag_map SET id_tag_shop = '.$this->response->tag->id.', version = '. $this->submittedVersion .', date_update = NOW() WHERE id_tag = '.$this->objectExternalId.' AND id_lang = '.$this->language,
			false
		);
    }

    /**
     * @param array $tagData
     * @return bool
     */
    private function checkMinimumFormatTag($tagData)
    {
        foreach ($this->structure as $tagExternalField => $structureData) {
            try {
                if (!array_key_exists($tagExternalField, $tagData) && $this->isRequired($tagExternalField)) {
                    $this->response = "EL TAG {$tagData['CategoryID']} NO TIENE EL CAMPO REQUERIDO $tagExternalField";
                    MMOLogger::getInstance()->error($this->response);

                    return false;
                }

                if (!$this->isNullable($tagExternalField) && $this->isRequired($tagExternalField) && !array_key_exists($tagExternalField, $tagData)) {
                    $this->response = "EL TAG {$tagData['CategoryID']} NO TIENE VALOR EN EL CAMPO SIN VALOR NULO $tagExternalField";
                    MMOLogger::getInstance()->error($this->response);

                    return false;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
            }
        }

        return true;
    }
}