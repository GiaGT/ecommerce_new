<?php

namespace MIP\PrestaShop;

/**
 * Class MMOPrestaShopWebserviceException
 *
 * @package MIP\PrestaShop
 * @author Eno <emullaraj.a4b@gmail.com>
 */
class MMOPrestaShopWebserviceException extends \Exception { }