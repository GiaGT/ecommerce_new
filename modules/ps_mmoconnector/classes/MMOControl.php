<?php

namespace MIP\PrestaShop;

use MIP\PrestaShop\Model\MMOAccountInfo;

class MMOControl
{
    const FACTOR_GB_TO_MB = 1024.0;
    const FACTOR_KB_TO_MB = 0.001;
    const FACTOR_B_TO_MB = 0.000001;
    const MIN_MEMORY_LIMIT = 512;
    const MIN_TIME_LIMIT = 15000;

    /**
     * Enables the module if it was disabled.
     */
    public function enableModule()
    {
        $module = \Module::getInstanceByName(\Ps_Mmoconnector::MODULE_NAME);

        try {
            if ($module->isEnabledForShopContext()) {
                return;
            }

            MMOLogger::getInstance()->notice('Module was enabled successfully.');
            $module->enable();
        } catch (\Exception $e) {
            MMOLogger::getInstance()->critical('Could not enable module. '.$e->getMessage());
        }
    }

    public function controlAllFilesProcessed()
    {
        $this->deleteAllFilesProcessed();
    }

    /**
     * @return string
     */
    public function getAccountInformation()
    {
        $response = $this->getAccountInfo();

        return json_decode(json_encode($response), true);
    }


    /**
     * @return MMOAccountInfo
     */
    public function getAccountInfo()
    {
        $moduleId = \Module::getModuleIdByName(\Ps_Mmoconnector::MODULE_NAME);

        $response = new MMOAccountInfo();
        $response->Installed = ($moduleId !== 0);
        $response->Version = \Ps_Mmoconnector::MODULE_VERSION;
        $response->LastCronExecutionDate = null;

        $roles = [5, 8, 173, 176, 229, 232];

        $sql = 'SELECT COUNT(a.id_profile) > 0
                FROM '._DB_PREFIX_.'access AS a
                WHERE a.id_profile = 5 AND id_authorization_role IN ('.implode(',', $roles).');';

        $result = \Db::getInstance()->getValue($sql, false);

        $response->ModuleInstallationPermissions = (boolean)$result;

        $response->FileFolderPermissions =
            (is_writable(_PS_MODULE_DIR_.'ps_mmoconnector/files') &&
                is_readable(_PS_MODULE_DIR_.'ps_mmoconnector/files'));

        $cronExecutionFilename = _PS_MODULE_DIR_.'ps_mmoconnector/cron_execution.log';

        if (file_exists($cronExecutionFilename)) {
            $timezone = new \DateTimeZone('UTC');
            $lastCronExecutionDate = \DateTime::createFromFormat(
                DATE_W3C,
                file_get_contents($cronExecutionFilename),
                $timezone
            );
            $response->LastCronExecutionDate = $lastCronExecutionDate->format(DATE_W3C);
        }

        return $response;
    }

    public function setInstallModulePermissions($actionType)
    {
        $roles = [5, 8, 173, 176, 229, 232];
        $user = 5;
        switch ($actionType) {
            case 'ALLOW':
                $values = [];
                foreach ($roles as $role) {
                    $values[] = '('.$user.','.$role.')';
                }
                $sql = 'INSERT IGNORE INTO '._DB_PREFIX_.'access VALUES '.implode(',', $values).';';
                \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);
                break;
            case 'DENY':
                $sql = 'DELETE FROM '._DB_PREFIX_.'access WHERE id_profile = '.$user.' AND id_authorization_role IN ('.implode(
                        ',',
                        $roles
                    ).');';
                \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);
                break;
            default :
                MMOLogger::getInstance()->critical('Permission Action not recognized.');

                return array(false);
        }

        return array(true);
    }

    public function controlFileExecutionTime()
    {
        $this->unlockFileProcesing();
    }

    public function limitStock()
    {
        $stockManager = new MMOStock();
        $stockManager->limitMaxStock();
    }

    public function resetExecutionLimits()
    {
        $maxExecutionTime = @ini_get('max_execution_time');
        if ((int)$maxExecutionTime > 0 && (int)$maxExecutionTime < self::MIN_TIME_LIMIT) {
            set_time_limit(self::MIN_TIME_LIMIT);
        }

        $memoryLimit = $this->convertShorthandBytesToMegabytes(@ini_get('memory_limit'));
        if ($memoryLimit > 0 && $memoryLimit < self::MIN_MEMORY_LIMIT) {
            ini_set('memory_limit', self::MIN_MEMORY_LIMIT.'M');
        }
    }

    private function unlockFileProcesing()
    {
        $result = MMOFile::unlockFileProcesing();

        if (!$result) {
            return;
        }

        foreach ($result as $row) {
            MMOFile::resetStateFile((int)$row['id_file']);
            /*MMOFile::sendEmailResetStateFile((int)$row['id_file']);*/
        }
    }

    private function deleteAllFilesProcessed()
    {
        $result = MMOFile::getAllFilesProcessed();

        if (!$result) {
            return;
        }

        foreach ($result as $row) {
            $nameFile = $row['name'];
            $urlFile = _PS_MODULE_DIR_.'ps_mmoconnector/files/'.$nameFile.'.json';

            try {
                if (file_exists($urlFile)) {
                    unlink($urlFile);
                }
            } catch (\Exception $e) {
                MMOLogger::getInstance()->error($e->getMessage(), $e->getTrace());
            }

            MMOFile::removeFileLog($nameFile);
            MMOImportProcess::removeImportProcessLog();
        }
    }

    private function convertShorthandBytesToMegabytes($shorthandValue)
    {
        $sanitizedShorthandValue = mb_strtolower(trim($shorthandValue));
        $value = (int)$sanitizedShorthandValue;

        if (false !== mb_strpos($sanitizedShorthandValue, 'm')) {
            return $value;
        }

        if (false !== mb_strpos($sanitizedShorthandValue, 'g')) {
            return (int)($value * self::FACTOR_GB_TO_MB);
        }

        if (false !== strpos($sanitizedShorthandValue, 'k')) {
            return (int)($value * self::FACTOR_KB_TO_MB);
        }

        return (int)($value * self::FACTOR_B_TO_MB);
    }
}