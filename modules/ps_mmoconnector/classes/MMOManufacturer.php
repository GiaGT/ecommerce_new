<?php

namespace MIP\PrestaShop;



class MMOManufacturer extends MMOObject
{
    public $response = false;
    public $update = false;
    public $log;
    public $structure = array(
        'BrandID' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => false,
            'field' => 'id',
            'value' => '',
        ),
        'BrandName' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'name',
            'value' => '-',
        ),
        'Version' => array(
            'mapped' => false,
            'required' => true,
            'nullable' => false,
            'field' => '',
            'value' => 0,
        ),
        'Active' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => false,
            'field' => 'active',
            'value' => 1,
        ),
        'date_add' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => true,
            'field' => 'date_add',
            'value' => '',
        ),
        'date_upd' => array(
            'mapped' => true,
            'required' => false,
            'nullable' => true,
            'field' => 'date_upd',
            'value' => '',
        ),
        'ImageURL' => array(
            'mapped' => true,
            'required' => true,
            'nullable' => false,
            'field' => 'images',
            'value' => '',
        ),
    );

    public function __construct()
    {
        parent::__construct();

        $this->objectType = 'manufacturers';
    }

    public function exists()
    {
        $this->not_process = false;
        $result = \Db::getInstance()->getRow(
            'SELECT id_manufacturer_shop, version FROM '._DB_PREFIX_.'mmo_connector_manufacturer_map WHERE id_manufacturer = '.$this->objectExternalId, false
        );

        if (empty($result)) {
            MMOLogger::getInstance()->debug("MANUFACTURER ID {$this->objectExternalId}: NOT FOUND");
            return false;
        }

        if ($result['version'] < $this->version) {
            MMOLogger::getInstance()->info("MANUFACTURER ID {$this->objectExternalId}: UPDATING TO VERSION {$this->version}");
            $this->objectId = $result['id_manufacturer_shop'];
            return true;
        }

        MMOLogger::getInstance()->debug("MANUFACTURER ID {$this->objectExternalId}: SKIPPING UPDATE WITH CURRENT VERSION {$result['version']}");
        $this->not_process = true;
        $this->objectId = $result['id_manufacturer_shop'];

        return true;
    }

    public function getIdManufacturerShop($idManufacturerShop)
    {
        $idManufacturer = \Db::getInstance()->getValue(
            'SELECT id_manufacturer_shop FROM '._DB_PREFIX_.'mmo_connector_manufacturer_map WHERE id_manufacturer = '.$idManufacturerShop, false
        );

        return $idManufacturer;
    }

    public function getXMLBlankObject()
    {
        $this->xmlObject = $this->webService->getXMLModel($this->objectType);
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function getXMLDefaultStructure()
    {
        foreach ($this->structure as $key => $value) {
            try {
	            if ($this->isMapped($key)) {
                    $field = $this->getField($this->structure, $key);
		            $valor = $this->getValue($this->structure, $key);
		            $this->xmlObject->manufacturer->{$field} = $valor;
	            }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }
    }

    public function setXMLObjectToWebService()
    {
        if (!$this->update) {
            $this->response = $this->webService->addXMLId($this->objectType, $this->xmlObject);
        } else {
            $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
        }
    }

    public function setXMLObjectData($data)
    {
        $this->checkMinimumFormatManufacturer($data);

        foreach ($data as $key => $value) {
            try {
                if ($key === 'Version') {
                    continue;
                }

                if ($this->isMapped($key)) {
                    $field = $this->getField($this->structure, $key);
                    $this->xmlObject->manufacturer->{$field} = $value;
                }

                unset($this->xmlObject->manufacturer->link_rewrite);
                unset($this->xmlObject->manufacturer->images);
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }

        if ($this->update) {
            $this->xmlObject->manufacturer->id = $this->objectId;
        } else {
            unset($this->xmlObject->manufacturer->id);
        }

        $this->setXMLObjectToWebService();
    }

    public function process($objectData)
    {
        $this->response = null;
        $this->objectId = null;
        $this->objectExternalId = $objectData['Brand']['BrandID'];
        $this->version = $objectData['Brand']['Version'];
        $manufacturerMappingExists = $this->exists();

        if ($manufacturerMappingExists) {
            $this->update = true;
            $this->getXMLObjectFromWebService();
        }

        if ($this->not_process && is_object($this->xmlObject)) {
            MMOLogger::getInstance()->debug("SKIP MANUFACTURER ID {$this->objectExternalId}");
            $obj = new \stdClass();

            return $obj;
        }

        $this->not_process = false;

        if ($this->update && !is_object($this->xmlObject)) {
            $this->update = false;
        }

        if (!$manufacturerMappingExists) {
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }
        else if ($manufacturerMappingExists && !is_object($this->xmlObject)) {
            MMOLogger::getInstance()->warning('Marca mapeada pero no existe en la tienda. Vamos a intentar crearla de todos modos. Webservice message: '.$this->xmlObject);
            $this->response = null;
            $this->objectId = null;
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }

        $this->setXMLObjectData($objectData['Brand']);
        $this->registerLog($this->response, $objectData['Brand']['BrandID'], 'MANUFACTURER', $manufacturerMappingExists);

        if (is_string($this->response)) {
            MMOLogger::getInstance()->error('Error del webservice al procesar el MANUFACTURER '.$this->objectExternalId);

            return $this->response;
        }

        MMOLogger::getInstance()->info('INSERCION MANUFACTURER: '.$this->objectId);

        if (!$manufacturerMappingExists && $this->update === false) {
            $this->createManufacturerMap();
        }
        else if ($manufacturerMappingExists && $this->update === false) {
            $this->remapManufacturerMap();
        }
        else {
            $this->updateManufacturerMap();
        }

        $this->update = false;

        return $this->response;
    }

    public function createManufacturerMap()
    {
        \Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'mmo_connector_manufacturer_map (id_manufacturer_shop ,id_manufacturer, version, date_add) VALUES ('.$this->response->manufacturer->id.','.$this->objectExternalId.",'". $this->version."',NOW())", false);
    }

    public function updateManufacturerMap()
    {
        \Db::getInstance()->execute('UPDATE '._DB_PREFIX_."mmo_connector_manufacturer_map SET version = '" . $this->version . "', date_update = NOW() WHERE id_manufacturer_shop = ".$this->objectId, false);
    }

    public function remapManufacturerMap()
    {
        \Db::getInstance()->execute('UPDATE '._DB_PREFIX_."mmo_connector_manufacturer_map SET version = '" . $this->version . "', id_manufacturer_shop = ".$this->response->manufacturer->id.', date_update = NOW() WHERE id_manufacturer = '.$this->objectExternalId, false);
    }

    private function checkMinimumFormatManufacturer($manufacturerData)
    {
        foreach ($this->structure as $manufacturerExternalField => $structureData) {
            try {
                if (!array_key_exists($manufacturerExternalField, $manufacturerData)
                    && $this->isRequired($manufacturerExternalField)) {
                    $this->response = "EL BRAND {$manufacturerData['BrandID']} NO TIENE EL CAMPO REQUERIDO $manufacturerExternalField";
                    MMOLogger::getInstance()->error($this->response);

                    return false;
                }

                if (!$this->isNullable($manufacturerExternalField)
                    && $this->isRequired($manufacturerExternalField)
                    && !array_key_exists($manufacturerExternalField, $manufacturerData)) {
                    $this->response = "EL BRAND {$manufacturerData['BrandID']} NO TIENE VALOR EN EL CAMPO SIN VALOR NULO $manufacturerExternalField";
                    MMOLogger::getInstance()->error($this->response);

                    return false;
                }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }

        return true;
    }
}
