<?php

namespace MIP\PrestaShop;

class MMOImage
{
    public $imageUrl = 'http://www.dropshippers.com.es/imgs/';
    public $urlWebService;
    public $keyWebService;
    public $imageTmpDir;
    public $imageTmpFilePath;
    public $error = false;

    const TYPE_CATEGORY = 'categories';
    const TYPE_MANUFACTURER = 'manufacturers';
    const TYPE_PRODUCT = 'products';

    public function __construct()
    {
        $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $this->urlWebService = \Context::getContext()->shop->getBaseURL($forceSsl, true);
        $this->urlWebService = rtrim($this->urlWebService, '/').'/api/images';

        $oldPermissions = umask(0000);
        $this->imageTmpDir = __DIR__.'/../tmp';

        if (!@mkdir($this->imageTmpDir) && !is_dir($this->imageTmpDir)) {
            throw new \Exception("No se pudo crear la carpeta temporal {$this->imageTmpDir}");
        }

        @chmod($this->imageTmpDir, 0775);

        $this->imageTmpFilePath = $this->imageTmpDir . '/file.jpeg';

        if (!file_exists($this->imageTmpFilePath)) {
            $oldPermissions = umask(0000);
            file_put_contents($this->imageTmpFilePath, '');
            chmod($this->imageTmpFilePath, 0775);
        }

        umask($oldPermissions);
    }

    private function updateImages($objectImage)
    {
        MMOLogger::getInstance()->info('DELETING IMAGES');
        $this->deleteImages($objectImage);
        MMOLogger::getInstance()->info('ADDING IMAGES');
        $this->addImages($objectImage);
    }

    private function deleteImages($objectArray)
    {
        $queryParameter = http_build_query(
            [
                'ws_key' => MMOCONECTOR_WEBSERVICE_KEY_ID,
                'ps_method' => 'DELETE',
            ]
        );

        foreach ($objectArray as $value) {
            $arrayIdImages = $value->idImage;

            foreach ($arrayIdImages as $key => $idImg) {
                $ch = curl_init();

                $urlWebService = $this->getUrlDeleteByObjectType($value, $idImg).'?'.$queryParameter;

                curl_setopt($ch, CURLOPT_URL, $urlWebService);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,30);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);

                curl_exec($ch);

                curl_close($ch);
            }
        }
    }

    private function addImages($objectArray)
    {
        $logger = MMOLogger::getInstance();
        $logger->info('INICIO INSERCIÓN IMÁGENES: ');
        $this->error = false;
        $queryParameter = http_build_query(['ws_key' => MMOCONECTOR_WEBSERVICE_KEY_ID,]);

        foreach ($objectArray as $object) {
            $urlWebService = $this->urlWebService.'/'.$object->type.'/'.$object->idOject.'?'.$queryParameter;

            $logger->debug("CREANDO IMAGEN DE {$object->type}: {$urlWebService}");
            $arrayImages = $object->image;

            foreach ($arrayImages as $key => $img) {
                $image[$key] = trim($img);
                $cacheFilename = \Ps_Mmoconnector::MODULE_DIR.'/cache/imgs/'.str_replace('http://dropshipping.bigbuy.eu/imgs/', '', $image[$key]);
                $logger->info('IMAGEN - INICIO LOCAL CACHE: '.$cacheFilename);

                if (file_exists($cacheFilename)) {
                    $image[$key] = $cacheFilename;
                    $logger->info('IMAGEN - HIT LOCAL CACHE: '.$cacheFilename);
                }
                else {
                    $image[$key] = str_replace(' ', '%20', $image[$key]);
                    $logger->info('IMAGEN - MISS LOCAL CACHE: '.$cacheFilename);
                }

                $logger->info('IMAGEN - TIPO: '.$object->type.' - ID: '. $object->idOject . ' - URL: ' .$image[$key]);

                try {
                    $logger->info('IMAGEN - INICIO LOCAL - TIPO: '.$object->type.' - ID: '. $object->idOject . ' - URL: ' .$image[$key]);
                    $imageTmpFilePath = $this->imageTmpDir.'/'.pathinfo($image[$key], PATHINFO_BASENAME);
                    copy($image[$key], $imageTmpFilePath);
                    $logger->info('IMAGEN - FIN LOCAL - TIPO: '.$object->type.' - ID: '. $object->idOject . ' - URL: ' .$image[$key]);

                    $logger->info('IMAGEN - INICIO SERVER - TIPO: '.$object->type.' - ID: '. $object->idOject . ' - URL: ' .$image[$key]);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $urlWebService);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:multipart/form-data'));
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,30);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, array('image' => new \CURLFile($imageTmpFilePath)));
                    curl_exec($ch);

                    $curlInfo = curl_getinfo($ch);
                    $curlError = curl_error($ch);
                    $curlStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);

                    if ($curlStatusCode !== 0) {
                        MMOLogger::getInstance()->warning(__METHOD__.": $curlStatusCode - ".$curlError);
                    }

                    if ($curlStatusCode === 401) {
                        throw new MMOPrestaShopWebserviceException('IMG Unauthorized', $curlStatusCode);
                    }

                    $logger->info('IMAGEN - FIN SERVER - TIPO: '.$object->type.' - ID: '. $object->idOject . ' - URL: ' .$image[$key], [$curlError, $curlInfo]);

                    /**
                     * Se activa el borrado de la imagen para evitar las imagenes repetidas en varios productos cuando
                     * la imagen no
                     */
                    try {
                        if (is_file($imageTmpFilePath)) {
                            unlink($imageTmpFilePath);
                        }
                    }
                    catch (\Exception $unlinkException) {
                        $logger->error('Error unlink image '.$unlinkException->getMessage());
                    }
                } catch (\Exception $e) {
                    $logger->error('NO SE PUEDE DESCARGAR LA IMAGEN DESDE LA URL: '.$image[$key] . ' MSG ERROR: ' .$e->getMessage());
                    $this->error = '400 Imagen no encontrada';

                    return;
                }
            }


            if (!MMOConnector::hasMogrify()) {
                continue;
            }

            $imagePaths = [];

            switch ($object->type) {
                case self::TYPE_PRODUCT:
                    $productManager = new MMOProduct();
                    $productImageIds = $productManager->getImages($object->idOject);

                    foreach ($productImageIds as $productImageId) {
                        $imageObject = new \Image((int)$productImageId);
                        $imagePaths[] = $imageObject->getPathForCreation();
                    }

                    break;
                case self::TYPE_MANUFACTURER:
                    $imagePaths[] = _PS_ROOT_DIR_.'/img/m/'.$object->idOject;
                    break;
                case self::TYPE_CATEGORY:
                    $imagePaths[] = _PS_ROOT_DIR_.'/img/c/'.$object->idOject;
                    break;
                default:
            }

            $imagesTypes = \ImageType::getImagesTypes($object->type);

            foreach ($imagePaths as $imagePath) {
                foreach ($imagesTypes as $imagesType) {
                    $imageFile = $imagePath.'-'.stripslashes($imagesType['name']).'.jpg';
                    exec('mogrify -quality 80% '.$imageFile);
                }
            }
        }

        $logger->info('FIN INSERCIÓN IMÁGENES: ');
    }

    private function getUrlDeleteByObjectType($object, $idImg = null)
    {
        $urlWebService = '';

        switch ($object->type){
            case self::TYPE_PRODUCT:
                $urlWebService = $this->urlWebService.'/'.$object->type.'/'.$object->idOject.'/'.$idImg;
                break;
            case self::TYPE_CATEGORY:
                $urlWebService = $this->urlWebService.'/'.$object->type.'/'.$object->idOject;
                break;
            case self::TYPE_MANUFACTURER:
                $urlWebService = $this->urlWebService.'/'.$object->type.'/'.$object->idOject;
                break;
        }

        return $urlWebService;
    }

    public function process($id, $type, $data)
    {
        $this->keyWebService = MMOCONECTOR_WEBSERVICE_KEY_ID;
        $objectArray = array();

        $object = $this->formatImageObject($id, $type, $data);
        $objectArray[] = $object;

        $cacheId = $object->type . '-' .$object->idOject;

        if(MMOCache::isValid($cacheId)) {
            $this->error = "CACHE HIT MMOImage process $cacheId.";

            return $this->error;
        }

        if (MMOCache::isStored($cacheId)) {
            $this->error = 'Image error in previously processed product.';

            return $this->error;
        }

        $this->updateImages($objectArray);
        MMOLogger::getInstance()->info("FINISHED UPDATING IMAGES $type ID $id");
        MMOCache::store($cacheId, empty($this->error));

        return $this->error;
    }

    private function formatImageObject($id, $type, $data)
    {
        $image = new \stdClass;

        switch ($type){
            case self::TYPE_MANUFACTURER:
                $image->idOject = $id;
                $image->idImage = [$id];
                $image->type = $type;
                $image->image = [$data['Brand']['ImageURL']];
                break;
            case self::TYPE_CATEGORY:
                $image->idOject = $id;
                $image->idImage = [$id];
                $image->type = $type;
                $image->image = [$data['ImageURL']];
                break;
            case self::TYPE_PRODUCT:

                $objectProduct = new MMOProduct();
                $oldImages = array();
                $newImages = array();

                MMOLogger::getInstance()->debug('Recuperando las imagenes del WSformatImageObject.');
                $images = $objectProduct->getImages($id);

                if (!empty($images)) {
                    foreach ($images as $img) {
                        $imge = (int)$img;
                        $oldImages[] = $imge;
                    }
                }

                foreach ($data['Images'] as $key => $value) {
                    if ($value['Cover']) {
                        array_unshift($newImages, $value['ImageURL']);
                        continue;
                    }

                    $newImages[] = $value['ImageURL'];
                }

                $image->type = self::TYPE_PRODUCT;
                $image->idOject = $id;
                $image->idImage = $oldImages;
                $image->image = $newImages;

                \Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'mmo_connector_product_map SET version_img = ' . $data['ImagesVersion'] . ' WHERE id_product_shop = ' . $id);
                break;
            default:
        }

        return $image;
    }
}