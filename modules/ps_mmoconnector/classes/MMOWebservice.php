<?php

namespace MIP\PrestaShop;

class MMOWebservice
{
    const FOLDER_IMAGES = '/tmp';
    const URL_IMAGES = 'http://www.dropshippers.com.es/imgs/';

    public $shop_path;
    public $auth_key;
    private $webService;
    private static $instance;

    private function __construct()
    {
        $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $this->shop_path = \Context::getContext()->shop->getBaseURL($forceSsl, true);
        $this->shop_path = rtrim($this->shop_path, '/');
        $this->webService = new MMOPrestaShopWebservice($this->shop_path, MMOCONECTOR_WEBSERVICE_KEY_ID, false);
    }

    /**
     * @return MMOWebservice
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
            //self::$instance = MMOWebserviceAdapter::getInstance();
        }

        return self::$instance;
    }

    public function getXMLModel($type)
    {
        MMOLogger::getInstance()->debug("MMOWS->getXMLModel $type");

        $url = $this->shop_path.'/api/'.$type.'?schema=synopsis';
        $cacheId = "MMOWS-$url";

        if (MMOCache::isValid($cacheId)) {
            MMOLogger::getInstance()->info("CACHE HIT getXMLModel $url");
            return MMOCache::get($cacheId);
        }

        try {
            $xml =  $this->webService->get(array('url' => $url));
            MMOCache::store($cacheId, $xml);
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

            if ($ex->getCode() === 404 || $ex->getCode() === 500) {
                $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
                $webservice_key = new \WebserviceKey($key_id);

                MMOLogger::getInstance()->notice('getXMLModel: '.$ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type]);
                return $ex->getCode().'|'.$ex->getMessage();
            }

            MMOLogger::getInstance()->critical(__FUNCTION__.' '.$ex->getCode().' '.$ex->getMessage(), [$this->shop_path, $type]);
            throw $ex;
        }

        return $xml;
    }

    public function getXMLId($type, $id)
    {
        MMOLogger::getInstance()->debug("MMOWS->getXMLId $type $id");

        $opt = array(
            'resource' => $type,
            'id' => $id,
        );

        try {
            $xml = $this->webService->get($opt);

            return $xml;
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

            if ($ex->getCode() === 404) {
                $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
                $webservice_key = new \WebserviceKey($key_id);

                MMOLogger::getInstance()->critical('getXMLId: '.$ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type, $id, $opt]);
                return $ex->getCode().'|'.$ex->getMessage();
            }

            if ($type === 'images/products' && $ex->getCode() === 500) {
                $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
                $webservice_key = new \WebserviceKey($key_id);

                MMOLogger::getInstance()->critical('getXMLId: '.$ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $type, $id, $opt]);
                return $ex->getCode().'|'.$ex->getMessage();
            }

            MMOLogger::getInstance()->critical(__FUNCTION__.' '.$ex->getCode().' '.$ex->getMessage(), [$this->shop_path, $type, $id, $opt]);
            throw $ex;
        }
    }

    public function setXMLId($type, $id, $xmlObject)
    {
        MMOLogger::getInstance()->debug("MMOWS->setXMLId $type $id");

        try {
            $opt = array(
                'resource' => $type,
                'putXml' => $xmlObject->asXML(),
                'id' => $id,
            );

            $xml = $this->webService->edit($opt);

            return $xml;
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

            $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
            $webservice_key = new \WebserviceKey($key_id);

            MMOLogger::getInstance()->critical($ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $opt, $type, $id]);
            MMOLogger::getInstance()->debug($ex->getMessage().' | W'.$webservice_key->key.'=', [$xmlObject]);

            return $ex->getCode().'|'.$ex->getMessage();
        }
    }

    public function addXMLId($type, $xmlObject)
    {
        MMOLogger::getInstance()->debug("MMOWS->addXMLId $type");

        try {
            $opt = array(
                'resource' => $type,
                'postXml' => $xmlObject->asXML(),
            );

            $xml = $this->webService->add($opt);

            return $xml;
        } catch (MMOPrestaShopWebserviceException $ex) {
            if ($ex->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $ex;
            }

            $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
            $webservice_key = new \WebserviceKey($key_id);

            MMOLogger::getInstance()->critical($ex->getMessage().' | W'.$webservice_key->key.'=', [$this->shop_path, $opt, $type]);
            MMOLogger::getInstance()->debug($ex->getMessage().' | W'.$webservice_key->key.'=', [$xmlObject]);

            return $ex->getCode().'|'.$ex->getMessage();
        }
    }

    /**
     * @see http://doc.prestashop.com/display/PS16/Chapter+8+-+Advanced+use
     *
     * $options = array(
     *      'resource'   => 'customers',
     *      'display'    => '[firstname,lastname]',
     *      'filter[firstname]' => '[John]',
     *      'filter[lastname]'  => '[DOE]',
     *      'sort'     => '[lastname_ASC]',
     *      'limit'    => '5',
     * );
     *
     * URL: (Store URL)/api/customers/?display=[lastname]&filter[id]=[1,10]
     *
     * @param $options
     * @return \SimpleXMLElement|null
     */
    public function getXMLList($options)
    {
        MMOLogger::getInstance()->debug('MMOWS->getXMLList', $options);

        try {
            $xml = $this->webService->get($options);

            return $xml;
        } catch (\Exception $e) {
            if ($e->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $e;
            }

            MMOLogger::getInstance()->critical('getXMLList: ' . $e->getMessage(), [$this->shop_path, $options]);
        }

        return null;
    }

    /**
     * @see http://doc.prestashop.com/display/PS16/Chapter+8+-+Advanced+use
     *
     * $options = array(
     *      'resource'   => 'customers',
     *      'display'    => '[firstname,lastname]',
     *      'filter[firstname]' => '[John]',
     *      'filter[lastname]'  => '[DOE]',
     *      'sort'     => '[lastname_ASC]',
     *      'limit'    => '5',
     * );
     *
     * URL: (Store URL)/api/products/?filter[id]=[1,10]
     *
     * @param array $options
     * @throws \Exception
     */
    public function delete(array $options)
    {
        MMOLogger::getInstance()->debug('MMOWS->delete', $options);

        try {
            $this->webService->delete($options);
        } catch (\Exception $e) {
            if ($e->getCode() === 401) {
                MMOLogger::getInstance()->critical('WS UNAUTHORIZED '.__FUNCTION__);
                throw $e;
            }

            MMOLogger::getInstance()->critical('delete: ' . $e->getMessage(), [$this->shop_path, $options]);
        }
    }

    /**
     * @param string $response
     * @return \SimpleXMLElement
     * @throws MMOPrestaShopWebserviceException
     */
    public function parseXML($response)
    {
        if (empty($response)) {
            throw new MMOPrestaShopWebserviceException('HTTP response is empty');
        }

        libxml_clear_errors();
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($response,'SimpleXMLElement', LIBXML_NOCDATA);

        if (libxml_get_errors()) {
            $msg = var_export(libxml_get_errors(), true);
            libxml_clear_errors();
            throw new MMOPrestaShopWebserviceException('HTTP XML response is not parsable: '.$msg);
        }

        return $xml;
    }
}
