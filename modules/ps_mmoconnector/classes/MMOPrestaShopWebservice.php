<?php

namespace MIP\PrestaShop;

class MMOPrestaShopWebservice
{
    /** @var string Shop URL */
    protected $url;

    /** @var string Authentification key */
    protected $key;

    /** @var boolean is debug activated */
    protected $debug;

    /** @var string PS version */
    protected $version;

    /** @var array compatible versions of PrestaShop Webservice */
    const psCompatibleVersionsMin = '1.4.0.0';
    const psCompatibleVersionsMax = '1.7.99.99';

    /**
     * PrestaShopWebservice constructor. Throw an exception when CURL is not installed/activated
     * <code>
     * <?php
     * require_once('./PrestaShopWebservice.php');
     * try
     * {
     * 	$ws = new PrestaShopWebservice('http://mystore.com/', 'ZQ88PRJX5VWQHCWE4EE7SQ7HPNX00RAJ', false);
     * 	// Now we have a webservice object to play with
     * }
     * catch (PrestaShopWebserviceException $ex)
     * {
     * 	echo 'Error : '.$ex->getMessage();
     * }
     * ?>
     * </code>
     * @param string $url Root URL for the shop
     * @param string $key Authentification key
     * @param mixed $debug Debug mode Activated (true) or deactivated (false)
     * @throws MMOPrestaShopWebserviceException
     */
    public function __construct($url, $key, $debug = true)
    {
        if (!extension_loaded('curl')) {
            MMOLogger::getInstance()->debug('Please activate the PHP extension \'curl\' to allow use of PrestaShop webservice library');
            throw new MMOPrestaShopWebserviceException('Please activate the PHP extension \'curl\' to allow use of PrestaShop webservice library');
        }

        $this->url = $url;
        $this->key = $key;
        $this->debug = $debug;
        $this->version = 'unknown';
    }

    /**
     * Take the status code and throw an exception if the server didn't return 200 or 201 code
     * @param int $status_code Status code of an HTTP return
     * @throws MMOPrestaShopWebserviceException
     */
    protected function checkStatusCode($status_code)
    {
        $error_label = 'This call to PrestaShop Web Services failed and returned an HTTP status of %d. That means: %s.';

        switch($status_code) {
            case 200:	
            case 201:
                break;
            case 204: throw new MMOPrestaShopWebserviceException(sprintf($error_label, $status_code, 'No content'), $status_code);break;
            case 400: throw new MMOPrestaShopWebserviceException(sprintf($error_label, $status_code, 'Bad Request'), $status_code);break;
            case 401: throw new MMOPrestaShopWebserviceException(sprintf($error_label, $status_code, 'Unauthorized'), $status_code);break;
            case 404: throw new MMOPrestaShopWebserviceException(sprintf($error_label, $status_code, 'Not Found'), $status_code);break;
            case 405: throw new MMOPrestaShopWebserviceException(sprintf($error_label, $status_code, 'Method Not Allowed'), $status_code);break;
            case 500: throw new MMOPrestaShopWebserviceException(sprintf($error_label, $status_code, 'Internal Server Error'), $status_code);break;
            default: throw new MMOPrestaShopWebserviceException('This call to PrestaShop Web Services returned an unexpected HTTP status of:' . $status_code, $status_code);
        }
    }
    /**
     * Handles a CURL request to PrestaShop Webservice. Can throw exception.
     * @param string $url Resource name
     * @param mixed $curl_params CURL parameters (sent to curl_set_opt)
     * @return array status_code, response
     * @throws MMOPrestaShopWebserviceException
     */
    protected function executeRequest($url, $curl_params = array())
    {
        $defaultParams = array(
            CURLOPT_HEADER => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLINFO_HEADER_OUT => TRUE,
            CURLOPT_HTTPHEADER => array( 'Expect:' ),
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_TIMEOUT => 60,
        );

        $urlData = parse_url($url);
        $parsedUrl = $urlData['scheme'].'://'.$urlData['host'].$urlData['path'].'?'.(!empty($urlData['query']) ? $urlData['query'].'&': '')."ws_key={$this->key}";
        $session = curl_init($parsedUrl);
        $curl_options = array();

        foreach ($defaultParams as $defkey => $defval) {
            if (isset($curl_params[$defkey])) {
                $curl_options[$defkey] = $curl_params[$defkey];
            }
            else {
                $curl_options[$defkey] = $defaultParams[$defkey];
            }
        }

        foreach ($curl_params as $defkey => $defval) {
            if (isset($curl_options[$defkey])) {
                continue;
            }

            $curl_options[$defkey] = $curl_params[$defkey];
        }

        curl_setopt_array($session, $curl_options);
        $response = curl_exec($session);
        $curlError = curl_error($session);
        $curlInfo = curl_getinfo($session);
        $curlHeaderOut = curl_getinfo($session, CURLINFO_HEADER_OUT);
        $curlHttpCode = curl_getinfo($session, CURLINFO_HTTP_CODE);
        curl_close($session);

        $index = strpos($response, "\r\n\r\n");

        if ($index === false && $curl_params[CURLOPT_CUSTOMREQUEST] != 'HEAD') {
            MMOLogger::getInstance()->critical(
                'Bad HTTP response '.$curlError,
                [
                    ['parsedUrl' => $parsedUrl],
                    'response' => $response, 'info' => $curlInfo
                ]
            );
            throw new MMOPrestaShopWebserviceException('Bad HTTP response');
        }

        $header = substr($response, 0, $index);
        $body = substr($response, $index + 4);
        $headerArrayTmp = explode("\n", $header);
        $headerArray = array();

        foreach ($headerArrayTmp as $headerItem) {
            $tmp = explode(':', $headerItem);
            $tmp = array_map('trim', $tmp);

            if (count($tmp) === 2) {
                $headerArray[$tmp[0]] = $tmp[1];
            }
        }

        if (array_key_exists('PSWS-Version', $headerArray)) {
            $this->version = $headerArray['PSWS-Version'];
            
            if (
                version_compare(self::psCompatibleVersionsMin, $headerArray['PSWS-Version']) == 1 ||
                version_compare(self::psCompatibleVersionsMax, $headerArray['PSWS-Version']) == -1
            ) {
                MMOLogger::getInstance()->critical('WS CURL Error: '.$curlError, ['parsedUrl' => $parsedUrl]);
                throw new MMOPrestaShopWebserviceException('This library is not compatible with this version of PrestaShop. Please upgrade/downgrade this library');
            }
        }

        if ($this->debug) {
            $this->printDebug('HTTP REQUEST HEADER', $curlHeaderOut);
            $this->printDebug('HTTP RESPONSE HEADER', $header);
        }

        if ($curlHttpCode === 0) {
            MMOLogger::getInstance()->critical('WS CURL Error: '.$curlError, ['parsedUrl' => $parsedUrl]);
            throw new MMOPrestaShopWebserviceException('CURL Error: '.$curlError);
        }

        if ($curlHttpCode > 299) {
            MMOLogger::getInstance()->critical('WS CURL response code error: '.$curlError, ['parsedUrl' => $parsedUrl]);
        }

        if ($this->debug) {
            if ($curl_params[CURLOPT_CUSTOMREQUEST] == 'PUT' || $curl_params[CURLOPT_CUSTOMREQUEST] == 'POST')
                $this->printDebug('XML SENT', urldecode($curl_params[CURLOPT_POSTFIELDS]));
            if ($curl_params[CURLOPT_CUSTOMREQUEST] != 'DELETE' && $curl_params[CURLOPT_CUSTOMREQUEST] != 'HEAD')
                $this->printDebug('RETURN HTTP BODY', $body);
        }

        return array('status_code' => (int)$curlHttpCode, 'response' => $body, 'header' => $header);
    }

    public function printDebug($title, $content)
    {
        echo '<div style="display:table;background:#CCC;font-size:8pt;padding:7px"><h6 style="font-size:9pt;margin:0">'.$title.'</h6><pre>'.htmlentities($content).'</pre></div>';
    }

    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Load XML from string. Can throw exception
     * @param string $response String from a CURL response
     * @return \SimpleXMLElement status_code, response
     * @throws MMOPrestaShopWebserviceException
     */
    protected function parseXML($response)
    {
        $response = trim($response, " \t\n\r\0\x0B");

        if ($response == '') {
            MMOLogger::getInstance()->critical('WS parseXML HTTP response is empty.', [$this->url, $response]);

            throw new MMOPrestaShopWebserviceException('HTTP response is empty', 400);
        }

        libxml_clear_errors();
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($response,'SimpleXMLElement', LIBXML_NOCDATA);

        if (libxml_get_errors()) {
            $msg = var_export(libxml_get_errors(), true);
            libxml_clear_errors();
            MMOLogger::getInstance()->critical('Parse XML ERROR', [$this->url, $response]);

            throw new MMOPrestaShopWebserviceException('HTTP XML response is not parsable: '.$msg, 400);
        }

        return $xml;
    }

    /**
     * Add (POST) a resource
     * <p>Unique parameter must take : <br><br>
     * 'resource' => Resource name<br>
     * 'postXml' => Full XML string to add resource<br><br>
     * Examples are given in the tutorial</p>
     * @param array $options
     * @return \SimpleXMLElement status_code, response
     * @throws MMOPrestaShopWebserviceException
     */
    public function add($options)
    {
        if (isset($options['resource'], $options['postXml']) || isset($options['url'], $options['postXml'])) {
            $url = (isset($options['resource']) ? $this->url.'/api/'.$options['resource'] : $options['url']);

            $xml = $options['postXml'];
            if (isset($options['id_shop'])) {
                $url .= '&id_shop='.$options['id_shop'];
            }

            if (isset($options['id_group_shop'])) {
                $url .= '&id_group_shop='.$options['id_group_shop'];
            }
        }
        else {
            MMOLogger::getInstance()->critical('WS ADD ERROR Bad parameters.', [$this->url, $options]);
            throw new MMOPrestaShopWebserviceException('Bad parameters given');
        }

        MMOLogger::getInstance()->debug('Sending WS ADD request.', [$this->url, $options, $url]);
        $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'POST', CURLOPT_POSTFIELDS => $xml));

        if ((int)$request['status_code'] > 299) {
            MMOLogger::getInstance()->critical("WS ADD ERROR {$request['status_code']}.", [$this->url, $url, $options, $request]);
        }

        $this->checkStatusCode($request['status_code']);

        return $this->parseXML($request['response']);
    }

    /**
     * Retrieve (GET) a resource
     * <p>Unique parameter must take : <br><br>
     * 'url' => Full URL for a GET request of Webservice (ex: http://mystore.com/api/customers/1/)<br>
     * OR<br>
     * 'resource' => Resource name,<br>
     * 'id' => ID of a resource you want to get<br><br>
     * </p>
     * <code>
     * <?php
     * require_once('./PrestaShopWebservice.php');
     * try
     * {
     * $ws = new PrestaShopWebservice('http://mystore.com/', 'ZQ88PRJX5VWQHCWE4EE7SQ7HPNX00RAJ', false);
     * $xml = $ws->get(array('resource' => 'orders', 'id' => 1));
     *	// Here in $xml, a SimpleXMLElement object you can parse
     * foreach ($xml->children()->children() as $attName => $attValue)
     * 	echo $attName.' = '.$attValue.'<br />';
     * }
     * catch (PrestaShopWebserviceException $ex)
     * {
     * 	echo 'Error : '.$ex->getMessage();
     * }
     * ?>
     * </code>
     * @param array $options Array representing resource to get.
     * @return \SimpleXMLElement status_code, response
     * @throws MMOPrestaShopWebserviceException
     */
    public function get($options)
    {
        if (isset($options['url'])) {
            $url = $options['url'];
        }
        elseif (isset($options['resource'])) {
            $url = $this->url.'/api/'.$options['resource'];
            $url_params = array();
            if (isset($options['id']))
                $url .= '/'.$options['id'];

            $params = array('filter', 'display', 'sort', 'limit', 'id_shop', 'id_group_shop');
            foreach ($params as $p)
                foreach ($options as $k => $o)
                    if (strpos($k, $p) !== false)
                        $url_params[$k] = $options[$k];
            if (count($url_params) > 0)
                $url .= '?'.http_build_query($url_params);
        }
        else {
            MMOLogger::getInstance()->critical("WS GET ERROR Bad parameters.", [$this->url, $options]);
            throw new MMOPrestaShopWebserviceException('Bad parameters given');
        }

        MMOLogger::getInstance()->debug("Sending WS GET request.", [$this->url, $options, $url]);
        $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'GET'));

        if ((int)$request['status_code'] > 299
            && !(isset($options['resource']) && $options['resource'] === 'images/products' && (int)$request['status_code'] === 500)) {
            MMOLogger::getInstance()->critical("WS GET ERROR {$request['status_code']}.", [$this->url, $url, $options, $request]);
        }

        $this->checkStatusCode($request['status_code']);// check the response validity

        return $this->parseXML($request['response']);
    }

    /**
     * Head method (HEAD) a resource
     *
     * @param array $options Array representing resource for head request.
     * @return \SimpleXMLElement status_code, response
     * @throws MMOPrestaShopWebserviceException
     */
    public function head($options)
    {
        if (isset($options['url'])) {
            $url = $options['url'];
        }
        elseif (isset($options['resource'])) {
            $url = $this->url.'/api/'.$options['resource'];
            $url_params = array();
            if (isset($options['id']))
                $url .= '/'.$options['id'];

            $params = array('filter', 'display', 'sort', 'limit');
            foreach ($params as $p)
                foreach ($options as $k => $o)
                    if (strpos($k, $p) !== false)
                        $url_params[$k] = $options[$k];
            if (count($url_params) > 0)
                $url .= '?'.http_build_query($url_params);
        }
        else {
            MMOLogger::getInstance()->critical("WS HEAD ERROR Bad parameters.", [$this->url, $options]);
            throw new MMOPrestaShopWebserviceException('Bad parameters given');
        }

        MMOLogger::getInstance()->debug("Sending WS HEAD request.", [$this->url, $options, $url]);
        $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'HEAD', CURLOPT_NOBODY => true));

        if ((int)$request['status_code'] > 299) {
            MMOLogger::getInstance()->critical("WS HEAD ERROR {$request['status_code']}.", [$this->url, $url, $options, $request]);
        }

        $this->checkStatusCode($request['status_code']);// check the response validity

        return $request['header'];
    }
    /**
     * Edit (PUT) a resource
     * <p>Unique parameter must take : <br><br>
     * 'resource' => Resource name ,<br>
     * 'id' => ID of a resource you want to edit,<br>
     * 'putXml' => Modified XML string of a resource<br><br>
     * Examples are given in the tutorial</p>
     * @param array $options Array representing resource to edit.
     * @return \SimpleXMLElement
     * @throws MMOPrestaShopWebserviceException
     */
    public function edit($options)
    {
        $xml = '';
        if (isset($options['url'])) {
            $url = $options['url'];
        }
        elseif ((isset($options['resource'], $options['id']) || isset($options['url'])) && $options['putXml']) {
            $url = (isset($options['url']) ? $options['url'] : $this->url.'/api/'.$options['resource'].'/'.$options['id']);
            $xml = $options['putXml'];
            if (isset($options['id_shop']))
                $url .= '&id_shop='.$options['id_shop'];
            if (isset($options['id_group_shop']))
                $url .= '&id_group_shop='.$options['id_group_shop'];
        }
        else {
            MMOLogger::getInstance()->critical('WS EDIT ERROR Bad parameters.', [$this->url, $options]);
            throw new MMOPrestaShopWebserviceException('Bad parameters given');
        }

        MMOLogger::getInstance()->debug('Sending WS EDIT request.', [$this->url, $options, $url]);
        $request = $this->executeRequest($url,  array(CURLOPT_CUSTOMREQUEST => 'PUT', CURLOPT_POSTFIELDS => $xml));

        if ((int)$request['status_code'] > 299) {
            MMOLogger::getInstance()->critical("WS EDIT ERROR {$request['status_code']}.", [$this->url, $url, $options, $request]);
        }

        $this->checkStatusCode($request['status_code']);// check the response validity

        return $this->parseXML($request['response']);
    }

    /**
     * Delete (DELETE) a resource.
     * Unique parameter must take : <br><br>
     * 'resource' => Resource name<br>
     * 'id' => ID or array which contains IDs of a resource(s) you want to delete<br><br>
     * <code>
     * <?php
     * require_once('./PrestaShopWebservice.php');
     * try
     * {
     * $ws = new PrestaShopWebservice('http://mystore.com/', 'ZQ88PRJX5VWQHCWE4EE7SQ7HPNX00RAJ', false);
     * $xml = $ws->delete(array('resource' => 'orders', 'id' => 1));
     *	// Following code will not be executed if an exception is thrown.
     * 	echo 'Successfully deleted.';
     * }
     * catch (PrestaShopWebserviceException $ex)
     * {
     * 	echo 'Error : '.$ex->getMessage();
     * }
     * ?>
     * </code>
     * @param array $options Array representing resource to delete.
     * @return bool
     * @throws MMOPrestaShopWebserviceException
     */
    public function delete($options)
    {
        if (isset($options['url']))
            $url = $options['url'];
        elseif (isset($options['resource']) && isset($options['id']))
            if (is_array($options['id']))
                $url = $this->url.'/api/'.$options['resource'].'/?id=['.implode(',', $options['id']).']';
            else
                $url = $this->url.'/api/'.$options['resource'].'/'.$options['id'];
        if (isset($options['id_shop']))
            $url .= '&id_shop='.$options['id_shop'];
        if (isset($options['id_group_shop']))
            $url .= '&id_group_shop='.$options['id_group_shop'];


        MMOLogger::getInstance()->debug('Sending WS DELETE request.', [$this->url, $options, $url]);
        $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'DELETE'));

        if ((int)$request['status_code'] > 299) {
            MMOLogger::getInstance()->critical("WS DELETE ERROR {$request['status_code']}.", [$this->url, $url, $options, $request]);
        }

        $this->checkStatusCode($request['status_code']);// check the response validity

        return true;
    }
}
