<?php

namespace MIP\PrestaShop;

class MMOImportProcess
{
    /**
     * @param $productIds
     * @param $idFile
     * @return array
     */
    public static function getProcessedProducts($productIds, $idFile)
    {
        if (empty($productIds)) {
            return [];
        }

        $sql = "SELECT id_product FROM `"._DB_PREFIX_."mmo_connector_import_process` WHERE id_product IN (".implode(",", $productIds).") AND id_file = ".$idFile." AND response_web_service = 1";
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return array_column($result, 'id_product');
    }

    /**
     * @param $idProduct
     * @param $idFile
     * @return array|false|\mysqli_result|null|\PDOStatement|resource
     */
    public static function isProcessed($idProduct, $idFile)
    {
        $sql = "SELECT id_product FROM `"._DB_PREFIX_."mmo_connector_import_process` WHERE id_product = ".$idProduct." AND id_file = ".$idFile." AND response_web_service = 1";
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }

    /**
     * Función para añadir o actualizar la informacion del estado de importación
     * de un prodcuto
     * @param $product
     * @return array
     */
    public function addNewMMOImportProcess($product)
    {
        if ($this->existProductMMOImportProcess($product->id_product, $product->id_file)) {
            return $this->updateMMOImportProcess($product);
        }

        return $this->insertMMOImportProcess($product);
    }

    /**
     * Comprobación de la existencia de un producto en la tabla
     * @param $idProduct
     * @param $idFile
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    private function existProductMMOImportProcess($idProduct, $idFile)
    {
        $sql = "SELECT id_product FROM `"._DB_PREFIX_."mmo_connector_import_process` WHERE id_product = ".$idProduct." AND id_file = ".$idFile;
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }

    /**
     * Función para insertar el resultado del procesamiento de un nuevo producto
     * @param type $product
     * @return $result
     */
    private function insertMMOImportProcess($product)
    {
        $sql = "INSERT INTO `"._DB_PREFIX_."mmo_connector_import_process` "
            ."(`id_product`, `sku`, `id_file`, `response_web_service`, "
            ."`message_response_web_service`, `date_add`)"
            ."VALUES (".$product->id_product.", '"
            .$product->sku."', "
            .$product->id_file.", '"
            .$product->response_web_service."', '"
            .$product->message_response_web_service."', 
            NOW())";

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    /**
     * Función para actualizar la información del procesamiento de un producto
     * ya existente en la tabla ps_mmo_connector_import_process
     * @param type $product
     * @return type
     */
    private function updateMMOImportProcess($product)
    {
        $sql = "UPDATE `"._DB_PREFIX_."mmo_connector_import_process` SET "
            ."response_web_service = '".$product->response_web_service."', "
            ."message_response_web_service = '".$product->message_response_web_service."', "
            ."date_update = NOW() "
            ."WHERE id_product = ".$product->id_product." AND id_file = ".$product->id_file;

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);

        return $result;
    }

    /**
     * Función para obtener la informacion del procesamiento de todos los productos realizacionados con un fichero
     * @param $submissionId
     * @return mixed
     */
    public static function infoProcessMMOImportProcess($submissionId)
    {
        $stateProcessFile = self::getProcessState($submissionId);
        $arrayInfoProducts = self::getInfoProcessMMOImportProcess($submissionId);
        $submissionResult = self::generateFormatMMOImportProcess($submissionId, $stateProcessFile, $arrayInfoProducts);

        return $submissionResult;
    }

    public static function removeImportProcessLog()
    {
        $sql = 'DELETE FROM '._DB_PREFIX_.'mmo_connector_import_process WHERE date_add < (NOW() - INTERVAL 1 WEEK)';

        \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql, false);
    }

    /**
     * Función para generar el formato con el que devolveremos la información del proceso de los productos
     * @param $submissionId
     * @param $stateProcessFile
     * @param array $arrayProducts
     * @return array
     */
    private static function generateFormatMMOImportProcess($submissionId, $stateProcessFile, $arrayProducts)
    {
        $submissionListResponse = array();

        $submissionListResponse['Submission']['SubmissionID'] = $submissionId;
        $submissionListResponse['Status'] = $stateProcessFile;

        if (empty($arrayProducts)) {
            return $submissionListResponse;
        }

        $submissionProducts = [];

        foreach ($arrayProducts as $arrayProduct) {
            $submissionProduct = array();
            $status = 'SUCCESS';

            $submissionProduct['SubmissionProductID'] = $arrayProduct['id'];
            $submissionProduct['ProductID'] = $arrayProduct['id_product'];
            $submissionProduct['Message'] = $arrayProduct['message_response_web_service'];

            if ($submissionProduct['Message'] === MMOImport::STATUS_IGNORED) {
                $status = 'IGNORED';
            }

            if (!$arrayProduct['response_web_service']) {
                $status = 'FAILED';
            }

            $submissionProduct['SKU'] = $arrayProduct['sku'];
            $submissionProduct['Status'] = $status;

            $submissionProducts[] = $submissionProduct;
        }

        $submissionListResponse['SubmissionProducts'] = $submissionProducts;

        return $submissionListResponse;
    }

    /**
     * Función para obtener la información del procesamiento de todos los productos pertenecientes a un fichero
     * @param $submissionId
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    private static function getInfoProcessMMOImportProcess($submissionId)
    {
        $sql = "SELECT * 
              FROM `"._DB_PREFIX_."mmo_connector_import_process` ip 
              INNER JOIN `"._DB_PREFIX_."mmo_connector_file_log` fl on ip.id_file = fl.id_file  
              WHERE fl.name = '$submissionId'";

        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        return $result;
    }

    /**
     * Función para obtener el estado del proceso de un fichero
     * @param $submissionId
     * @return string
     */
    private static function getProcessState($submissionId)
    {
        $sql = "SELECT date_process FROM `"._DB_PREFIX_."mmo_connector_file_log` WHERE name = '$submissionId' AND date_process IS NOT NULL LIMIT 1";
        $result = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql, true, false);

        if (empty($result)) {
            MMOLogger::getInstance()->debug('La Submission no se ha procesado', $result);
            return 'SUBMITTED';
        }

        return 'DONE';
    }
}
