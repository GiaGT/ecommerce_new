<?php

namespace MIP\PrestaShop;

class MMOImport
{
    const TYPE_CATEGORY = 'Categories';
    const TYPE_BRAND = 'Brand';
    const TYPE_TAG = 'Tags';
    const TYPE_PRODUCT = 'Products';
    const TYPE_VARIATION = 'Variations';
    const TYPE_STOCK = 'Stock';
    const MAX_IMPORTED_PRODUCTS = 500;
    const STATUS_IGNORED = 'IGNORED';
    const STATUS_DISABLED = 'Product disabled.';

    public static $responseWb = 1;
    public static $stateFinalImport = 1;
    public static $typeErrorResponseWeb = self::TYPE_PRODUCT;
    public static $idProduct;
    public static $message_response_web_service = 'SUCCESS : ';

    public static $lastIdController;

    /**
     * Función para leer el fichero de importación
     */
    public static function readFile()
    {
        $fileInProcess = MMOFile::existFileInProcess();

        if ($fileInProcess) {
            MMOLogger::getInstance()->info('YA SE ESTA PROCESANDO UN FICHERO.');
            return;
        }

        $infoFile = MMOFile::getFilesWithoutProcess();

        if (!isset($infoFile->id_file) || (isset($infoFile->id_file) && !$infoFile->id_file)) {
            MMOLogger::getInstance()->info('NO EXISTEN FICHEROS PARA SER PROCESADOS.');
            return;
        }

        if (0 < version_compare($infoFile->version,\Ps_Mmoconnector::MODULE_VERSION)) {
            @trigger_error('Módulo de versión inferior. Actualizar el modulo.');
            throw new \Exception('Module upgrade needed.',400);
        }

        $productsArray = self::getProductsArrayFromFile($infoFile);

        if (0 > version_compare($infoFile->version,\Ps_Mmoconnector::MODULE_VERSION)) {
            @trigger_error('Fichero de versión inferior. No se deberá procesar.');
            /** @TODO - descomentar el siguiente código para activar el salto de version. */
//            MMOFile::updateMMOConectorFileLog($infoFile->id_file);
//            MMOFile::updateFinishedProcessingState($infoFile->id_file);
//
//            MMOLogger::getInstance()->info('SALTANDO FICHERO DE VERSIÓN INFERIOR.');
//            return;
        }

        $mmoManager = new MMOConnector();

        if (!$mmoManager->getSupplierId()) {
            $mmoManager->createSupplier();
        }

        if (!$productsArray) {
            MMOFile::updateInProcessState($infoFile->id_file);
            MMOLogger::getInstance()->critical("SALTANDO PROCESAMIENTO DE FICHERO CORRUPTO {$infoFile->id_file}.");
            MMOFile::updateMMOConectorFileLog($infoFile->id_file);
            MMOFile::updateFinishedProcessingState($infoFile->id_file);

            return;
        }

        MMOLogger::getInstance()->info('INICIADO FICHERO - ID: '.$infoFile->id_file.' - NOMBRE: '.$infoFile->name);
        MMOFile::updateInProcessState($infoFile->id_file);

        $objectMMOImportProcess = new MMOImportProcess();
        $productsArrayIds = array_column($productsArray, 'ProductID');
        $processedProducts = MMOImportProcess::getProcessedProducts($productsArrayIds, $infoFile->id_file);
        $importedProductsCount = 0;
        foreach ($productsArray as $productArray) {
            if ($importedProductsCount > self::MAX_IMPORTED_PRODUCTS) {
                break;
            }
            self::resetVariable();

            try {
                MMOLogger::getInstance()->info('COMIENZO PROCESAMIENTO - PRODUCTO '.$productArray['ProductID']);

                /**
                 * Saltamos los productos ejecutados en procesos anteriores
                 */
                if (in_array($productArray['ProductID'], $processedProducts)) {
                    MMOLogger::getInstance()->info(
                        'PRODUCTO - PROCESADO ANTERIORMENTE '.$productArray['ProductID']
                    );
                    continue;
                }

                $importProcess = new \stdClass;
                $importProcess->id_product = $productArray['ProductID'];
                $importProcess->sku = $productArray['SKU'];
                $importProcess->id_file = $infoFile->id_file;

                /**
                 * Comprobamos que la version del mensaje del producto a procesar
                 */
                if(!self::checkMessageVersion($productArray)){
                    MMOLogger::getInstance()->info(
                        'PRODUCTO - PROCESADO CON INFORMACION ANTIGUA '.$productArray['ProductID']
                    );
                    $importProcess->response_web_service = 1;
                    $importProcess->message_response_web_service = self::STATUS_IGNORED;
                    $objectMMOImportProcess->addNewMMOImportProcess($importProcess);
                    $importedProductsCount++;
                    continue;
                }

                /**
                 * Si el producto entra desactivado, si se ha creado anteriormente cambiamos solo el estado,
                 * de lo contrario no lo añadimos
                 */
                if (isset($productArray['Active']) && $productArray['Active'] == 0) {
                    self::disableProduct($productArray);
                    $importProcess->response_web_service = 1;
                    $importProcess->message_response_web_service = self::STATUS_DISABLED;
                    $objectMMOImportProcess->addNewMMOImportProcess($importProcess);
                    $importedProductsCount++;
                    continue;
                }

                self::getBrand($productArray);
                self::getCategories($productArray);
                self::getTags($productArray);

                $responseProducts = self::getProducts($productArray);

                self::getStock($productArray, $responseProducts);
                self::getVariations($productArray, $responseProducts, self::$idProduct);

                $importProcess->response_web_service = self::$responseWb;
                $importProcess->message_response_web_service = self::$message_response_web_service.self::$typeErrorResponseWeb.' ID: '.self::$lastIdController;
                $objectMMOImportProcess->addNewMMOImportProcess($importProcess);
                $importedProductsCount++;
                self::checkReponseImportProduct($productArray);
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->critical('ERROR AL PROCESAR EL PRODUCTO '.$productArray['ProductID'].'. '.$e->getMessage(), $e->getTrace());

                if ($e->getCode() === 401) {
                    MMOLogger::getInstance()->critical('REINICIANDO ESTADO DEL FICHERO - ID: '.$infoFile->id_file.' - NOMBRE: '.$infoFile->name);
                    MMOFile::resetStateFile($infoFile->id_file);
                    MMOCache::clean();

                    return;
                }
            }
        }

        $productManager = new MMOProduct();
        $productManager->createSupplierAssociation();

        MMOCache::clean();
        MMOFile::updateMMOConectorFileLog($infoFile->id_file);
        MMOFile::updateFinishedProcessingState($infoFile->id_file);

        MMOLogger::getInstance()->info(
            'FINALIZADO FICHERO - ID: '.$infoFile->id_file.' - NOMBRE: '.$infoFile->name
        );
    }

    /**
     * Función para resetear las variables por cada ejecución de cada producto.
     */
    private static function resetVariable()
    {
        if (!self::$responseWb) {
            self::$stateFinalImport = self::$responseWb;
        }

        self::$lastIdController = '';
        self::$responseWb = 1;
        self::$message_response_web_service = 'SUCCESS : ';
        self::$typeErrorResponseWeb = self::TYPE_PRODUCT;
        self::$idProduct = null;
    }

    /**
     * Función para loguear si un producto se ha importado correctamente o no
     *
     * @param $productArray
     */
    private static function checkReponseImportProduct($productArray)
    {
        if (self::$responseWb) {
            MMOLogger::getInstance()->info('PRODUCTO - PROCESADO '.$productArray['ProductID']);
            self::setProductActive($productArray);

            return;
        }

        self::disableProduct($productArray);
        MMOLogger::getInstance()->info('PRODUCTO NO PROCESADO - WARNING '.$productArray['ProductID']);
    }

    /**
     * Función para abrir el fichero de importación
     *
     * @param $infoFile
     * @return bool|mixed
     */
    private static function getProductsArrayFromFile($infoFile)
    {
        try {
            $filename = \Ps_Mmoconnector::MODULE_DIR.'/files/'.$infoFile->name.'.json';

            if (!file_exists($filename)) {
                MMOLogger::getInstance()->info(
                    'NO EXISTE EL FICHERO - ID: '.$infoFile->id_file.' - NOMBRE: '.$infoFile->name
                );
                return false;
            }

            $data = json_decode(file_get_contents($filename), true);

            if (!$data || empty($data['Products'])) {
                MMOLogger::getInstance()->info(
                    'FICHERO CORRUPTO - ID: '.$infoFile->id_file.' - NOMBRE: '.$infoFile->name
                );

                return false;
            }

            return $data['Products'];
        } catch (\Exception $e) {
            MMOLogger::getInstance()->info(
                'NO SE PUEDE ABRIR EL FICHERO - ID: '.$infoFile->id_file.' - NOMBRE: '.$infoFile->name
            );

            return false;
        }
    }

    /**
     * Función para capturar las marcas del fichero
     *
     * @param $objectData
     * @throws \Exception
     */
    private static function getBrand($objectData)
    {
        if (!self::$responseWb) {
            return;
        }

        if (empty($objectData['Brand']) || empty($objectData['Brand']['BrandID'])) {
            return;
        }

        $cacheId = "Brand-{$objectData['Brand']['BrandID']}-{$objectData['Brand']['Version']}";

        if(MMOCache::isValid($cacheId)) {
            MMOLogger::getInstance()->info("CACHE HIT getBrand $cacheId");
            return;
        }

        $objectManufacturer = new MMOManufacturer();
        $response = $objectManufacturer->process($objectData);

        MMOCache::store($cacheId, is_object($response));

        self::checkResponseWebService($response, self::TYPE_BRAND, $objectData['Brand']['BrandID']);

        if ($objectManufacturer->not_process) {
            return;
        }

        if(self::$responseWb) {
            $idManufacturer = $objectManufacturer->getIdManufacturerShop($objectData['Brand']['BrandID']);
            $objectMMOImage = new MMOImage();
            $response = $objectMMOImage->process($idManufacturer, MMOImage::TYPE_MANUFACTURER, $objectData);
        }

        self::checkResponseWebService($response, self::TYPE_BRAND, $objectData['Brand']['BrandID']);
    }

    /**
     * Función para obtener las categorías del fichero
     *
     * @param $objectData
     * @throws \Exception
     */
    public static function getCategories($objectData)
    {
        if (!self::$responseWb) {
            return;
        }

        if (empty($objectData['Categories'])) {
            return;
        }

        foreach ($objectData['Categories'] as $value) {
            $objectCategory = new MMOCategory();
            $cacheId = "Category-{$value['CategoryID']}-{$value['Version']}";

            if(MMOCache::isValid($cacheId)) {
                MMOLogger::getInstance()->info("CACHE HIT getCategories $cacheId");
                continue;
            }

            if(isset($value['Active']) && (int)$value['Active'] === 0){
                $value['ImageURL'] = null;
            }

            $response = $objectCategory->process($value);
            MMOCache::store($cacheId, is_object($response));
            self::checkResponseWebService($response, self::TYPE_CATEGORY, $value['CategoryID']);

            if(!is_object($response) || $objectCategory->not_process) {
                continue;
            }

            $idCategoryShop = $objectCategory->getIdCategoryShop($value['CategoryID']);
            $objectMMOImage = new MMOImage();
            $response = $objectMMOImage->process($idCategoryShop, MMOImage::TYPE_CATEGORY, $value);
            self::checkResponseWebService($response, self::TYPE_CATEGORY, $value['CategoryID']);
            self::checkResponseImportCategory($value);
        }
    }

    /**
     * Función para loguear si un producto se ha importado correctamente o no
     * @param $value
     */
    private static function checkResponseImportCategory($value)
    {
        if (self::$responseWb) {
            self::enableCategory($value);

            return;
        }
    }

    /**
     * @param $value
     * @return bool|null
     */
    private static function enableCategory($value)
    {
        if (!self::$responseWb) {
            return null;
        }

        $objectCategory = new MMOCategory();
        $response = $objectCategory->activeCategory($value);

        return $response;
    }

    /**
     * Función para obtener los tags del fichero
     * @param $objectData
     */
    private static function getTags($objectData)
    {
        if (!self::$responseWb) {
            return;
        }

        if (empty($objectData['Tags'])) {
            return;
        }

        foreach ($objectData['Tags'] as $value) {
            $objectTag = new MMOTags();

            if (empty($value['Version'])) {
                $value['Version'] = 0;
            }

            $cacheId = "Tags-{$value['TagID']}-{$value['Version']}";

            if (MMOCache::isValid($cacheId)) {
                MMOLogger::getInstance()->info("CACHE HIT getTags $cacheId");
                continue;
            }

            $response = $objectTag->process($value);
            MMOCache::store($cacheId, is_object($response));
            self::checkResponseWebService($response, self::TYPE_TAG, $value['TagID']);
        }
    }

    /**
     * Función para obtener los productos de la lista
     *
     * @param array $objectData
     * @return null|string
     */
    private static function getProducts($objectData)
    {
        if (!self::$responseWb) {
            return null;
        }

        if (empty($objectData['ProductID'])) {
            return null;
        }

        $objectProduct = new MMOProduct();
        $response = $objectProduct->process($objectData);

        self::checkResponseWebService($response, self::TYPE_PRODUCT, $objectData['ProductID']);

        if (!self::$responseWb) {
            return $response;
        }

        self::$idProduct = (int)$response->product->id;
        self::$lastIdController = $objectData['ProductID'];

        $version_img = \Db::getInstance()->getValue(
            'SELECT version_img FROM '._DB_PREFIX_.'mmo_connector_product_map WHERE id_product_shop = '.self::$idProduct,
            false
        );
        if ($version_img !== NULL && $objectData['ImagesVersion'] <= $version_img) {
            MMOLogger::getInstance()->info('IMAGEN PRODUCTO MAPEADA - ID PRODUCT SHOP: '.self::$idProduct.' - VERSION IMG: '.$objectData['ImagesVersion']);
            return $response;
        }

        $objectMMOImage = new MMOImage();

        if (self::$idProduct) {
            $responseImage = $objectMMOImage->process(self::$idProduct, MMOImage::TYPE_PRODUCT, $objectData);
            self::checkResponseWebService($responseImage, self::TYPE_PRODUCT, $objectData['ProductID']);
        }

        return $response;
    }

    /**
     * @param $objectData
     * @return bool|
     */
    private static function setProductActive($objectData)
    {
        if (!self::$responseWb) {
            return null;
        }

        $objectProduct = new MMOProduct();
        $response = $objectProduct->setActiveProduct($objectData);

        return $response;
    }

    /**
     * @param $objectData
     * @return bool|null
     */
    private static function checkMessageVersion($objectData)
    {
        if (!self::$responseWb) {
            return null;
        }

        $objectProduct = new MMOProduct();
        $response = $objectProduct->checkMessageVersion($objectData);

        return $response;
    }

    /**
     * @param $objectData
     * @return bool|null
     */
    private static function disableProduct($objectData)
    {
        if (!self::$responseWb) {
            return null;
        }

        $objectProduct = new MMOProduct();
        $response = $objectProduct->disableProduct($objectData);

        return $response;
    }

    /**
     * Función para obtener el stock del fichero
     * @param $objectData
     * @param $response
     */
    private static function getStock($objectData, $response)
    {
        if (!self::$responseWb || !$response) {
            if (empty(self::$typeErrorResponseWeb)) {
                self::$typeErrorResponseWeb = self::TYPE_PRODUCT;
            }

            return;
        }

        if (!empty($objectData['Stock'])) {
            $id_stock_available = $response->product->associations->stock_availables->stock_available->id;
            $objectStock = new MMOStock();
            $response = $objectStock->process($id_stock_available, $objectData['Stock']);
        }

        self::checkResponseWebService($response, self::TYPE_STOCK);
    }

    /**
     * Función para obtener las variaciones del fichero
     *
     * @param $objectData
     * @param $response
     * @param $productId
     */
    private static function getVariations($objectData, $response, $productId)
    {
        if (!self::$responseWb || !$response) {
            if (empty(self::$typeErrorResponseWeb)) {
                self::$typeErrorResponseWeb = self::TYPE_PRODUCT;
            }

            return;
        }

        if (empty($objectData['Variations'])) {
            return;
        }

        $productPriceTaxIncluded = $objectData['Price'];
        $taxRuleGroupId = $objectData['Tax']['TaxID'];

        foreach ($objectData['Variations'] as $variation) {
            $processVariation = false;

            if (!empty($variation['VariationAttributes'])) {
                $processVariation = true;

                foreach ($variation['VariationAttributes'] as $variationAttribute) {
                    if (!MMOCombination::hasValidLanguageData($variationAttribute)) {
                        MMOLogger::getInstance()->error('INVALID LANGUAGE DATA FOR ID COMBINATION: '.$variation['VariationID'].' - ID PRODUCT: '.$productId);
                        $processVariation = false;
                        break;
                    }

                    $objectAttributeGroup = new MMOAttributeGroup();
                    $attributeGroupResponse = $objectAttributeGroup->process($response, $variationAttribute['AttributeGroup']);

                    self::checkResponseWebService($attributeGroupResponse, self::TYPE_VARIATION);

                    if (!self::$responseWb) {
                        MMOLogger::getInstance()->error('INVALID ATTRIBUTE GROUP WS RESPONSE FOR ID COMBINATION: '.$variation['VariationID'].' - ID PRODUCT: '.$productId);
                        $processVariation = false;
                        break;
                    }

                    $objectAttribute = new MMOAttribute();
                    $attributeResponse = $objectAttribute->process($attributeGroupResponse, $variationAttribute['Attribute']);

                    self::checkResponseWebService($attributeResponse, self::TYPE_VARIATION);

                    if (!self::$responseWb) {
                        MMOLogger::getInstance()->error('INVALID ATTRIBUTE WS RESPONSE FOR ID COMBINATION: '.$variation['VariationID'].' - ID PRODUCT: '.$productId);
                        $processVariation = false;
                        break;
                    }
                }
            }

            if ($processVariation && !empty($variation['VariationID'])) {
                $objectCombination = new MMOCombination();
                $objectCombination->setProductID($productId);
                $objectCombination->setProductPriceTaxIncluded($productPriceTaxIncluded);
                $objectCombination->setTaxRuleGroupId($taxRuleGroupId);
                $objectCombinationResponse = $objectCombination->process($variation);

                self::checkResponseWebService($objectCombinationResponse, 'VariationID');

                if (self::$responseWb) {
                    $stockAvailableID = (int)$objectCombination->getStockAvailableID($productId, $objectCombinationResponse->combination->id);

                    if ($stockAvailableID === 0) {
                        MMOLogger::getInstance()->error(
                            'COMBINATION STOCK - ID COMBINATION: '.$objectCombinationResponse->combination->id.' - ID PRODUCT: '.$productId
                        );
                    } else {
                        $objectStock = new MMOStock();
                        $objectStock->process($stockAvailableID, $variation['Stock']);
                    }
                }
            }
        }
    }

    /**
     * Función para comprobar si ha dado error la respuesta de algún webservice
     * @param $responseWebService
     * @param $type
     * @param null $idObject
     */
    private static function checkResponseWebService($responseWebService, $type, $idObject = null)
    {
        if (is_string($responseWebService)) {
            self::$responseWb = 0;
            self::$typeErrorResponseWeb = $type;
            self::$message_response_web_service = 'FAIL : ';

            if (!empty($idObject)) {
                self::$lastIdController = $idObject;
            }
        }
    }

    /**
     * @param $filename
     * @throws \Exception
     */
    public static function preProcessInformation($filename)
    {
        $infoFile = MMOFile::getFile($filename);

        if (!isset($infoFile->id_file) || (isset($infoFile->id_file) && !$infoFile->id_file)) {
            MMOLogger::getInstance()->info('NO EXISTE EL FICHERO PARA SER PREPROCESADO.');
            return;
        }

        if (0 < version_compare($infoFile->version, \Ps_Mmoconnector::MODULE_VERSION)) {
            @trigger_error('Módulo de versión inferior. Actualizar el modulo.');
            throw new \Exception('Module upgrade needed.', 400);
        }

        $productsArray = MMOImport::getProductsArrayFromFile($infoFile);

        if (0 > version_compare($infoFile->version, \Ps_Mmoconnector::MODULE_VERSION)) {
            @trigger_error('Fichero de versión inferior. No se deberá procesar.');
            /** @TODO - descomentar el siguiente código para activar el salto de version. */
//            MMOFile::updateMMOConectorFileLog($infoFile->id_file);
//            MMOFile::updateFinishedProcessingState($infoFile->id_file);
//
//            MMOLogger::getInstance()->info('SALTANDO FICHERO DE VERSIÓN INFERIOR.');
//            return;
        }

        MMOLogger::getInstance()->info('INICIADO PREPROCESAMIENTO FICHERO - ID: ' . $infoFile->id_file . ' - NOMBRE: ' . $infoFile->name);
        MMOLogger::getInstance()->info('Preprocesando '.count($productsArray).' productos');
        foreach ($productsArray as $productArray) {
            $product = new MMOProduct();

            if ($product->checkMessageVersion($productArray)) {
                $product->setMessageVersion($productArray['ProductID'], new \DateTime($productArray['MessageVersion']));
            }
        }
        MMOLogger::getInstance()->info('FINALIZADO PREPROCESAMIENTO FICHERO - ID: ' . $infoFile->id_file . ' - NOMBRE: ' . $infoFile->name);

    }
}
