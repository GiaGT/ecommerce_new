<?php
/**
 * Created by PhpStorm.
 * User: a4b-4
 * Date: 04/05/2018
 * Time: 13:09
 */

namespace MIP\PrestaShop;


use PrestaShop\PrestaShop\Adapter\Entity\Carrier;

class MMOCarrier
{
    const URL_CARRIERS = 'http://platform.bigbuy.eu/rest/customer/carriers';

    public static function getCarrierDataRequest()
    {
        $productData = null;
        $mipAPIToken = \Configuration::get('MMO_CONNECTOR_ACCESS_TOKEN');

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $mipAPIToken
        );

        $curlOptions = array(
            CURLOPT_HEADER => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLINFO_HEADER_OUT => TRUE,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_TIMEOUT => 60,
        );

        $session = curl_init(self::URL_CARRIERS);
        curl_setopt_array($session, $curlOptions);
        $response = curl_exec($session);
        $index = strpos($response, "\r\n\r\n");

        $header = substr($response, 0, $index);
        $body = substr($response, $index + 4);
        $headerArrayTmp = explode("\n", $header);
        $headerArray = array();

        foreach ($headerArrayTmp as $headerItem) {
            $tmp = explode(':', $headerItem);
            $tmp = array_map('trim', $tmp);

            if (count($tmp) === 2) {
                $headerArray[$tmp[0]] = $tmp[1];
            }
        }

        $status_code = curl_getinfo($session, CURLINFO_HTTP_CODE);

        curl_close($session);

        return array('status_code' => (int)$status_code, 'response' => $body, 'header' => $header);
    }

    public static function getCarrier($carriersApiMip, $nameCarrier)
    {
        $carriersAssociation = json_decode(\Configuration::get('MMO_CONNECTOR_ASSOCIATION_CARRIERS'), true);
        $idCarrierApiFound = false;

        if(empty($carriersAssociation)){
            return false;
        }

        foreach ($carriersApiMip as $carrierApiMip => $key)
        {
            $posSearchNameCarrier = strpos($nameCarrier, $key['name']);
            if($posSearchNameCarrier !== false){
                $idCarrierApiFound = $key['id'];
            }
        }

        if(!$carriersAssociation[$idCarrierApiFound] || empty($carriersAssociation[$idCarrierApiFound]) || !$idCarrierApiFound){
            return false;
        }

        $carrier = new Carrier($carriersAssociation[$idCarrierApiFound]);

        if(!$carrier){
            return false;
        }

        return $carrier->id_reference;
    }

    public static function updateOrderCarrier($order, $carrierId)
    {
        $orderCarrierId = (int) $order->getIdOrderCarrier();

        if(empty($orderCarrierId)){
            return;
        }

        $orderCarrier = new \OrderCarrier($orderCarrierId);
        $orderCarrier->id_carrier = (int) $carrierId;
        $orderCarrier->update();
    }

    public static function isNativeCarrier($languageId, $orderData)
    {
        $carriers = \Carrier::getCarriers($languageId);

        foreach ($carriers as $carrier){
            if((int)$carrier['id_carrier'] === (int)$orderData->id_carrier){
                return true;
            }
        }

        return false;
    }
}