<?php

namespace MIP\PrestaShop;

class MMOSetup
{

    public static function install()
    {
        try{
            MMOLogger::getInstance()->info('Finished installation.');
            return true;
        }
        catch(\Exception $e){
            MMOLogger::getInstance()->critical('Installation failed. '.$e->getMessage());
            return false;
        }
    }

    public static function uninstall()
    {
        try {
            $sql = "
                UPDATE `" . _DB_PREFIX_ . "mmo_connector_attribute_group_map` set version = '{}';
                UPDATE `" . _DB_PREFIX_ . "mmo_connector_attribute_map` set version = '{}';
                UPDATE `" . _DB_PREFIX_ . "mmo_connector_category_map` set version = '{}';
                UPDATE `" . _DB_PREFIX_ . "mmo_connector_manufacturer_map` set version = '0';
                UPDATE `" . _DB_PREFIX_ . "mmo_connector_product_map` set version = '{}', version_img = NULL;
            ";

            /**
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_attribute_group_map;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_attribute_map;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_category_map;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_image_url;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_manufacturer_map;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_order_log;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_product_attribute_map;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_product_image_url;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_product_map;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_product_url;
             * TRUNCATE " . _DB_PREFIX_ . "mmo_connector_tag_map;
             */

            \Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

            MMOLogger::getInstance()->info('Finished uninstall.');
            return true;
        }
        catch(\Exception $e){
            MMOLogger::getInstance()->critical('Uninstall failed. '.$e->getMessage());
            return false;
        }
    }

    /**
     * @param string $newModuleVersion
     * @return bool
     */
    public static function update($newModuleVersion)
    {
        if (!\Module::isInstalled(\Ps_Mmoconnector::MODULE_NAME)) {
            MMOLogger::getInstance()->debug('Module is not installed.');

            return false;
        }

        $module = \Module::getInstanceByName(\Ps_Mmoconnector::MODULE_NAME);
        $module->installed = true;
        $module->database_version = self::getDatabaseModuleVersion();

        if (!\Module::needUpgrade($module)) {
            MMOLogger::getInstance()->debug('Module upgrade is not needed.');

            if (self::getDatabaseModuleVersion() !== $newModuleVersion) {
                \Module::upgradeModuleVersion($module->name, $newModuleVersion);

                return true;
            }

            return false;
        }

        MMOLogger::getInstance()->info('Update started. Module installed: '.$module->installed);

        try {
            if (\Module::initUpgradeModule($module)) {
                $runUpgradeModuleErrors = $module->runUpgradeModule();

                if (count($runUpgradeModuleErrors)) {
                    MMOLogger::getInstance()->critical('Module upgrade failed!', $runUpgradeModuleErrors);
                }
                /**
                 * Run update module udpates the module version to next available update.
                 */
                /**  */
            }
            else {
                MMOLogger::getInstance()->info('Could not initialize module upgrade.');
            }

            $upgradeStatus = \Module::getUpgradeStatus($module->name);

            if ($upgradeStatus) {
                MMOLogger::getInstance()->info("Module upgrade status: $upgradeStatus");
            }

            MMOLogger::getInstance()->info('Finished update.');
            return true;
        } catch (\Exception $e) {
            MMOLogger::getInstance()->critical('Update failed. '.$e->getMessage(), $e->getTrace());

            return false;
        }
    }

    public static function fixPermissions ($path)
    {
        $oldPermissions = umask(0000);
        $dir = new \DirectoryIterator($path);

        foreach ($dir as $item) {
            try {
                chmod($item->getPathname(), 0775);
            }
            catch (\Exception $chmodException) {
                MMOLogger::getInstance()->info('Failed executing chmod '.$chmodException->getMessage());
            }

            MMOLogger::getInstance()->info('Updated permission for '.$item->getPathname());

            if ($item->isDir() && !$item->isDot()) {
                self::fixPermissions($item->getPathname());
            }
        }

        umask($oldPermissions);
    }

    public static function removeDirectory($path)
    {
        if (!is_dir($path)) {
            return;
        }

        $dir = new \DirectoryIterator($path);

        foreach ($dir as $item) {
            if ($item->isDot()) {
                continue;
            }

            if ($item->isDir()) {
                self::removeDirectory($item->getPathname());
                MMOLogger::getInstance()->info('Removing directory '.$item->getPathname());
                rmdir($item->getPathname());

                continue;
            }

            MMOLogger::getInstance()->info('Removing file '.$item->getPathname());

            try {
                unlink($item->getPathname());
            }
            catch (\Exception $unlinkException) {
                MMOLogger::getInstance()->info('Failed executing unlink '.$unlinkException->getMessage());
            }
        }

        rmdir($path);
    }

    public static function getDatabaseModuleVersion()
    {
        $sql = "SELECT version FROM "._DB_PREFIX_."module
				WHERE name = '".\Ps_Mmoconnector::MODULE_NAME."'";

        $version = \Db::getInstance()->getValue($sql);

        return $version;
    }
}