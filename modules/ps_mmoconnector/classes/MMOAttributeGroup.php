<?php

namespace MIP\PrestaShop;

/**
 * Created by PhpStorm.
 * User: BigBuy
 * Date: 18/05/2017
 * Time: 9:04
 */
class MMOAttributeGroup extends MMOObject
{
    public $response;
    public $update = false;
    public $objectType;
    public $productID;
    public $structure = array(
        'AttributeGroupID' => array(
	        'mapped' => false,
	        'required' => true,
	        'nullable' => false,
            'field' => 'id',
            'value' => '',
        ),
        'AttributeGroupLangs' => array(
	        'mapped' => false,
	        'required' => true,
	        'nullable' => false,
            'field' => '',
            'value' => '',
            'IsoCode' => array(
                'field' => 'language',
                'value' => '',
            ),
            'AttributeGroupName' => array(
                'field' => 'name',
                'value' => '-',
            ),
        ),
    );

    public function __construct()
    {
        parent::__construct();
        $this->objectType = 'product_options';
    }

    public function exists()
    {
        $result = \Db::getInstance()->getRow(
            'SELECT id_attribute_group_shop, version FROM '._DB_PREFIX_.'mmo_connector_attribute_group_map WHERE id_attribute_group = '.$this->objectExternalId,
            false
        );

        if (null === $result['version']) {
            $version = $this->version;
        } else {
            $version = json_decode($result['version'], true);
        }

        if (empty($result)) {
            foreach ($this->objeto['AttributeGroupLangs'] as $attributeGroupLang) {
                $this->lang_to_process[$attributeGroupLang['IsoCode']] = $attributeGroupLang['IsoCode'];
            }

            return false;
        }

        if ($this->objeto['AttributeGroupLangs']) {
            foreach ($this->objeto['AttributeGroupLangs'] as $attributeGroupLang) {
                if ((array_key_exists(
                            $attributeGroupLang['IsoCode'],
                            $version
                        ) && ($version[$attributeGroupLang['IsoCode']] < $attributeGroupLang['Version'])) || !array_key_exists(
                        $attributeGroupLang['IsoCode'],
                        $version
                    )) {
                    $this->lang_to_process[$attributeGroupLang['IsoCode']] = $attributeGroupLang['IsoCode'];
                }
                $this->version[$attributeGroupLang['IsoCode']] = $attributeGroupLang['Version'];
            }
        }

        $this->objectId = $result['id_attribute_group_shop'];

        if (!empty($this->lang_to_process)) {
            return true;
        }

        $this->not_process = true;

        return $this->not_process;
    }

    public function getXMLBlankObject()
    {
        $this->xmlObject = $this->webService->getXMLModel($this->objectType);
    }

    public function getXMLObjectFromWebService()
    {
        $this->xmlObject = $this->webService->getXMLId($this->objectType, $this->objectId);
    }

    public function getXMLDefaultStructure()
    {
        $lang = $this->getLanguagesShop();

        foreach ($this->structure as $key => $value) {
            try {
            	if($key === 'AttributeGroupLangs'){
		            foreach ($value as $clave => $valor) {
			            for ($i = 0, $countLangs = count($lang); $i < $countLangs; $i++) {
				            if ($clave == 'IsoCode') {
					            continue;
				            }

				            $val = $this->getValue($value, $clave);
				            $this->xmlObject->product_option->name->language[$i] = $val;
				            $this->xmlObject->product_option->public_name->language[$i] = $val;
			            }
		            }
		            continue;
	            }

	            if ($this->isMapped($key)) {
                    $field = $this->getField($this->structure, $key);
		            $valor = $this->getValue($this->structure, $key);
		            $this->xmlObject->product_option->{$field} = $valor;
	            }
            }
            catch (\Exception $e) {
                MMOLogger::getInstance()->warning($e->getMessage());
                continue;
            }
        }

        $this->xmlObject->product_option->group_type = 'select';
    }

    public function setXMLObjectData($data)
    {
	    $this->checkMinimumFormatAttributeGroup($data);

        foreach ($data['AttributeGroupLangs'] as $attributeGroupLang) {
            $attributeGroupLangVersion = $attributeGroupLang['Version'];
            $attributeGroupLangIsoCode = $attributeGroupLang['IsoCode'];
            $attributeGroupLangName = $attributeGroupLang['AttributeGroupName'];
            $languageId = $this->getIdLangFromIso($attributeGroupLangIsoCode);

            for ($i = 0, $countLanguages = count($this->xmlObject->product_option->name->language); $i < $countLanguages; $i++) {
                foreach ($this->xmlObject->product_option->name->language[$i]->attributes() as $productOptionAttributeName => $productOptionAttributeValue) {
                    if ($productOptionAttributeName === 'id' && (int)$productOptionAttributeValue === $languageId && array_key_exists($attributeGroupLangIsoCode, $this->lang_to_process)) {
                        $this->version[$attributeGroupLangIsoCode] = $attributeGroupLangVersion;
                        $this->xmlObject->product_option->name->language[$i] = $attributeGroupLangName;
                        $this->xmlObject->product_option->public_name->language[$i] = $attributeGroupLangName;
                    }
                }
            }
        }

        if ($this->update) {
            $this->xmlObject->product_option->id = $this->objectId;
        } else {
            unset($this->xmlObject->product_option->id);
        }

        $this->setXMLObjectToWebService();
    }

    public function setXMLObjectToWebService()
    {
        if (!$this->update) {
            $this->response = $this->webService->addXMLId($this->objectType, $this->xmlObject);
        } else {
            $this->response = $this->webService->setXMLId($this->objectType, $this->objectId, $this->xmlObject);
        }
    }

    public function process($response, $dataAttributeGroup)
    {
        if (!is_object($response)) {
            return 'RESPUESTA INCORRECTA MMOAttributeGroup';
        }

        $this->response = null;
        $this->objectId = null;
	    $this->objectExternalId = $dataAttributeGroup['AttributeGroupID'];
        $this->objeto = $dataAttributeGroup;
        $this->productID = $response->product->id;
	    $attributeGroupMappingExists = $this->exists();

        if ($attributeGroupMappingExists) {
            $this->update = true;
            $this->getXMLObjectFromWebService();
        }

        if ($this->not_process && is_object($this->xmlObject)) {
            return (int)$this->objectId;
        }

        $this->not_process = false;

        if ($this->update && !is_object($this->xmlObject)) {
            $this->update = false;
        }

        if (!$attributeGroupMappingExists) {
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }
        else if ($attributeGroupMappingExists && !is_object($this->xmlObject)) {
            MMOLogger::getInstance()->warning('Grupo de atributos mapeado pero no existe en la tienda. Vamos a intentar crearlo de todos modos. Webservice message: '.$this->xmlObject);
            $this->response = null;
            $this->objectId = null;
            $this->version = $this->getLanguageVersionsToProcess($dataAttributeGroup['AttributeGroupLangs'], []);
            $this->lang_to_process = $this->getLanguagesToProcess($this->version);
            $this->getXMLBlankObject();
            $this->getXMLDefaultStructure();
        }

        $this->setXMLObjectData($dataAttributeGroup);
        $this->registerLog($this->response, $dataAttributeGroup['AttributeGroupID'], 'ATTRIBUTE GROUP', $attributeGroupMappingExists);

        if (is_string($this->response)) {
            MMOLogger::getInstance()->error('Error del webservice al procesar el ATTRIBUTE GROUP '.$this->objectExternalId);

            return $this->response;
        }

        MMOLogger::getInstance()->info('INSERCION ATTRIBUTE GROUP: '.$this->objectId);

	    if (!$attributeGroupMappingExists && $this->update === false) {
		    $this->createAttributeGroupMap();
	    }
	    else if ($attributeGroupMappingExists && $this->update === false) {
		    $this->remapAttributeGroupMap();
	    }
	    else {
		    $this->updateAttributeGroupMap();
	    }

        $this->update = false;

        return (int)$this->response->product_option->id;
    }

	public function createAttributeGroupMap()
	{
		$versionData = json_encode($this->version);

		\Db::getInstance()->execute(
			'INSERT INTO '._DB_PREFIX_.'mmo_connector_attribute_group_map (id_attribute_group_shop ,id_attribute_group, version, date_add) VALUES ('.$this->response->product_option->id.','.$this->objectExternalId.",'".$versionData."',NOW())",
			false
		);

		$this->version = '';
	}

	public function updateAttributeGroupMap()
	{
		$versionData = json_encode($this->version);

		\Db::getInstance()->execute(
			'UPDATE '._DB_PREFIX_."mmo_connector_attribute_group_map SET version = '".$versionData."', date_update = NOW() WHERE id_attribute_group_shop = ".$this->objectId,
			false
		);

		$this->version = '';
	}

	public function remapAttributeGroupMap()
	{
		$versionData = json_encode($this->version);

		\Db::getInstance()->execute(
			'UPDATE '._DB_PREFIX_.'mmo_connector_attribute_group_map SET id_attribute_group_shop = '.$this->response->product_option->id.', version = '.$versionData .', date_update = NOW() WHERE id_attribute_group = '.$this->objectExternalId,
			false
		);

		$this->version = '';
	}

    /**
     * @param array $languagesVersionsToProcess
     * @return array
     */
    private function getLanguagesToProcess(array $languagesVersionsToProcess)
    {
        $languagesToProcess = [];

        foreach ($languagesVersionsToProcess as $languageIsoCode => $version) {
            $languagesToProcess[$languageIsoCode] = $languageIsoCode;
        }

        return $languagesToProcess;
    }

    /**
     * @param array $attributeGroupLangsData
     * @param array $currentLanguageVersions
     *
     * @return array
     */
    private function getLanguageVersionsToProcess(array $attributeGroupLangsData, array $currentLanguageVersions)
    {
        $languagesVersionsToProcess = [];

        if (empty($attributeGroupLangsData)) {
            return $languagesVersionsToProcess;
        }

        foreach ($attributeGroupLangsData as $attributeGroupLangData) {
            if (array_key_exists($attributeGroupLangData['IsoCode'], $currentLanguageVersions) && $currentLanguageVersions[$attributeGroupLangData['IsoCode']] <= $attributeGroupLangData['Version']) {
                continue;
            }

            $languagesVersionsToProcess[$attributeGroupLangData['IsoCode']] = $attributeGroupLangData['Version'];
        }

        return $languagesVersionsToProcess;
    }

	private function checkMinimumFormatAttributeGroup($attributeGroupData)
	{
		foreach ($this->structure as $attributeGroupExternalField => $structureData) {
			try {
				if (!array_key_exists($attributeGroupExternalField, $attributeGroupData) && $this->isRequired($attributeGroupExternalField)) {
					$this->response = "EL ATRIBUTE GROUP {$attributeGroupData['AttributeGroupID']} NO TIENE EL CAMPO REQUERIDO $attributeGroupExternalField";
					MMOLogger::getInstance()->error($this->response);

					return false;
				}

				if (!$this->isNullable($attributeGroupExternalField)
                    && $this->isRequired($attributeGroupExternalField)
                    && !array_key_exists($attributeGroupExternalField, $attributeGroupData)) {
					$this->response = "EL ATRIBUTE GROUP {$attributeGroupData['AttributeGroupID']} NO TIENE VALOR EN EL CAMPO SIN VALOR NULO $attributeGroupExternalField";
					MMOLogger::getInstance()->error($this->response);

					return false;
				}
			}
			catch (\Exception $e) {
				MMOLogger::getInstance()->warning($e->getMessage());
				continue;
			}
		}

		return true;
	}
}