<?php

namespace MIP\PrestaShop;

abstract class MMOObject
{
    public $structure;

    public $xmlObject;

    public $objectType;

    public $objectId;

    public $objectExternalId;

    public $messageVersion;

    public $objeto;

    /**
     * @var MMOWebservice
     */
    public $webService;

    public $logger;

    public $version = array();

    public $not_process = false;

    public $lang_to_process = array();

    public function __construct()
    {
        $this->webService = MMOWebservice::getInstance();
        $this->logger = MMOLogger::getInstance();
    }

    /**
     * Chequea la existencia del objeto a través del WebService.
     * - La función llamará internamente a la tabla de log del objeto para determinar la existencia del objeto
     * @return boolean
     */
    abstract public function exists();

    /**
     * Esta función conectará con el WebService para obtener el objeto
     * @return boolean Indicando el correcto funcionamiento de la función
     */
    abstract public function getXMLBlankObject();


    /**
     * Obtiene a partir de un identificador el objeto XML de la tienda a través del WebService
     * @return boolean Indicando el correcto funcionamiento de la función
     */
    abstract public function getXMLObjectFromWebService();

    /**
     * Creará un xml dependiendo del tipo de objecto que se le envia
     */
    abstract public function getXMLDefaultStructure();

    /**
     * Mapea los datos obtenidos con el objeto XML a partir de la estructura
     * @param $data
     * @return boolean Indicando el correcto funcionamiento de la función
     */
    abstract public function setXMLObjectData($data);

    abstract public function setXMLObjectToWebService();

    public function isMapped($field)
    {
        if (!isset($this->structure[$field])) {
            throw new \Exception("No existe el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        if (!isset($this->structure[$field]['mapped'])) {
            throw new \Exception("No existe el valor mapeado del campo (mapped) para el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        return $this->structure[$field]['mapped'];
    }

    public function isNullable($field)
    {
        if (!isset($this->structure[$field])) {
            throw new \Exception("No existe el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        if (!isset($this->structure[$field]['nullable'])) {
            throw new \Exception("No existe el valor nulo del campo (nullable) para el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        return $this->structure[$field]['nullable'];
    }

    public function isRequired($field)
    {
        if (!isset($this->structure[$field])) {
            throw new \Exception("No existe el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        if (!isset($this->structure[$field]['required'])) {
            throw new \Exception("No existe el valor requerido del campo (required) para el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        return $this->structure[$field]['required'];
    }

    public function hasField($field)
    {
        if (!isset($this->structure[$field])) {
            MMOLogger::getInstance()->warning("No existe el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");

            return false;
        }

        if (!isset($this->structure[$field]['field'])) {
            MMOLogger::getInstance()->warning("No existe el nombre del campo (field) para el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");

            return false;
        }

        return true;
    }

    public function getField($structure, $field)
    {
        if (!isset($structure[$field])) {
            throw new \Exception("No existe el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        if (!isset($structure[$field]['field'])) {
            throw new \Exception("No existe el nombre del campo (field) para el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        return $structure[$field]['field'];
    }

    public function getValue($structure, $field)
    {
        if (!isset($structure[$field])) {
            throw new \Exception("No existe el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        if (!isset($structure[$field]['value'])) {
            throw new \Exception("No existe el valoer del campo (value) para el campo {$field} en la estructura del objeto de tipo {$this->objectType}.");
        }

        return $structure[$field]['value'];
    }

    public function getLanguagesShop()
    {
        $cacheId = "MMOObject-getLanguagesShop";

        if (MMOCache::isValid($cacheId)) {
            return MMOCache::get($cacheId);
        }

        $languageIds = \Language::getLanguages(false, false, true);

        MMOCache::store($cacheId, $languageIds);

        return $languageIds;
    }

    /**
     * @param $iso
     * @return int
     */
    public function getIdLangFromIso($iso)
    {
        $cacheId = "MMOObject-getIdLangFromIso-$iso";

        if (MMOCache::isValid($cacheId)) {
            return MMOCache::get($cacheId);
        }

        $languageId = (int)\LanguageCore::getIdByIso($iso);

        MMOCache::store($cacheId, $languageId);

        return $languageId;
    }

    public static function getIsoLangFromId($id)
    {
        $cacheId = "MMOObject-getIsoLangFromId-$id";

        if (MMOCache::isValid($cacheId)) {
            return MMOCache::get($cacheId);
        }

        $isoLang = \Language::getIsoById($id);

        MMOCache::store($cacheId, $isoLang);

        return $isoLang;
    }

    public function getProductCategories($idProduct)
    {
        $productCategories = \ProductCore::getProductCategories($idProduct);

        return $productCategories;
    }

    public function getManufacturerID($manufacturerId)
    {
        $shopManufacturerId = 0;

        if (!$manufacturerId) {
            return $shopManufacturerId;
        }

        $shopManufacturerId = (int)\Db::getInstance()->getValue(
            'SELECT id_manufacturer_shop FROM '._DB_PREFIX_.'mmo_connector_manufacturer_map WHERE id_manufacturer = '.$manufacturerId,
            false
        );

        return $shopManufacturerId;
    }

    /**
     * @param $attributeId
     * @return int
     */
    public function getAttributeID($attributeId)
    {
        $shopAttributeId = 0;

        if (!$attributeId) {
            return $shopAttributeId;
        }

        $shopAttributeId = (int)\Db::getInstance()->getValue(
            'SELECT id_attribute_shop FROM '._DB_PREFIX_.'mmo_connector_attribute_map WHERE id_attribute = '.$attributeId,
            false
        );

        return $shopAttributeId;
    }

    /**
     * @param $categoryId
     * @return int
     */
    public function getCategoryID($categoryId)
    {
        $shopCategoryId = 0;

        if (!$categoryId) {
            return $shopCategoryId;
        }

        $shopCategoryId = (int)\Db::getInstance()->getValue(
            'SELECT id_category_shop FROM '._DB_PREFIX_.'mmo_connector_category_map WHERE id_category = '.$categoryId,
            false
        );

        return $shopCategoryId;
    }

    /**
     * @param $tagId
     * @return int
     */
    public function getTagID($tagId)
    {
        $shopTagId = 0;

        if (!$tagId) {
            return $shopTagId;
        }

        $shopTagId = (int)\Db::getInstance()->getValue(
            'SELECT id_tag_shop FROM '._DB_PREFIX_.'mmo_connector_tag_map WHERE id_tag = '.$tagId,
            false
        );

        return $shopTagId;
    }

    /**
     * @return mixed
     */
    public function getSupplierID()
    {
        $idsupplier = \Configuration::get("MMO_CONNECTOR_SUPPLIER");

        return $idsupplier;
    }

    public function getLastStateOrder($id_order)
    {
        $id_order_state = \Db::getInstance()->getValue(
            'SELECT `id_order_state`
                FROM `'._DB_PREFIX_.'order_history`
                WHERE `id_order` = '.(int)$id_order.'
                ORDER BY `date_add` DESC, `id_order_history` DESC',
            false
        );

        // returns false if there is no state
        if (!$id_order_state) {
            return false;
        }

        return $id_order_state;
    }

    public function getCategoryMapped()
    {
        $categoriesMapped = \Db::getInstance()->query(
            'SELECT id_category_shop FROM '._DB_PREFIX_.'mmo_connector_category_map'
        );

        return $categoriesMapped;
    }

    public function registerLog($response, $id, $type, $existe)
    {
        if (!is_object($response)) {
            MMOLogger::getInstance()->error(strtoupper($type).' - ID: '.$id.' - ERROR: '.$response);

            return;
        }

        if ($type === 'STOCK') {
            MMOLogger::getInstance()->info(strtoupper($type).' - ID: '.$id.' - QUANTITY: '.$existe);

            return;
        }

        MMOLogger::getInstance()->info(strtoupper($type).' - ID: '.$id.' - EXISTE: '.$existe);
    }
	
	public function updateVersion($currentVersion, $submittedVersion)
    {
        if (null === $currentVersion && null === $submittedVersion) {
            return true;
        }

        if ($currentVersion < $submittedVersion) {
            return true;
        }

		return false;
	}
}
