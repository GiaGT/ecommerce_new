<?php

namespace MIP\PrestaShop;

class MMOTaxesRules
{
    public static $idCountry;
    public static $objectType = "tax_rules";
    public static $objectId;
    /** @var MMOWebservice */
    public static $webService;
    public static $xmlObject;
    public static $shop_taxes = array();

    public function __construct()
    {
        self::createDefaultConfiguration();
        self::$idCountry = \Configuration::get('PS_COUNTRY_DEFAULT');
    }

    /**
     * Recupera la tarifa aplicable con ta tasa identificada por el ID de grupo de tasa y el ID de país.
     *
     * Si el país es nulo, se usa el país por defecto de la tienda.
     *
     * @param $taxRuleGroupId
     * @param $countryId
     * @return float
     */
    public function getTaxRateByTaxRuleGroup($taxRuleGroupId, $countryId = null)
    {
        if (!$countryId) {
            $countryId = self::$idCountry;
        }

        $cacheId = 'Tax-TaxRateByTaxRuleGroup-'.$taxRuleGroupId.'-'.$countryId;

        if (MMOCache::isValid($cacheId)) {
            MMOLogger::getInstance()->info("CACHE HIT getTaxRateByTaxRuleGroup $taxRuleGroupId taxRuleGroupId $countryId countryId");
            return MMOCache::get($cacheId);
        }

        $taxRulesXmlList = self::$webService->getXMLList(
            [
                'resource' => 'tax_rules',
                'filter[id_tax_rules_group]' => $taxRuleGroupId,
                'filter[id_country]' => $countryId,
            ]
        );

        $taxRuleAttributes = $taxRulesXmlList->tax_rules->tax_rule->attributes();

        $taxRuleId = $taxRuleAttributes['id'];
        $taxRuleXml = self::$webService->getXMLId('tax_rules', $taxRuleId);

        $taxId = $taxRuleXml->tax_rule->id_tax;
        $taxXml = self::$webService->getXMLId('taxes', $taxId);

        $taxRate = (float)$taxXml->tax->rate;
        MMOCache::store($cacheId, $taxRate);

        return $taxRate;
    }

    public static function process()
    {
        if (!self::$idCountry) {
            self::$idCountry = (int) \Configuration::get('PS_COUNTRY_DEFAULT');
        }

        self::createDefaultConfiguration();

        MMOLogger::getInstance()->info('Started processing taxes.');
        self::getXMLObjectFromWebService();
        MMOLogger::getInstance()->info('Finished processing taxes.');

        MMOLogger::getInstance()->info('Started processing tax rule groups.');
        self::getTaxRulesGroupId();
        MMOLogger::getInstance()->info('Finished processing tax rule groups.');

        return self::$shop_taxes;
    }

    public static function getXMLObjectFromWebService()
    {
        if (self::$objectId) {
            MMOLogger::getInstance()->info('getXMLObjectFromWebService '.self::$objectType.' objectType objectId', self::$objectId);
            self::$xmlObject = self::$webService->getXMLId(self::$objectType, self::$objectId);
        } else {
            MMOLogger::getInstance()->info('getXMLObjectFromWebService '.self::$objectType.' objectType', self::$objectType);
            self::$xmlObject = self::$webService->getXMLList(['resource' => self::$objectType, 'filter[id_country]' => '['.self::$idCountry.']']);
        }
    }

    public static function getTaxRulesGroupId()
    {
        $count = 0;

        foreach (self::$xmlObject->tax_rules->tax_rule as $key => $value) {
            self::$objectId = $value['id'];
            self::$objectType = 'tax_rules';

            $cacheId = 'MMOTaxRules-'.self::$objectType.'-'.self::$objectId.'-'.self::$idCountry;

            if (MMOCache::isValid($cacheId)) {
                MMOLogger::getInstance()->info('CACHE HIT getXMLObjectFromWebService '.self::$objectType.' objectType', self::$objectType);
                self::$xmlObject = MMOCache::get($cacheId);
            }
            else {
                self::getXMLObjectFromWebService();
                MMOCache::store($cacheId, self::$xmlObject);
            }

            $id_country = self::$xmlObject->tax_rule->id_country;

            if ($id_country != self::$idCountry) {
                continue;
            }

            $id_tax = self::$xmlObject->tax_rule->id_tax;
            $id_tax_rules_group = self::$xmlObject->tax_rule->id_tax_rules_group;
            self::getTaxRules($id_tax, $id_tax_rules_group, $count);
            $count++;
        }
    }

    public static function getTaxRules($id_tax, $id_tax_rules_group, $count)
    {
        $id_tax_rules_group = (string)$id_tax_rules_group;
        $name = (string)self::getTaxRulesName($id_tax_rules_group);
        $tax = (string)self::getTaxRate($id_tax);

        self::$shop_taxes['Taxes'][$count]['TaxID'] = $id_tax_rules_group;
        self::$shop_taxes['Taxes'][$count]['Name'] = $name;
        self::$shop_taxes['Taxes'][$count]['Rate'] = $tax;
    }

    public static function getTaxRulesName($taxRuleGroupId)
    {
        $cacheId = "Tax-TaxRulesName-$taxRuleGroupId";

        if (MMOCache::isValid($cacheId)) {
            MMOLogger::getInstance()->info("CACHE HIT getTaxRulesName $taxRuleGroupId taxRuleGroupId");
            return MMOCache::get($cacheId);
        }

        self::$objectId = $taxRuleGroupId;
        self::$objectType = 'tax_rule_groups';
        self::getXMLObjectFromWebService();
        $taxRuleGroupName = (string)self::$xmlObject->tax_rule_group->name;

        MMOCache::store($cacheId, $taxRuleGroupName);

        return $taxRuleGroupName;
    }

    public static function getTaxRate($taxId)
    {
        $cacheId = "Tax-TaxRate-$taxId";

        if (MMOCache::isValid($cacheId)) {
            MMOLogger::getInstance()->info("CACHE HIT getTaxRate $taxId taxId");
            return MMOCache::get($cacheId);
        }

        self::$objectId = $taxId;
        self::$objectType = 'taxes';
        self::getXMLObjectFromWebService();
        $taxRate = (string)self::$xmlObject->tax->rate;

        MMOCache::store($cacheId, $taxRate);

        return $taxRate;
    }

    private static function createDefaultConfiguration()
    {
        $key_id = \Configuration::get('MMOCONECTOR_WEBSERVICE_KEY_ID');
        $webservice_key = new \WebserviceKey($key_id);

        $forceSsl = (\Configuration::get('PS_SSL_ENABLED') && \Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $urlWebService = \Context::getContext()->shop->getBaseURL($forceSsl, true);
        $urlWebService = rtrim($urlWebService, '/');

        if (!defined('MMOCONECTOR_WEBSERVICE_KEY_ID')){
            define('MMOCONECTOR_WEBSERVICE_KEY_ID', $webservice_key->key);
        }

        if (!defined('URLWEBSERVICE')) {
            define('URLWEBSERVICE', $urlWebService);
        }

        self::$webService = MMOWebservice::getInstance();
    }
}