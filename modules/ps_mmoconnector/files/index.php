<?php

if (!in_array(@$_SERVER['REMOTE_ADDR'],['90.161.45.249','localhost','127.0.0.1','::1'])) {
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");

    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    header("Location: ../");

    exit;
}

ob_start();
phpinfo();
$phpInfo = ob_get_contents();
ob_end_clean();

echo str_replace(
    '</body>',
    '<input value="PHP Version|max_execution_time|memory_limit" type="text" id="info-filter" onkeyup="filterInformation()" placeholder="Filter information...">'
    ."<style>#info-filter{width: 100%;font-size: 16px;padding: 12px 20px 12px 40px;margin-bottom: 12px;position: fixed;left: 0px;top: 0px;}body{margin-top:50px;}.center table{margin:0 auto;}td.success{background-color: green}td.error{background-color: red}</style>"
    .'<script>
function filterInformation() {
    var input, i, style, rows;
    input = document.getElementById("info-filter");
    searchPattern = new RegExp(input.value);
    rows = Array.prototype.slice.call(document.getElementsByTagName("tr")).concat(Array.prototype.slice.call(document.getElementsByTagName("h2")));

    for (i = 0; i < rows.length; i++) {
        style = "none";

        if (searchPattern.test(rows[i].innerHTML)) {
            if (rows[i].innerHTML.toUpperCase().indexOf("PHP VERSION") > -1) {
                var phpVersionRowData = rows[i].getElementsByTagName("td");
                
                if (phpVersionRowData.length > 1) {                
                    if (versionCompare(phpVersionRowData[1].innerText,"5.6.9999") < 0 && versionCompare(phpVersionRowData[1].innerText,"5.6.0") >= 0) {
                        if (phpVersionRowData[1].classList.contains("recomended")) {
                            phpVersionRowData[1].classList.remove("recomended");
                            var recomendedValue = phpVersionRowData[1].getElementsByTagName("span");
                            
                            if (recomendedValue.length > 0) {
                                recomendedValue.remove();
                            }
                        }
                        
                        phpVersionRowData[0].classList.remove("error");
                        phpVersionRowData[1].classList.remove("error");
                        phpVersionRowData[0].classList.add("success");
                        phpVersionRowData[1].classList.add("success");
                    }
                    else {
                        phpVersionRowData[0].classList.remove("success");
                        phpVersionRowData[1].classList.remove("success");
                        phpVersionRowData[0].classList.add("error");
                        phpVersionRowData[1].classList.add("error");
                        
                        if (!phpVersionRowData[1].classList.contains("recomended")) {
                            phpVersionRowData[1].classList.add("recomended");
                            var recomendedValue = document.createElement("span");
                            recomendedValue.appendChild(document.createTextNode(" Valor recomendado: 5.6"))
                            phpVersionRowData[1].appendChild(recomendedValue);
                        }
                    }
                }
            }
            
            if (rows[i].innerHTML.toUpperCase().indexOf("MAX_EXECUTION_TIME") > -1) {
                var phpMaxExecutionTimeRowData = rows[i].getElementsByTagName("td");
                
                if (phpMaxExecutionTimeRowData.length > 1) {
                    var maxExecutionTimeValue = phpMaxExecutionTimeRowData[1].innerText;
                    
                    if (maxExecutionTimeValue == 0 || maxExecutionTimeValue >= 12000) {
                        if (phpMaxExecutionTimeRowData[1].classList.contains("recomended")) {
                            phpMaxExecutionTimeRowData[1].classList.remove("recomended");
                            var recomendedValue = phpMaxExecutionTimeRowData[1].getElementsByTagName("span");
                            
                            if (recomendedValue.length > 0) {
                                recomendedValue.remove();
                            }
                        }
                        
                        phpMaxExecutionTimeRowData[0].classList.remove("error");
                        phpMaxExecutionTimeRowData[1].classList.remove("error");
                        phpMaxExecutionTimeRowData[0].classList.add("success");
                        phpMaxExecutionTimeRowData[1].classList.add("success");
                    }
                    else {
                        phpMaxExecutionTimeRowData[0].classList.add("error");
                        phpMaxExecutionTimeRowData[1].classList.add("error");
                        phpMaxExecutionTimeRowData[0].classList.remove("success");
                        phpMaxExecutionTimeRowData[1].classList.remove("success");
                        
                        if (!phpMaxExecutionTimeRowData[1].classList.contains("recomended")) {
                            phpMaxExecutionTimeRowData[1].classList.add("recomended");
                            var recomendedValue = document.createElement("span");
                            recomendedValue.appendChild(document.createTextNode(" Valor recomendado: 15000"))
                            phpMaxExecutionTimeRowData[1].appendChild(recomendedValue);
                        }
                    }
                }
            }
            
            if (rows[i].innerHTML.toUpperCase().indexOf("MEMORY_LIMIT") > -1) {
                var phpMemoryLimitRowData = rows[i].getElementsByTagName("td");
                
                if (phpMemoryLimitRowData.length > 1) {
                    
                    var phpMemoryLimitValue = phpMemoryLimitRowData[1].innerText;
                    var lastCharacter = phpMemoryLimitValue.substr(phpMemoryLimitValue.length - 1);
                    
                    if (!Number.isInteger(lastCharacter)) {
                        phpMemoryLimitValue = phpMemoryLimitValue.substr(0, phpMemoryLimitValue.length - 1);
                        
                        if (lastCharacter === "G") {
                            phpMemoryLimitValue = phpMemoryLimitValue * 1024;
                        }
                    }
                    
                    if (phpMemoryLimitValue == 0 || phpMemoryLimitValue >= 512) {
                        if (phpMemoryLimitRowData[1].classList.contains("recomended")) {
                            phpMemoryLimitRowData[1].classList.remove("recomended");
                            var recomendedValue = phpMemoryLimitRowData[1].getElementsByTagName("span");
                            
                            if (recomendedValue.length > 0) {
                                recomendedValue.remove();
                            }
                        }
                        
                        phpMemoryLimitRowData[0].classList.remove("error");
                        phpMemoryLimitRowData[1].classList.remove("error");
                        phpMemoryLimitRowData[0].classList.add("success");
                        phpMemoryLimitRowData[1].classList.add("success");
                    }
                    else {
                        phpMemoryLimitRowData[0].classList.remove("success");
                        phpMemoryLimitRowData[1].classList.remove("success");
                        phpMemoryLimitRowData[0].classList.add("error");
                        phpMemoryLimitRowData[1].classList.add("error");
                        
                        if (!phpMemoryLimitRowData[1].classList.contains("recomended")) {
                            phpMemoryLimitRowData[1].classList.add("recomended");
                            phpMemoryLimitRowData[1].innerHTML += "<span> Valor recomendado: 512M</span>";
                            
                            if (!phpMemoryLimitRowData[1].classList.contains("recomended")) {
                                phpMemoryLimitRowData[1].classList.add("recomended");
                                var recomendedValue = document.createElement("span");
                                recomendedValue.appendChild(document.createTextNode(" Valor recomendado: 15000"))
                                phpMemoryLimitRowData[1].appendChild(recomendedValue);
                            }
                        }
                    }
                }
            }
            
            style = "";
        }
        
        rows[i].style.display = style;
    }
}

function versionCompare (a, b) {
    var i, diff;
    var regExStrip0 = /(\.0+)+$/;
    var segmentsA = a.replace(regExStrip0, \'\').split(\'.\');
    var segmentsB = b.replace(regExStrip0, \'\').split(\'.\');
    var l = Math.min(segmentsA.length, segmentsB.length);

    for (i = 0; i < l; i++) {
        diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
        if (diff) {
            return diff;
        }
    }
    return segmentsA.length - segmentsB.length;
}

filterInformation();

</script></body>',
    $phpInfo
);

exit;
