<form action="{$form_action}" method="POST">
    <div class="panel col-lg-12">     
        <div class="panel-heading">
            <i class="icon-cogs"></i>
            {l s="Asociación de estados" mod="mmoconnector"}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4">
                    <h3>{l s="Estado inicial para realizar los pedidos BigBuy" mod="mmoconnector"}</h3>
                    <select id="initialStateOrderBigBuy" name="initialStateOrderBigBuy[]" multiple="multiple">
                        {foreach from=$orderStates item=tag key=key}
                            {if $selectionStatus->initial != null}
                                {if in_array($tag['id_order_state'], $selectionStatus->initial)}
                                    <option selected value="{$tag['id_order_state']}"> {$tag['name']}</option>
                                {else}   
                                    <option value="{$tag['id_order_state']}"> {$tag['name']}</option>
                                {/if}
                            {else}
                                <option value="{$tag['id_order_state']}"> {$tag['name']}</option>
                            {/if}

                        {/foreach}
                    </select>
                </div> 
                <div class="col-lg-4">
                    <h3>{l s="En proceso" mod="mmoconnector"}</h3>
                    <select id="inProcessStateOrder" name="inProcessStateOrder">
                        {foreach from=$orderStates item=tag key=key}
                            {if $selectionStatus->in_process != null}
                                {if $tag['id_order_state'] == $selectionStatus->in_process}
                                    <option selected value="{$tag['id_order_state']}"> {$tag['name']}</option>
                                {else}
                                    <option value="{$tag['id_order_state']}"> {$tag['name']}</option>
                                {/if}
                            {else}
                                <option value="{$tag['id_order_state']}"> {$tag['name']}</option>
                            {/if}      
                        {/foreach}
                    </select>
                </div>
                <div class="col-lg-4">
                    <h3>{l s="Enviado" mod="mmoconnector"}</h3>
                    <select id="sentStateOrder" name="sentStateOrder">
                        {foreach from=$orderStates item=tag key=key}
                            {if $selectionStatus->sent != null}
                                {if $tag['id_order_state'] == $selectionStatus->sent}
                                    <option selected value="{$tag['id_order_state']}"> {$tag['name']}</option>
                                {else}
                                    <option value="{$tag['id_order_state']}"> {$tag['name']}</option>
                                {/if}   
                            {else}
                                <option value="{$tag['id_order_state']}"> {$tag['name']}</option>
                            {/if} 
                        {/foreach}
                    </select>
                </div>
            </div>
        </div> 
        <div class="panel-footer">
            <button type="submit" value="1" id="configuration_form_submit_btn" name="submitConfigurationStates" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> Guardar
            </button>
        </div>  
    </div>            
</form>                 