<form action="{$form_action}" method="POST">
    <div class="panel col-lg-12">
        <div class="panel-heading">
            <i class="icon-cogs"></i>
            {l s="Envío de email" mod="mmoconnector"}
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4">
                     <span class="switch prestashop-switch fixed-width-lg">
                         {if $sendEmail}
                            <input type="radio" name="send_email_sandbox" id="send_email_sandbox_on" value="1" checked="checked">
                         {else}
                             <input type="radio" name="send_email_sandbox" id="send_email_sandbox_on" value="1">
                         {/if}
                        <label for="send_email_sandbox_on">{l s="Si" mod="mmoconnector"}</label>
                         {if $sendEmail}
                             <input type="radio" name="send_email_sandbox" id="send_email_sandbox_off" value="0">
                         {else}
                             <input type="radio" name="send_email_sandbox" id="send_email_sandbox_off" value="0" checked="checked">
                         {/if}
                        <label for="send_email_sandbox_off">{l s="No" mod="mmoconnector"}</label>
                        <a class="slide-button btn"></a>
                    </span>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" value="1" id="send_email_configuration_form_submit_btn" name="submitSendEmailConfiguration" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s="Guardar" mod="mmoconnector"}
            </button>
        </div>
    </div>
</form>