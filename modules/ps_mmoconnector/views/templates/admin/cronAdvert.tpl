<div class="panel">
    <h3>
        <i class="icon-info"></i>
    </h3>
    <div id="changedFiles">
        <div class="alert alert-warning">
            {l s="Para el correcto funcionamiento de este módulo es necesario crea la siguiente tarea CRON en su servidor" mod="ps_mmoconnector"}:
            <br />
            <br />
            * * * * * /usr/bin/flock -n /tmp/mmo_process_import.lockfile /usr/bin/curl {$domainShop}/modules/ps_mmoconnector/processImportCron.php >> {$moduleDir}/logs/process_import_cron.log
            <br />
            7 */1 * * *  /usr/bin/flock -n /tmp/mmo_cache_clean.lockfile {$moduleDir}/cache_clean >> {$moduleDir}/logs/cache_clean.log
            <br />
            15 0 * * * /usr/bin/flock -n /tmp/mmo_update_product.lockfile /usr/bin/curl {$domainShop}/modules/ps_mmoconnector/updateProductURLsCron.php >> {$moduleDir}/logs/update_product_urls_cron.log
        </div>
        {if !$phpCompatible}
        <div class="alert alert-danger">
            {l s="Su versión de php es inferior a php 5.6" mod="ps_mmoconnector"}
        </div>
        {/if}
        {if !$hasMogrify}
        <div class="alert alert-warning">
            {l s="La compresión de imagenes no esta disponible." mod="ps_mmoconnector"}
        </div>
        {/if}
    </div>
</div>