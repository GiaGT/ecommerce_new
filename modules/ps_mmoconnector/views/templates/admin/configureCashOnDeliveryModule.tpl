<form action="{$form_action}" method="POST">
    <div class="panel col-lg-12">
        <div class="panel-heading">
            <i class="icon-cogs"></i>
            {l s="Asociación de módulos con pago contrareembolso" mod="mmoconnector"}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4">
                    <h3>{l s="Módulos de pago" mod="mmoconnector"}</h3>
                    <select id="modulesCOD" name="modulesCOD[]" multiple="multiple">
                        {foreach from=$ModulesCOD item=item key=module}
                            {$module|@var_dump}
                            {foreach from=$item item=name key=key}
                                {if in_array($module, $selectionModules)}
                                    <option selected value="{$module}">{$name}</option>
                                {else}
                                    <option value="{$module}">{$name}</option>
                                {/if}
                            {/foreach}
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" value="1" id="configuration_form_submit_btn" name="submitModulesCOD" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> Guardar
            </button>
        </div>
    </div>
</form>