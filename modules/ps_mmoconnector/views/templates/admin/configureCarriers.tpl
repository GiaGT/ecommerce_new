<form action="{$form_action}" method="POST">
    <div class="panel col-lg-12" style="display: none;">
        <div class="panel-heading">
            <i class="icon-cogs"></i>
            {l s="Asociación de transportistas" mod="mmoconnector"}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-2">
                    <h3>{l s="Transportistas Bigbuy" mod="mmoconnector"}</h3>
                </div>
                <div class="col-lg-4">
                    <h3>{l s="Transportistas propios" mod="mmoconnector"}</h3>
                </div>
            </div>
            <br>
            {foreach from=$carriersMip item=tagCarrierMip key=keyMip}
            <div class="row">
                <div class="col-lg-2">
                    <h4>{$tagCarrierMip['name']}</h4>
                </div>
                <div class="col-lg-4">
                    <select id="carriers[{{$tagCarrierMip['id']}}]" name="carriers[{{$tagCarrierMip['id']}}]">
                        <option value="">{l s="Seleccionar" mod="mmoconnector"} </option>
                        {foreach from=$carriers item=tag key=key}
                            <option value="{$tag['id_carrier']}" {if $carriersAssociation[$tagCarrierMip['id']] ==  {$tag['id_carrier']}}selected{/if}> {$tag['name']}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
                <br>
            {/foreach}
        </div> 
        <div class="panel-footer">
            <button type="submit" value="1" id="configuration_form_carrier_submit_btn" name="submitCarriers" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> Guardar
            </button>
        </div>  
    </div>            
</form>                 