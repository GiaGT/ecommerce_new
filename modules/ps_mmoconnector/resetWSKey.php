<?php

require_once __DIR__.'/ps_mmoconnector.php';

$mmoconnector = new \Ps_Mmoconnector();
$result = $mmoconnector->resetWebserviceKey();

$resultDecode = json_encode($result);
echo $resultDecode;