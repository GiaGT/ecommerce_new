<?php

require_once __DIR__.'/ps_mmoconnector.php';
require_once __DIR__.'/vendor/autoload.php';

use MIP\PrestaShop\MMOControl;
use MIP\PrestaShop\MMOLogger;
use MIP\PrestaShop\MMOSetup;

$timezone = new \DateTimeZone('UTC');
$currentDate = new \DateTime();
$currentDate->setTimezone($timezone);
file_put_contents('cron_execution.log', $currentDate->format(DATE_W3C));

$moduleManager = new MMOControl();
$moduleManager->resetExecutionLimits();

if (MMOSetup::update(\Ps_Mmoconnector::MODULE_VERSION)) {
    MMOLogger::getInstance()->info('Module updated.');

    die(date('Y-m-d H:i:s') . " - Module updated\r\n");
}

$moduleManager->enableModule();

$mmoconnector = new \Ps_Mmoconnector();

try {
    $moduleManager->controlFileExecutionTime();
    $moduleManager->controlAllFilesProcessed();
    $mmoconnector->process();
    $mmoconnector->updateCatalogStock();
    $moduleManager->limitStock();

    echo date('Y-m-d H:i:s') . " - OK\r\n";
}
catch (\Exception $exception) {
    if ($exception->getCode() !== 400) {
        MMOLogger::getInstance()->critical('Falla el procesamiento de ficheros. '.$exception->getMessage(), [$exception->getFile(), $exception->getCode(), $exception->getLine(), $exception->getTrace(),]);

        die(date('Y-m-d H:i:s') . " - Error\r\n");
    }

    MMOLogger::getInstance()->warning('Module Upgrade started.');

    if (\Ps_Mmoconnector::upgrade()) {
        die(date('Y-m-d H:i:s')." - Module Upgrade\r\n");
    }

    die(date('Y-m-d H:i:s') . " - Error\r\n");
}