<?php
/* Smarty version 3.1.32, created on 2018-11-21 16:17:28
  from '/Applications/MAMP/htdocs/ecommerce_new/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf57708579f61_03360490',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '994cefd0594fa4034ba84fb7566fe674caa42cf7' => 
    array (
      0 => '/Applications/MAMP/htdocs/ecommerce_new/themes/classic/templates/index.tpl',
      1 => 1541518284,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf57708579f61_03360490 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1624514965bf57708569462_60171605', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_6586562845bf5770856bcd4_20173157 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_11811411745bf57708571816_09087627 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_11895341405bf5770856f2d3_36369316 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11811411745bf57708571816_09087627', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_1624514965bf57708569462_60171605 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_1624514965bf57708569462_60171605',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_6586562845bf5770856bcd4_20173157',
  ),
  'page_content' => 
  array (
    0 => 'Block_11895341405bf5770856f2d3_36369316',
  ),
  'hook_home' => 
  array (
    0 => 'Block_11811411745bf57708571816_09087627',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6586562845bf5770856bcd4_20173157', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11895341405bf5770856f2d3_36369316', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
