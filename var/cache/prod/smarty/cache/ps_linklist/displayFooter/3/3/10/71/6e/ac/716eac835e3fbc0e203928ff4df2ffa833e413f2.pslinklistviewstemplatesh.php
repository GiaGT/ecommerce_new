<?php
/* Smarty version 3.1.32, created on 2018-11-21 16:17:29
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf57709416139_21754641',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1541518284,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf57709416139_21754641 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?><div class="col-md-4 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Prodotti</p>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_26603" data-toggle="collapse">
        <span class="h3">Prodotti</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_26603" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/offerte"
                title="Our special products"
                            >
              Offerte
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/nuovi-prodotti"
                title="I nostri nuovi prodotti, gli ultimi arrivi"
                            >
              Nuovi prodotti
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/piu-venduti"
                title="I nostri prodotti più venduti"
                            >
              Più venduti
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">La nostra azienda</p>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_32061" data-toggle="collapse">
        <span class="h3">La nostra azienda</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_32061" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/content/1-consegna"
                title="I nostri termini e condizioni di consegna"
                            >
              Consegna
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/content/2-note-legali"
                title="Note legali"
                            >
              Note legali
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/content/3-termini-e-condizioni-di-uso"
                title="I nostri termini e condizioni d&#039;uso"
                            >
              Termini e condizioni d&#039;uso
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/content/4-chi-siamo"
                title="Scoprite chi siamo"
                            >
              Chi siamo
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/content/5-pagamento-sicuro"
                title="Il nostro metodo di pagamento sicuro"
                            >
              Pagamento sicuro
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/contattaci"
                title="Si può usare il nostro modulo per contattarci"
                            >
              Contattaci
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/Mappa del sito"
                title="Vi siete persi? Qui potete trovate quello che state cercando"
                            >
              Mappa del sito
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://127.0.0.1/ecommerce_new/negozi"
                title=""
                            >
              Negozi
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }
}
