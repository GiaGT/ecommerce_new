<?php
/* Smarty version 3.1.32, created on 2018-11-21 16:17:25
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf5770512eff8_17599342',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1541518285,
      2 => 'module',
    ),
    '698a6af20e7d7d1ec3787240fa51b79836757f26' => 
    array (
      0 => '/Applications/MAMP/htdocs/ecommerce_new/themes/classic/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1541518284,
      2 => 'file',
    ),
    '67c10a8af62e009d03d95068855dd8f237718d1a' => 
    array (
      0 => '/Applications/MAMP/htdocs/ecommerce_new/themes/classic/templates/catalog/_partials/variant-links.tpl',
      1 => 1541518284,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf5770512eff8_17599342 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Prodotti Popolari
  </h2>
  <div class="products">
          
  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/men/1-1-hummingbird-printed-t-shirt.html#/1-dimensione-s/8-colore-bianco" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/2-home_default/hummingbird-printed-t-shirt.jpg"
              alt = "Hummingbird printed t-shirt"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/2-large_default/hummingbird-printed-t-shirt.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/men/1-1-hummingbird-printed-t-shirt.html#/1-dimensione-s/8-colore-bianco">Hummingbird printed t-shirt</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Prezzo base</span>
                <span class="regular-price">29,16 €</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">23,33 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag discount">Prezzo scontato</li>
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                      <div class="variant-links">
      <a href="http://127.0.0.1/ecommerce_new/men/1-1-hummingbird-printed-t-shirt.html#/1-dimensione-s/8-colore-bianco"
       class="color"
       title="Bianco"
              style="background-color: #ffffff"           ><span class="sr-only">Bianco</span></a>
      <a href="http://127.0.0.1/ecommerce_new/men/1-2-hummingbird-printed-t-shirt.html#/1-dimensione-s/11-colore-nero"
       class="color"
       title="Nero"
              style="background-color: #434A54"           ><span class="sr-only">Nero</span></a>
    <span class="js-count count"></span>
</div>
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="9" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/home/2-9-brown-bear-printed-sweater.html#/1-dimensione-s" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/21-home_default/brown-bear-printed-sweater.jpg"
              alt = "Brown bear printed sweater"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/21-large_default/brown-bear-printed-sweater.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/home/2-9-brown-bear-printed-sweater.html#/1-dimensione-s">Hummingbird printed sweater</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Prezzo base</span>
                <span class="regular-price">43,80 €</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">35,04 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag discount">Prezzo scontato</li>
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="3" data-id-product-attribute="13" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/3-home_default/the-best-is-yet-to-come-framed-poster.jpg"
              alt = "The best is yet to come&#039; Framed poster"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/3-large_default/the-best-is-yet-to-come-framed-poster.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm">The best is yet to come&#039;...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">35,38 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="4" data-id-product-attribute="16" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/home/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/4-home_default/the-adventure-begins-framed-poster.jpg"
              alt = "The adventure begins Framed poster"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/4-large_default/the-adventure-begins-framed-poster.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/home/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm">The adventure begins Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">35,38 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/5-home_default/today-is-a-good-day-framed-poster.jpg"
              alt = "Today is a good day Framed poster"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/5-large_default/today-is-a-good-day-framed-poster.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm">Today is a good day Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">35,38 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/home-accessories/6-mug-the-best-is-yet-to-come.html" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/6-home_default/mug-the-best-is-yet-to-come.jpg"
              alt = "Mug The best is yet to come"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/6-large_default/mug-the-best-is-yet-to-come.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/home-accessories/6-mug-the-best-is-yet-to-come.html">Mug The best is yet to come</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">14,52 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/home-accessories/7-mug-the-adventure-begins.html" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/7-home_default/mug-the-adventure-begins.jpg"
              alt = "Mug The adventure begins"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/7-large_default/mug-the-adventure-begins.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/home-accessories/7-mug-the-adventure-begins.html">Mug The adventure begins</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">14,52 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://127.0.0.1/ecommerce_new/home/8-mug-today-is-a-good-day.html" class="thumbnail product-thumbnail">
            <img
              src = "http://127.0.0.1/ecommerce_new/8-home_default/mug-today-is-a-good-day.jpg"
              alt = "Mug Today is a good day"
              data-full-size-image-url = "http://127.0.0.1/ecommerce_new/8-large_default/mug-today-is-a-good-day.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://127.0.0.1/ecommerce_new/home/8-mug-today-is-a-good-day.html">Mug Today is a good day</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Prezzo</span>
              <span itemprop="price" class="price">14,52 €</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Nuovo</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Anteprima
          </a>
        

        
                  
      </div>

    </div>
  </article>

      </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://127.0.0.1/ecommerce_new/2-home">
    Tutti i prodotti<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
